const express = require('express');
const crypto = require('crypto');
const Recruitment = require('../models/Recruitment');
const SimRecruitment = require('../models/Recommendation/SimRecruitment');
const Notification = require('../libs/notification').Notification;
const { Invitation, INVITATION_TYPES, ACCEPTED_TYPES, CANDIDATE_ACCEPTED_TYPES } = require('../libs/invitation');
const M_Invitation = require('../models/Invitation');
const District = require('../models/District');
const Province = require('../models/Province');
// const Mail = require('../libs/mail').Mail;
const User = require('../models/User/User');
const Field3 = require('../models/Field/Field3');
const R_Like = require('../models/_Like/R_Like');
const R_Follow = require('../models/Follow/R_Follow');
const async = require('async');
const _ = require('lodash');
const math = require('mathjs');
const { fromNow, sumAllElemWithSameField } = require('../libs/shareFunction');
const { trending } = require('../libs/trending');
const { SORT_TYPES } = require('../libs/constants');
const { selectCollections } = require('../libs/applyRecruitment');
const { TRENDING_TYPES } = require('./user');
const { sendMail } = require('../libs/mail');
const config = require('../../config');

module.exports = {
  getAll: (req, res) => {
    const { page, PAGE_SIZE } = req.query;
    Recruitment
      .find({})
      .sort('-updatedDate')
      .limit(Number(PAGE_SIZE))
      .skip(Number(page * PAGE_SIZE))
      .exec((err, recruitments) => {
        if (err) throw err;
        Recruitment
          .count({})
          .exec((err, total) => {
            if (err) throw err;
            res.json({ recruitments, total, page, elemCount: recruitments.length });
          })
      })
  },
  getOne: async (req, res) => {
    try {
      const { id } = req.params;
      
      const result = 
      await Recruitment.findById(id)
      .populate({
        path: '_likes',
        match: { isLiked: true }
      })
      .populate({
        path: '_follows',
        match: { isFollowed: true }
      })
      .populate({
        path: '_organization.id',
        populate: {path: '_banner _logo'}
      })
      if (!req.query.mode) {
        const district = await District.findOne({district_id: result.location.district});
        const province = await Province.findOne({province_id: result.location.province});
        result.location.district = (district)? `${district.type} ${district.name}`: '';
        result.location.province = (province)? `${province.type} ${province.name}`: '';
        try {
          const orgDistrict = await District.findOne({district_id: result._organization.id.location.district});
          const orgProvince = await Province.findOne({province_id: result._organization.id.location.province});
          result._organization.id.location.district = (orgDistrict)? `${orgDistrict.type} ${orgDistrict.name}`: '';
          result._organization.id.location.province = (orgProvince)? `${orgProvince.type} ${orgProvince.name}`: '';  
        } catch (error) {

        }  
      }
      res.json({ result });  
    } catch (error) {
      console.log(error);
      res.status(500).send();
    }
    
  },
  getOneForUserCMS: async (req, res) => {
    const { id } = req.params;
    // Recruitment
    //   .findById(id)
    //   .populate('_candidates.userId interestedCandidates.candidateId')
    //   .lean()
    //   .exec(async (err, result) => {
    //     if (err) {
    //       console.log(err);
    //       throw err;
    //     }
    //     result.interestedCandidates = result.interestedCandidates.filter(elem => elem.isAccepted !== ACCEPTED_TYPES.CANCELLED);
        
        
    //     res.json({ result });
    //   })
    try {
      const result = await Recruitment.findById(id).populate('_candidates.userId interestedCandidates.candidateId').lean();
      result.interestedCandidates = result.interestedCandidates.filter(elem => elem.isAccepted !== ACCEPTED_TYPES.CANCELLED);
      for (let u of result._candidates) {
        console.log(u.userId.address);
        const district = await District.findOne({district_id: u.userId.address.district});
        const province = await Province.findOne({province_id: u.userId.address.province});
        u.userId.address.district = (district)? `${district.type} ${district.name}`: '';
        u.userId.address.province = (province)? `${province.type} ${province.name}`: '';
      }
      for (let u of result.interestedCandidates) {
        const district = await District.findOne({district_id: u.candidateId.address.district});
        const province = await Province.findOne({province_id: u.candidateId.address.province});
        u.candidateId.address.district = (district)? `${district.type} ${district.name}`: '';
        u.candidateId.address.province = (province)? `${province.type} ${province.name}`: '';
      }
      res.json({ result });

    } catch (error) {
      console.log(error);
      res.status(500).send();
    }
    

  },
  getByUserId: async (req, res) => {
    try {
      const userId = req.decoded._id;
      const recruitments = await Recruitment.find({ _author: userId }).sort('-updatedDate');
      res.json({ recruitments })
    } catch (error) {
      console.log(error);
      res.status(500).send(error.toString());
    }

  },
  getAppliedByUserId: async (req, res) => {
    try {
      const userId = req.decoded._id;
      const user = await User.findOne({ _id: userId });
      const recruitments = await Recruitment.find({ _id: { $in: user.applyTo } }).sort('-updatedDate');
      for (let r of recruitments) {
        const district = await District.findOne({district_id: r.location.district});
        const province = await Province.findOne({province_id: r.location.province});
        r.location.district = (district)? `${district.type} ${district.name}`: '';
        r.location.province = (province)? `${province.type} ${province.name}`: '';
      }
      res.json({ recruitments })
    } catch (error) {
      console.log(error);
      res.status(500).send(error.toString());
    }

  },
  create: (req, res) => {
    const data = req.body;
    data._author = req.decoded._id;
    data.createdDate = new Date();
    const newRecruitment = new Recruitment(data);
    newRecruitment.save((err) => {
      if (err) throw err;
      res.json({ success: true });
    })
  },
  update: (req, res) => {
    const { id } = req.params;
    const obj = req.body;
    Recruitment.findByIdAndUpdate(id, obj, function (err, updated) {
      if (err) {
        res.json({ success: false });
        throw err;
      }
      else {
        res.json({ success: true });
      }
    })
  },
  delete: (req, res) => {
    const { id } = req.body;
    Recruitment.findByIdAndRemove(id, function (err, deleted) {
      if (err) {
        res.json({ success: false });
        throw err;
      }
      else {
        res.json({ success: true });
      }
    })
  },
  like: async (req, res) => {
    try {
      const userId = req.decoded._id;
      const recruitmentId = req.params.id;
      const like = await R_Like.findOne({ _author: userId, _recruitment: recruitmentId });
      if (like) {
        await R_Like.findOneAndUpdate({ _author: userId, _recruitment: recruitmentId }, { isLiked: !like.isLiked });
        const likes = await R_Like.find({ _recruitment: recruitmentId, isLiked: true });
        res.json({ likes });
      }
      else {
        const newLike = new R_Like({
          _author: userId,
          _recruitment: recruitmentId,
          isLiked: true
        })
        await newLike.save();
        const likes = await R_Like.find({ _recruitment: recruitmentId, isLiked: true });
        res.json({ likes });
      }
    } catch (error) {
      console.log(error);
      res.status(500).send();
    }

  },
  follow: async (req, res) => {
    try {
      const userId = req.decoded._id;
      const recruitmentId = req.params.id;
      const follow = await R_Follow.findOne({ _author: userId, _recruitment: recruitmentId });
      if (follow) {
        await R_Follow.findOneAndUpdate({ _author: userId, _recruitment: recruitmentId }, { isFollowed: !follow.isFollowed });
        const follows = await R_Follow.find({ _recruitment: recruitmentId, isFollowed: true });
        res.json({ follows });
      }
      else {
        const newFollow = new R_Follow({
          _author: userId,
          _recruitment: recruitmentId,
          isFollowed: true
        })
        await newFollow.save();
        const follows = await R_Follow.find({ _recruitment: recruitmentId, isFollowed: true });
        res.json({ follows });
      }
    } catch (error) {
      console.log(error);
      res.status(500).send();
    }
  },
  apply: async (req, res) => {
    try {
      const { newCV, CVUrl, recruitmentId, cvCode, invitationId } = req.body;
      
      async.each(newCV, (item, callback) => {
        const isPublicItem = { 
          isPublic: item.isPublic, 
          recruitmentId: item.recruitmentId, 
          cvCode: item.cvCode
        };
        var Collection = selectCollections(item.type);
        Collection
          .findByIdAndUpdate(item.id, { $push: { isPublic: isPublicItem } })
          .exec((err, updated) => {
            if (err) console.log(err);
            callback();
          })

      }, (err) => {
        const version = {
          recruitmentId,
          cvCode,
          createdDate: new Date()
        };
        var candidate = {
          userId: req.decoded._id,
          CVUrl,
          updatedDate: new Date()
        };
        const findRecruitmentOptions = {
          _id: recruitmentId,
          _candidates: { $elemMatch: { userId: req.decoded._id } }
          // '_candidates.userId': { $ne: req.decoded._id}
        };
        const updateRecruitmentOptions = {
          $set: {
            '_candidates.$.CVUrl': candidate.CVUrl,
            '_candidates.$.userId': candidate.userId,
            '_candidates.$.updatedDate': new Date(),
          }
          // $addToSet: { _candidates: candidate }
        };
        User
        .findByIdAndUpdate(req.decoded._id, { $addToSet: { versions: version, applyTo: recruitmentId } })
        .exec((err, user) => {
          Recruitment
          .findById(recruitmentId)
          .select('_candidates _author title')
          .populate('_author')
          .exec((error, result) => {
            if (error) {
              console.log(error);
              res.status(500).send({success: false, error});
            }
            const isExisted = result._candidates.find(elem => elem.userId == req.decoded._id);
              if (isExisted) {
                Recruitment
                .update(findRecruitmentOptions, updateRecruitmentOptions, {upsert: true})
                .exec((error) => {
                  if (error) {
                    console.log(error);
                    res.status(500).send({success: false, error});
                  }
                  const content = `Đã ứng tuyển cho tin: ${result.title}`;
                  const url = `/user/recruitment/list-candidate/${recruitmentId}`;
                  const notification = new Notification(req.decoded._id, result._author, content, url);
                  notification.send();
                  const mailOptions = {
                    from: "Youthvn <" + config.email.username + ">",
                    to: result._author.email, // author mail
                    subject: `${user.fullname} đã ứng tuyển cho tin: ${result.title}`,
                    text: '',
                    html: "<a href='" + process.env.API_ENDPOINT + "/cvs/" + user._id + "/" + cvCode + "'>Nhấp vào link để xem hồ sơ online của ứng viên</a>"
                  }
                  sendMail(mailOptions);
                  M_Invitation.findOne({
                    _recruitment: recruitmentId,
                    receiver: req.decoded._id
                  }).sort('-updatedAt').exec((err, result) => {
                    if (err) { console.log(err); res.status(500).send({ success: false }); }
                    if(!result) return;
                    Invitation.update(result._id, ACCEPTED_TYPES.ACCEPTED);
                    
                  })
                  res.json({ success: true });
                });
              }
              else {
                Recruitment.findByIdAndUpdate(recruitmentId, { $addToSet: { _candidates: candidate }}).exec((err) => {
                  if (err) {console.log(err); res.status(500).send({success: false})}
                  M_Invitation.findOne({
                    _recruitment: recruitmentId,
                    receiver: req.decoded._id
                  }).sort('-updatedAt').exec((err, result) => {
                    if (err) { console.log(err); res.status(500).send({ success: false }); }
                    if(!result) return;
                    Invitation.update(result._id, ACCEPTED_TYPES.ACCEPTED);
                    
                  })
                  res.json({ success: true });
                });
              }
            })
            
  
          });
          
          
        })
    } catch (error) {
      console.log(error);
      res.status(500).send({ success: false, error });
    }
  },
  recruit: async (req, res) => {
    try {
      const options = req.body;
      const result = await Recruitment.findById(options.recruitmentId).select('_candidates _author title');
      const findRecruitmentOptions = {
        _id: options.recruitmentId,
        _candidates: { $elemMatch: { userId: options.userId } }
      };
      const updateRecruitmentOptions = {
        $set: {
          '_candidates.$.isAccepted': options.isAccepted
        }
      };
      const user = await User.findById(options.userId);
      await Recruitment.update(findRecruitmentOptions, updateRecruitmentOptions);
      var content = `Chúc mừng bạn đã ứng tuyển thành công cho tin tuyển dụng: ${result.title}.`;
      if (!options.isAccepted)
        content = `Cám ơn bạn đã quan tâm nhưng bạn chưa phù hợp với yêu cầu của tin tuyển dụng: ${result.title}`;
      const url = `/user/recruitment/${options.recruitmentId}/${options.userId}/${options.cvCode}`;
      const notification = new Notification(req.decoded._id, options.userId, content, url);
      notification.send();
      const mailOptions = {
        from: "Youthvn <" + config.email.username + ">",
        to: user.email, // user mail
        subject: `Kết quả ứng tuyển của bạn cho tin tuyển dụng: ${result.title}`,
        text: '',
        html: content + "<br /><a href='" + process.env.API_ENDPOINT + url + "'>Nhấp vào link để xem hồ sơ online của bạn</a>"
      }
      sendMail(mailOptions);
      res.json({ success: true });
    } catch (error) {
      console.log(error);
      res.status(500).send(error);
    }
  },
  rate: async (req, res) => {
    try {
      var options = req.body;
      const result = await Recruitment.findById(options.recruitmentId).select('_candidates _author title');
      const findRecruitmentOptions = {
        _id: options.recruitmentId,
        _candidates: { $elemMatch: { userId: options.userId } }
      };
      const updateRecruitmentOptions = {
        $set: {
          '_candidates.$.rating': options.rating
        }
      };
      const user = await User.findById(options.userId);
      await Recruitment.update(findRecruitmentOptions, updateRecruitmentOptions);
      var content = `Bạn đã được đánh giá trong tin tuyển dụng: ${result.title}.`;
      
      var url = `/user/recruitment/${options.recruitmentId}/${options.userId}/${options.cvCode}`;
      if (!options.cvCode) {
        url = `/user/recruitment/${options.recruitmentId}/${options.userId}`;
      }
      const notification = new Notification(req.decoded._id, options.userId, content, url);
      notification.send();
      res.json({ success: true });
    } catch (error) {
      console.log(error);
      res.status(500).send(error);
    }
  },
  createInterestedCandidates: async (req, res) => {
    try {
      const { recruitmentId, invitationContent } = req.body;
      const receiverId = req.body.userId;
      const senderId = req.decoded._id;

      const recruitment = await Recruitment.findOne({ _id: recruitmentId });
      const interestedCandidate = recruitment.interestedCandidates.find(elem => elem.candidateId.toString() === receiverId.toString());
      const candidate = recruitment._candidates.find(elem => elem.userId.toString() === receiverId.toString());
      if (!recruitment) {
        res.json({ success: false, message: 'Không có tin tuyển dụng này' })
        return;
      }
      if (recruitment._author.toString() === receiverId.toString()) {
        res.json({ success: false, message: 'Không thể tuyển dụng chính bạn' })
        return;
      }
      if (candidate && candidate.isAccepted === CANDIDATE_ACCEPTED_TYPES.ACCEPTED) {
        res.json({ success: false, message: "Thành viên đã trúng tuyển vào tin tuyển dụng của bạn" });
        return;
      }
      if ((interestedCandidate && (interestedCandidate.isAccepted === ACCEPTED_TYPES.REJECTED ||interestedCandidate.isAccepted === ACCEPTED_TYPES.CANCELLED))
        || (candidate && candidate.isAccepted === CANDIDATE_ACCEPTED_TYPES.REJECTED && interestedCandidate.isAccepted !== ACCEPTED_TYPES.UNKNOWN)) {
        await Recruitment.findOneAndUpdate({
          _id: recruitmentId,
          'interestedCandidates.candidateId': interestedCandidate.candidateId
          }, {
            $set: {
              'interestedCandidates.$.isAccepted': ACCEPTED_TYPES.UNKNOWN
            }
        })
        const invitation = new Invitation(
          senderId,
          receiverId,
          invitationContent + `<br />Xem chi tiết tin tuyển dụng: <a href="/recruitments/${recruitment._id}">${recruitment.title}</a>`,
          '',
          INVITATION_TYPES.RECRUITMENT,
          null,
          recruitmentId
        )
        invitation.send();
        res.json({ success: true });
        return;
      }
      if (interestedCandidate && interestedCandidate.isAccepted === ACCEPTED_TYPES.ACCEPTED) {
        res.json({ success: false, message: "Thành viên đã ứng tuyển vào tin tuyển dụng của bạn" });
        return;
      }
      if (interestedCandidate && interestedCandidate.isAccepted === ACCEPTED_TYPES.UNKNOWN) {
        const invitation = await M_Invitation.findOne(
          { receiver: interestedCandidate.candidateId, _recruitment: recruitmentId, isAccepted: ACCEPTED_TYPES.UNKNOWN }
        );
        if (invitation) {
          res.json({ success: false, message: "Lời mời đã được gửi đi trước đó. Xin chở phản hồi" });
          return;
        }
      }


      // create new interested candidate
      const updateOptions = {
        $push: { interestedCandidates: { candidateId: receiverId, createdDate: new Date() } }
      }
      await Recruitment.findOneAndUpdate({ _id: recruitmentId }, updateOptions);

      const invitation = new Invitation(
        senderId,
        receiverId,
        invitationContent + `<br />Xem chi tiết tin tuyển dụng: <a href="/recruitments/${recruitment._id}">${recruitment.title}</a>`,
        '',
        INVITATION_TYPES.RECRUITMENT,
        null,
        recruitmentId
      )
      invitation.send();
      res.json({ success: true });
    } catch (error) {
      console.log(error);
      res.status(500).send(error);
    }

  },
  removeInterestedCanidate: async (req, res) => {
    try {
      const { candidateId, recruitmentId } = req.body;
      const recruitment = await Recruitment.update({
        _id: recruitmentId,
      }, {
          $pull: { interestedCandidates: { candidateId, isAccepted: ACCEPTED_TYPES.ACCEPTED } }
        }, { new: true });
      res.json(recruitment);
    } catch (error) {
      console.log(error);
      res.status(500).send({ success: false });
    }
  },
  cancelInvitation: async (req, res) => {
    try {
      if (!req.decoded) {
        res.status(401).send({ success: false });
        return;
      }
      const { candidateId, recruitmentId } = req.body;
      Invitation.cancel(req.decoded._id, candidateId, recruitmentId, INVITATION_TYPES.RECRUITMENT);
      res.json({ success: true });
    } catch (error) {
      console.log(error);
      res.status(500).send({ success: false });
    }
  },
  viewCount: async (req, res) => {
    try {
      const { id } = req.body;
      const option = {
        $inc: { 'view.total': 1 },
        $currentDate: { 'view.lastModified': true },
        $push: { 'view.at': new Date() }
      }
      const recruitment = await Recruitment.findOneAndUpdate({ _id: id }, option);
      res.json({ recruitment });
    } catch (error) {
      res.status(500).send({ success: false, error })
    }
  },

  filter: async (req, res) => {
    try {

      const NOT_REQUIRED = "Không yêu cầu";
      const query = req.query;
      const body = req.body;
      var recruitments;
      const options = {
        'requirement.minSalary': { $lte: body.salary[1] },
        'requirement.maxSalary': { $gte: body.salary[0] },
        'requirement.minExperience': { $gte: body.experience },
      }
      if (!_.isEmpty(body.fields)) options['requirement.fieldName'] = { $elemMatch: { $in: body.fields } };
      if (!_.isEmpty(body.jobType)) options['requirement.jobType'] = { $in: body.jobType };
      if (!_.isEmpty(body.degree) && !_.includes(body.degree, NOT_REQUIRED)) options['requirement.atLeastDegree'] = { $in: body.degree };
      if (!_.isEmpty(body.gender) && !_.includes(body.gender, NOT_REQUIRED)) options['requirement.gender'] = { $in: body.gender };
      if (!_.isEmpty(body.province)) options['location.province'] = { $in: body.province };

      const populateOptions = {
        path: '_organization.id',
        populate: {path: '_banner _logo'}
      };
      if (query.sortMode === SORT_TYPES.NEWEST) {
        recruitments = await Recruitment
        .find(options)
        .populate(populateOptions).sort('-updatedDate')
          .limit(Number(query.PAGE_SIZE)).skip(Number(query.page * query.PAGE_SIZE));
        const total = await Recruitment.count(options);
        for (let r of recruitments) {
          const district = await District.findOne({district_id: r.location.district});
          const province = await Province.findOne({province_id: r.location.province});
          r.location.district = (district)? `${district.type} ${district.name}`: '';
          r.location.province = (province)? `${province.type} ${province.name}`: '';
        }
        res.json({ recruitments, total, page: query.page, elemCount: recruitments.length })
      }
      else if (query.sortMode === SORT_TYPES.TRENDING) {
        
        trending(req, res, options, Recruitment, TRENDING_TYPES.RECRUITMENT, populateOptions);
      }

    } catch (error) {
      console.log(error);
      res.status(500).send({ error });
    }

  },
  recommendBasedOnUser: async (req, res) => {
    try {
      const { recruitmentId } = req.query;
      const userId = req.decoded._id;
      const user = await User.findById(userId).lean().populate({ path: '_history', match: { type: 'recruitment' } }).populate('_avatar');
      var simRecruitments = await SimRecruitment.find().lean().populate('col');
      if (!user) { res.json({ message: 'Không tìm thấy người dùng' }); return; }
      // calculate history point
      simRecruitments = simRecruitments.map(elem => {
        return {
          ...elem,
          colId: elem.col._id
        }
      });
      user._history.forEach(history => {
        const dayFromNow = fromNow(history.viewAt);
        var viewAtPoint = 0, viewAt_base = 30;
        if (dayFromNow === 0 || dayFromNow === 1) viewAtPoint = 5;
        else viewAtPoint = 1 / math.log(dayFromNow, viewAt_base);
        history.point = history.point * viewAtPoint;
      })
      user._history = sumAllElemWithSameField(user._history, 'recruitmentId', 'point')
        .map(elem => { elem.point /= elem.count; return elem }); // calculate average

      // multiply similarity with history point
      simRecruitments.forEach(recruitment => {
        recruitment.point = 0;
        user._history.forEach(history => {
          if (recruitment.row.toString() === history.recruitmentId.toString()) {
            recruitment.point = recruitment.similarity * history.point;
          }
        })
      })
      simRecruitments = sumAllElemWithSameField(simRecruitments, 'colId', 'point');
      simRecruitments =
        simRecruitments
          .filter(elem => elem.colId.toString() !== recruitmentId.toString() && elem.point !== 0)
          .sort((a, b) => b.point - a.point)
      for (let r of simRecruitments) {
        const district = await District.findOne({district_id: r.col.location.district});
        const province = await Province.findOne({province_id: r.col.location.province});
        r.col.location.district = (district)? `${district.type} ${district.name}`: '';
        r.col.location.province = (province)? `${province.type} ${province.name}`: '';
      }
      res.json({ history: user._history, simRecruitments });
    } catch (error) {
      console.log(error);
      res.status(500).send({ error })
    }
  },
  recommendBasedOnRecruitment: async (req, res) => {
    try {
      const recruitmentId = req.params.id;
      let simRecruitments = await SimRecruitment.find({ row: recruitmentId }).populate('col');
      simRecruitments = simRecruitments.sort((a, b) => b.similarity - a.similarity);
      for (let r of simRecruitments) {
        const district = await District.findOne({district_id: r.col.location.district});
        const province = await Province.findOne({province_id: r.col.location.province});
        r.col.location.district = (district)? `${district.type} ${district.name}`: '';
        r.col.location.province = (province)? `${province.type} ${province.name}`: '';
      }
      res.json({ simRecruitments })
    } catch (error) {
      console.log(error);
      res.status(500).send({ error })
    }

  },
  recommendUser: async (req, res) => {
    const { simCandidates, getFields, RECRUITMENT_TYPES } = require('../libs/recruitmentRecommender');
    const recruitment = await Recruitment.findById(req.params.id).lean().catch(err => { throw err });
    const users = await User.find({}).lean()
      .populate('_educations _experiences _projects _languages _skills _avatar').catch(err => { throw err });
    async.parallel({
      users: (cb) => {
        async.map(
          users,
          (user, next) => {
            getFields(
              [user.jobField],
              (field) => {
                user.field = field
                next(null, user)
              }
            )
          },
          (err, results) => {
            cb(null, results);
          }
        )
      },
      recruitment: (cb) => {
        getFields(
          recruitment.requirement.fieldName,
          (field) => {
            recruitment.field = field;
            cb(null, recruitment)
          }
        )
      }
    }, (err, results) => {
      if (err) throw err;
      const { recruitment, users } = results;

      const Candidates = new simCandidates(recruitment, users, RECRUITMENT_TYPES);
      const finalList = Candidates.main();
      const ids = finalList.final.map(elem => elem.id);
      User
        .find({ _id: { $in: ids } })
        .lean()
        .populate('_languages _skills _avatar')
        .exec((err, recommededUsers) => {
          if (err) throw err;
          async.each(recommededUsers, (user, next) => {
            Field3.findOne({ code: user.jobField }).exec((err, field) => {
              if (err) throw err;
              user.fieldName = field;
              try {
                const { similarity } = finalList.final.find(elem => elem.id.toString() === user._id.toString());
                user.similarity = similarity;
              } catch (error) {
                console.log(error);
                user.similarity = 0;
              }
              next();
            })
          }, async (err) => {
            if (err) throw err;
            recommededUsers = recommededUsers.sort((a, b) => b.similarity - a.similarity);
            for (let u of recommededUsers) {
              const district = await District.findOne({district_id: u.address.district});
              const province = await Province.findOne({province_id: u.address.province});
              u.address.district = (district)? `${district.type} ${district.name}`: '';
              u.address.province = (province)? `${province.type} ${province.name}`: '';
            }
            res.json({ recommededUsers, finalList });
          })

        });
    })
  }
}
