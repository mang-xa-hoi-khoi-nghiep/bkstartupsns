const async = require('async');
const math = require('mathjs');
const UserHistory = require('../models/User/UserHistory');
const HISTORY_TYPES = {
  RECRUITMENT: 'recruitment',
  USER: 'user',
  ORGANIZATION: 'organization'
}

class Point {
  constructor(timePeriod) {
    this.timePeriod = timePeriod;
    this.r_base = 12; // recruitment
    this.u_base = 12; // user
  }
  calculateR_Point() {
    return math.log(this.timePeriod, this.r_base);
  }
  calculateC_Point() {
    return math.log(this.timePeriod, this.c_base);
  }
  calculateU_Point() {
    return math.log(this.timePeriod, this.c_base);
  }
}

const saveUserViewHistory = () => {
  async.each(
    global.user_history, 
    (item, next) => {
      const history = new UserHistory(item);
      history.save((err) => {
        if (err) throw err;
        next();
      })
    }, 
    (err) => {
      console.log('HISTORY SAVED');
      global.user_history = [];
    }
  )
}

setInterval(saveUserViewHistory, 30*1000);
module.exports = {
  trackUserViewHistory: (req, res) => {
    if (req.decoded._id) {
      const body = req.body;
      const history = {
        author: req.decoded._id,
        timePeriod: Number(body.timePeriod),
        type: body.type
      }
      const PointInstance = new Point(body.timePeriod/1000); // convert timePeriod to second
      
      if (body.type === HISTORY_TYPES.RECRUITMENT) {
        history.recruitmentId = body.recruitmentId;
        history.point = PointInstance.calculateR_Point();
      }
      else if (body.type === HISTORY_TYPES.USER) {
        history.userId = body.userId;
        history.point = PointInstance.calculateU_Point();
      }
      global.user_history.push(history);
    }
    console.log('GLOBAL HISTORY: ', global.user_history);   
    res.json({success: true});
  }
}
