const School = require('../models/School');

module.exports = {
    getAll: async (req, res) => {
        try {
            const results = await School.find({}).lean();
            schools = results.map((s) => {
                return { value: s._id, label: s.name };
            })
            res.json({ schools });    
        } catch (error) {
            console.log(error);
            res.status(500).send();
        }
    },

    getOne: async (req, res) => {
        try {
            const school = await School.findOne({ _id: req.params.id });
            res.json({ school });    
        } catch (error) {
            console.log(error);
            res.status(500).send();
        }
    }
}