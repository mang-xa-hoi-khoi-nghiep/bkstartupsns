const Resource = require('../models/Resource/Resource');
const Organization = require('../models/Organization/Organization');
const User = require('../models/User/User');
const async = require('async');

module.exports = {
  org_basicInfo: async (req, res) => {
    try {
      const { itemId } = req.body;
      
      try {
        const org_logo = req.files.org_logo[0];  
        const logo = new Resource({
          url: org_logo.destination + '/' + org_logo.filename,
          _organization: itemId
        })
        await logo.save();
        await Organization.findByIdAndUpdate(itemId, {_logo: logo._id});
      } catch (error) {
      }
      try {
        const org_banner = req.files.org_banner[0];
        const banner = new Resource({
          url: org_banner.destination + '/' + org_banner.filename,
          _organization: itemId
        })
        await banner.save();
        await Organization.findByIdAndUpdate(itemId, {_banner: banner._id});
      } catch (error) {
      }
      res.json({success: true});  
    } catch (error) {
      console.log(error);
      res.status(500).send({success: false});
    }
    
  },

  rec_attachFiles: async (req, res) => {
    try {
      const { itemId, fileUploadKey } = req.body;
      const titles = JSON.parse(fileUploadKey);
      await Resource.remove({ _recruitment: itemId, _user: req.decoded._id, _organization: null });
      async.forEachOf(titles, (elem, index, callback) => {
        const obj = {};
        obj["title"] = elem;
        obj["originalName"] = req.files[index].originalname;
        obj["url"] = req.files[index].destination + "/" + req.files[index].filename;
        obj["_recruitment"] = itemId;
        obj["_user"] = req.decoded._id;
        const file = new Resource(obj);
        file.save((err) => {
          console.log(err);
        });
        callback();
      }, (error) => {
        res.json({ success: true });
      })
    } catch (error) {
      console.log(error);
      res.status(500).send({success: false});
    }
  },

  getAttachFiles: async (req, res) => {
    try {
      const { recruitmentId, userId } = req.params;
      const files = await Resource.find({ _user: userId, _recruitment: recruitmentId, _organization: null });
      res.json({ files });
    } catch (error) {
      console.log(error);
      res.status(500).send({success: false});
    }
  },
  u_avatar: async (req, res) => {
    try {
      const { itemId } = req.body;
      const u_avatar = req.file;
      
      const avatar = new Resource({
        url: u_avatar.destination + '/' + u_avatar.filename,
        _user: itemId
      })

      await avatar.save();
      await User.findByIdAndUpdate(itemId, {_avatar: avatar._id })
      res.json({success: true});
    } catch (error) {
      console.log(error);
      res.status(500).send({success: false});
    }
  }
}