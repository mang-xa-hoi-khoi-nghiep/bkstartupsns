const District = require('../models/District');
const Province = require('../models/Province');
const Organization = require('../models/Organization/Organization');
const Recruitment = require('../models/Recruitment');
const User = require('../models/User/User');
const LOCATION_TYPE = {
  RECRUITMENT: 'recruitment',
  USER: 'user',
  ORGANIZATION: 'organization'
}
const _ = require('lodash');
module.exports = {
  getDistricts: async (req, res) => {
    try {
      const results = await District.find();
      res.json({ results });
    } catch (error) {
      console.log(error);
      res.status(500).send(error);
    }
  },

  getProvinces: async (req, res) => {
    try {
      const results = await Province.find();
      res.json({ results });
    } catch (error) {
      console.log(error);
      res.status(500).send(error);
    }
  },

  getDistrictsByProvince: async (req, res) => {
    try {
      const { provinceId } = req.params;
      const results = await District.find({ province_id: provinceId });
      res.json({ results });
    } catch (error) {
      console.log(error);
      res.status(500).send(error);
    }
  },

  getDistrict: async (req, res) => {
    try {
      const { districtId } = req.params;
      const result = await District.findOne({ district_id: districtId });
      res.json({ result });
    } catch (error) {
      console.log(error);
      res.status(500).send(error);
    }
  },

  getProvince: async (req, res) => {
    try {
      const { provinceId } = req.params;
      const result = await Province.findOne({ province_id: provinceId });
      res.json({ result });
    } catch (error) {
      console.log(error);
      res.status(500).send(error);
    }
  },
  getExistedProvinces: async (req, res) => {
    try {
      const { type } = req.params;
      var Collection, findOptions = {};
      switch (type) {
        case LOCATION_TYPE.RECRUITMENT:
          Collection = Recruitment;
          break;
        case LOCATION_TYPE.USER: 
          Collection = User;
          break;
        case LOCATION_TYPE.ORGANIZATION:
          Collection = Organization;
          break;
        default:
          break;
      }
      const items = await Collection.find({}).lean();
      const itemProvinces = items.map(elem => {
        switch (type) {
          case LOCATION_TYPE.RECRUITMENT:
            return elem.location.province;
            break;
          case LOCATION_TYPE.USER: 
            return elem.address.province;
            break;
          case LOCATION_TYPE.ORGANIZATION:
            return elem.location.province;
            break;
          default:
            break;
        }
      })
      const frequencyProvinceIds = _.reduce(itemProvinces, (results, value) => {
        if (_.isEmpty(results) || !_.some(results, ['province_id', value])) {
          results.push({
            count: 1,
            province_id: value 
          })
          return results;
        }
        else {
          const tmp = _.map(results, (elem) => {
            if (elem.province_id  == value) {
              elem.count++;
            }
            return elem;
          })
          return tmp;
        }
      }, [])
      const ids = frequencyProvinceIds.map(elem => elem.province_id);

      var provinces = await Province.find({province_id: {$in: ids}}).lean();
      provinces.forEach(elem => {
        frequencyProvinceIds.forEach(item => {
          if (elem.province_id === item.province_id) {
            elem.count = item.count;
          }
        })
      })
      res.json({provinces});

    } catch (error) {
      console.log(error);
      res.status(500).send();
    }
  }
}
