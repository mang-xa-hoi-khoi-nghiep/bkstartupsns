const Field1 = require('../models/Field/Field1');
const Field2 = require('../models/Field/Field2');
const Field3 = require('../models/Field/Field3');

module.exports = {
  getField1: (req, res) => {
    Field1
    .find({})
    .exec((err, results) => {
      if (err) throw err;
      res.json({results});
    })
  },
  getField2: (req, res) => {
    Field2
    .find({parentCode: req.query.parentCode})
    .exec((err, results) => {
      if (err) throw err;
      res.json({results});
    })
  },
  getField3: (req, res) => {
    Field3
    .find({parentCode: req.query.parentCode})
    .exec((err, results) => {
      if (err) throw err;
      res.json({results});
    })
  },
  getField3Options: (req, res) => {
    Field3
    .find()
    .exec((err, results) => {
      if (err) throw err;
      res.json({results});
    })
  },
  getOne1: (req, res) => {
    Field1
    .findOne({code: req.params.code})
    .exec((err, result) => {
      if (err) throw err;
      res.json({result});
    })
  },
  getOne2: (req, res) => {
    Field2
    .findOne({code: req.params.code})
    .exec((err, result) => {
      if (err) throw err;
      res.json({result});
    })
  },
  getOne3: (req, res) => {
    Field3
    .findOne({code: req.params.code})
    .exec((err, result) => {
      if (err) throw err;
      res.json({result});
    })
  }
}