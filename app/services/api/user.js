// User
const User = require('../models/User/User');
const UserEducation = require('../models/User/UserEducation');
const UserExperience = require('../models/User/UserExperience');
const UserProject = require('../models/User/UserProject');
const UserHistory = require('../models/User/UserHistory');
const UserDescription = require('../models/User/UserDescription');
const UserActivity = require('../models/User/UserActivity');
const UserAward = require('../models/User/UserAward');
const UserCourse = require('../models/User/UserCourse');
const UserLanguage = require('../models/User/UserLanguage');
const UserSkill = require('../models/User/UserSkill');
const UserPublication = require('../models/User/UserPublication');
const UserCertificate = require('../models/User/UserCertificate');
const UserMiddleman = require('../models/User/UserMiddleman');
const SimUser = require('../models/Recommendation/SimUser');
const Like = require('../models/Like');
const Comment = require('../models/Comment');

// Recruitment
const Recruitment = require('../models/Recruitment');
// Notification
const Notification = require('../models/Notification');
// Field
const Field3 = require('../models/Field/Field3');
// Location
const District = require('../models/District');
const Province = require('../models/Province');
// Module
const ObjectId = require('mongodb').ObjectId;
const crypto = require('crypto');
const jwt    = require('jsonwebtoken');
const config = require('../../config');
const fs = require('fs');
const async = require('async');
const math = require('mathjs');
const _ = require('lodash');
const { trending } = require('../libs/trending');
const { SORT_TYPES } = require('../libs/constants');
const { fromNow, sumAllElemWithSameField } = require('../libs/shareFunction');
const STATUS = {
  ACTIVE: 1
}
const TRENDING_TYPES = {
  USER: 0,
  RECRUITMENT: 1,
  ORGANIZATION: 2
}
const genRandomString = function(length) {
  return crypto.randomBytes(Math.ceil(length/2))
               .toString('hex')
               .slice(0,length);
}
const sha512 = function(password, salt) {
  const hash = crypto.createHmac('sha512', salt);
  hash.update(password);
  const value = hash.digest('hex');
  return {
    salt: salt,
    passwordHash: value
  }
}
function saltHashPassword(userpassword) {
  const salt = genRandomString(16); /** Gives us salt of length 16 */
  const passwordData = sha512(userpassword, salt);
  return passwordData;
}

module.exports = {
  TRENDING_TYPES,
  test: async function(req, res) {
    const user = await User.findOneAndUpdate({ email: "sontrinh179@gmail.com"}, {
      wallet: 220000
    })
  },
  register: function(req, res) {
      const {email, firstname, lastname, password, versions} = req.body;
      const passwordData = saltHashPassword(password);
      const d = new Date();
      const newUser = new User({
        email,
        firstname,
        lastname,
        passwordHash: passwordData.passwordHash,
        salt: passwordData.salt,
        // role,
        createdDate: Date.now(),
        versions
      });
      console.log(newUser);
      newUser.save(function(err) {
        if (err) {
          console.log(err);
          res.json({success: false, message: err.errors.email.message});
        }
        else {
          res.json({success: true, message: "Successful registration"});
        }
      })
  },

  authenticate: function(req, res) {
    const {email, password} = req.body;
    User.findOne({email}, function(err, user) {
      if (err) {
        throw err;
        res.json({success: false, message: "Can't load data"});
      }
      else if (!user) {
        res.json({success: false, message: "Authentication failed. User not found"});
      }
      else if (user) {
        const passwordData = sha512(password, user.salt);
        if (passwordData.passwordHash !== user.passwordHash) {
          res.json({success: false, message: "Authentication failed. Wrong password"});
        }
        else {
          User.findOneAndUpdate({email}, {loginedDate: Date.now()}, {new: true}, function(error, updated) {
            if (error) {
              throw error;
              res.json({success: false, message: "Can't load data"});
            }
            else {
              console.log(updated);
              updated = updated.toObject();
              delete updated.passwordHash;
              delete updated.salt;
              const token = jwt.sign(updated, process.env.JWT_SECRET, {expiresIn : 24*60*60});
              res.json({success: true, message: 'Enjoy your token!', token: token});
            }
          })
        }
      }
    })
  },
  getAllUser: async function(req, res) {
    const users = await User.find({});
    res.send({
      code: 0,
      data: users
    })
  },
  getAll: function(req, res) {
    User.find({}, function(err ,users) {
      res.json(req.decoded);
    })
  },
  searchUsers: async (req, res) => {
    try {
      var { searchStr } = req.query;
      console.log(searchStr);
      const query = {
        $or: [
          {firstname: {$regex: searchStr }},
          {lastname: {$regex: searchStr }},
          {email: {$regex: searchStr }},
        ]
      }
      const users = await User.find(query).select('_id firstname lastname email').populate('_avatar');
      res.json({users})
    } catch (error) {
      console.log(error);
      res.status(500).send();
    }
  },
  validate: function(req, res) {
    User.findOne({_id: req.decoded._id, status: {$ne: 3}}).populate('_likes _avatar').exec(function (err, user) {
      if (err) {
        res.json({success: false, message: "Can't load data"});
        throw err;
      }
      else if (!user) {
        res.json({success: false, message: "Can't find user"});
      }
      else {
        user = user.toObject();
        delete user.passwordHash;
        delete user.salt;
        res.json({success: true, user});
      }
    })
  },

  userInfo: function(req, res) {
    User
    .findOne({_id: req.decoded._id, status: {$ne: 3}})
    .populate('_organizations _educations _experiences _likes _comments _projects _skills _publications _awards _languages _activities _courses _certificates _middlemans _recruitments applyTo _avatar')
    .exec(function(err, user) {
      if (err) {
        res.json({success: false, message: "Can't find user"});
        throw err;
      }
      else {
        user = user.toObject();
        delete user.passwordHash;
        delete user.salt;
        res.json({success: true, user});
      }
    })
  },

  getOne: function(req, res) {
    const { id } = req.params;
    User
    .findById(id)
    .populate('_organizations _educations _experiences  _likes _comments _projects _skills _publications _awards _languages _activities _courses _certificates _middlemans _recruitments _avatar')
    .exec(function(err, user) {
      if (err) {
        res.json({success: false, message: "Can't find user"});
        console.log(err);
        throw err;
      }
      else {
        user = user.toObject();
        delete user.passwordHash;
        delete user.salt;
        res.json({success: true, user});
      }
    })
  },

  publicUser: function(req, res) {
    var options = req.query;
    User
    .findById(query.user)
    .populate('_organizations _educations _experiences  _projects _skills _publications _awards _languages _activities _courses _certificates _middlemans _recruitments _avatar')
    .exec(function(err, user) {
      if (err) {
        res.json({success: false, message: "Can't find user"});
        throw err;
      }
      else {
        user = user.toObject();
        delete user.passwordHash;
        delete user.salt;
        res.json({success: true, user});
      }
    })
  },

  updateUserInfo: function(req, res) {
    const {lastname, firstname, description, phone, address, interest} = req.body;
    if (req.file !== undefined) {
      User.update({_id: req.decoded._id},
        {lastname, firstname, description, phone, address, interest, image_url: req.file.destination, image_name: req.file.filename, last_update_date: Date.now()},
        function(err, user) {
        if (err) {
          res.send("Can't load user");
          throw err;
        }
        else if (!user) {
          res.send("User not found");
        }
        else {
          res.send("Update user info successfully");
        }
      })
    }
    else {
      User.update({_id: req.decoded._id}, {lastname, firstname, description, phone, address, interest, last_update_date: Date.now()},
        function(err, user) {
        if (err) {
          res.send("Can't load user");
          throw err;
        }
        else if (!user) {
          res.send("User not found");
        }
        else {
          res.send("Update user info successfully");
        }
      })
    }
  },

  changePass: function(req, res) {
    const {password, newPassword} = req.body;
    User.findOne({_id: req.decoded._id}, function (err, user) {
      if (err) {
        res.json({success: false, message: "Can't load data"});
        throw err;
      }
      else if (!user) {
        res.json({success: false, message: "User not found"});
      }
      else {
        const passwordData = sha512(password, user.salt);
        if (passwordData.passwordHash !== user.passwordHash) {
          res.json({success: false, message: "Wrong password"});
        }
        else {
          const newPasswordData = saltHashPassword(newPassword);
          User.update({_id: req.decoded._id}, {passwordHash: newPasswordData.passwordHash, salt: newPasswordData.salt},
                      function (err, updated) {
            res.json({success: true, updated});
          })
        }
      }
    })
  },

  deleteUser: function(req, res) {
    User.update({_id: req.decoded._id}, {status: 3}, function(err, user) {
      if (err) {
        res.json({success: false, message: "Can't load data"});
        throw err;
      }
      else if (!user) {
        res.json({success: false, message: "User not found"});
      }
      else {
        res.json({success: true, message: "Delete user successfully"});
      }
    })
  },
  getActiveUsers: function(req, res) {
    const page_size = Number(req.query.page_size) || 6;
    const page = req.query.page || 0;
    User
    .find({status: STATUS.ACTIVE})
    // .populate('_tags')
    .sort('-createdDate')
    .limit(page_size)
    .skip(page*page_size)
    .exec(function(err, users) {
      if (err) {
        res.json({success: false, message: "Can't get active users"})
        throw err;
      }
      User.count({status: STATUS.ACTIVE}, function(err, total_elements) {
        if (err) {
          res.json({success: false, message: "Can't count total active users"});
          throw err;
        }
        res.json(
          { success: true,
            page,
            elements: users.length,
            total_elements,
            users
          }
        );
      })

    })
  },

  recommendRecruitment: async (req, res) => {
    try {
      const {simCandidates, getFields, USER_TYPES} = require('../libs/recruitmentRecommender');

      const user = await User.findById(req.params.id).lean()
      .populate('_educations _experiences _projects _languages _skills');

      const recruitments = await Recruitment.find({}).lean();

      async.parallel({
        recruitments: (cb) => {
          async.map(
            recruitments,
            (recruitment, next) => {

              getFields(
                recruitment.requirement.fieldName,
                (field) => {
                  recruitment.field = field
                  next(null, recruitment)
                }
              )
            },
            (err, results) => {
              cb(null, results);
            }
          )
        },
        user: (cb) => {
          getFields(
            [user.jobField],
            (field) => {
              user.field = field;
              cb(null, user)
            }
          )
        }
      }, (err, results) => {
        if (err) throw err;
        const { recruitments, user } = results;

        const Candidates = new simCandidates(user, recruitments, USER_TYPES);
        const finalList = Candidates.main();
        const ids = finalList.final.map(elem => elem.id);
        Recruitment
        .find({ _id: { $in: ids } })
        .lean()
        .populate('')
        .exec(async (err, recommededRecruitments) => {
        if (err) throw err;
        recommededRecruitments.forEach(r => {
          finalList.final.forEach(e => {
            if (r._id.toString() === e.id.toString()) {
              r.similarity = e.similarity;
            }
          })
        })
        recommededRecruitments = recommededRecruitments.sort((a, b) => b.similarity - a.similarity);
        for (let r of recommededRecruitments) {
          const district = await District.findOne({district_id: r.location.district});
          const province = await Province.findOne({province_id: r.location.province});
          r.location.district = (district)? `${district.type} ${district.name}`: '';
          r.location.province = (province)? `${province.type} ${province.name}`: '';
        }
        res.json({recommededRecruitments, finalList});
        })
      })
    } catch (error) {
      console.log(error);
      res.status(500).send(error)
    }
  },
  viewCount: async (req, res) => {
    try {
      const { id } = req.body;
      const option = {
        $inc: { 'view.total': 1 },
        $currentDate: { 'view.lastModified': true },
        $push: { 'view.at': new Date() }
      }
      const user = await User.findOneAndUpdate({_id: id}, option);
      res.json({user});
    } catch (error) {
      res.status(500).send({success: false, error})
    }
  },
  filter: async (req, res) => {
    try {
      const NOT_REQUIRED = "Không yêu cầu";
      const query = req.query;
      const body = req.body;
      console.log('FILTER BODY: ', body);
      const options = {
        // 'requirement.minSalary': {$lte: body.salary[1]},
        // 'requirement.maxSalary': {$gte: body.salary[0]},
        'experience': { $gte: body.experience },
      }
      if (body.isJobSearchOn) options['jobSearch.isOn'] = body.isJobSearchOn;
      if (!_.isEmpty(body.fields)) options['jobField'] = { $in: body.fields } ;
      if (!_.isEmpty(body.degree) && !_.includes(body.degree, NOT_REQUIRED)) options['educationLevel'] = { $in: body.degree };
      if (!_.isEmpty(body.gender) && !_.includes(body.gender, NOT_REQUIRED)) options['gender'] = { $in: body.gender };
      if (!_.isEmpty(body.province)) options['address.province'] = { $in: body.province };
      console.log('FILTER OPTIONS: ', options);
      const total = await User.count(options);

      if (query.sortMode === SORT_TYPES.NEWEST) {
        const cvs = await User.find(options).lean().sort('-updatedDate').populate('_languages _skills _avatar')
        .limit(Number(query.PAGE_SIZE)).skip(Number(query.page*query.PAGE_SIZE));
        for (let u of cvs) {
          const district = await District.findOne({district_id: u.address.district});
          const province = await Province.findOne({province_id: u.address.province});
          
          const field = await Field3.findOne({code: u.jobField})
          u.fieldName = field;
          u.address.district = (district)? `${district.type} ${district.name}`: '';
          u.address.province = (province)? `${province.type} ${province.name}`: '';
        }
        res.json({cvs, total, page: query.page, elemCount: cvs.length})
      }
      else if (query.sortMode === SORT_TYPES.TRENDING) {
        trending(req, res, options, User, TRENDING_TYPES.USER, '_skills _languages _avatar');
      }


    } catch (error) {
      console.log(error);
      res.json({error});
    }
  },
  recommendBasedOnUser: async (req, res) => {
    try {
      const userId = req.params.id;
      let simUsers = await SimUser.find({row: userId}).
      populate({
          path: 'col',
          populate: { path: '_skills _languages _avatar' }
      });
      for (let u of simUsers) {
        const district = await District.findOne({district_id: u.col.address.district});
        const province = await Province.findOne({province_id: u.col.address.province});
        
        const field = await Field3.findOne({code: u.col.jobField})
        u.col.fieldName = field;
        u.col.address.district = (district)? `${district.type} ${district.name}`: '';
        u.col.address.province = (province)? `${province.type} ${province.name}`: '';
      }
      simUsers = simUsers.sort((a, b) => b.similarity - a.similarity);

      res.json({simUsers})
    } catch (error) {
      console.log(error);
      res.status(500).send({error})
    }

  },
  recommendBasedOnPersonalReference: async (req, res) => {
    try {
      const { currentViewId } = req.query;
      const userId = req.decoded._id;
      const user = await User.findById(userId).lean().populate({path: '_history', match: { type: 'user' }});
      var simUsers = await SimUser.find().lean().populate({path: 'col', populate: {path: '_skills _languages _avatar'}});
      if (!user) {res.json({message: 'Không tìm thấy người dùng'}); return;}
      // calculate history point
      simUsers = simUsers.map(elem => {
        return {
          ...elem,
          colId: elem.col._id
        }
      });

      user._history.forEach(history => {
        const dayFromNow = fromNow(history.viewAt);
        var viewAtPoint = 0, viewAt_base = 30;
        if (dayFromNow === 0 || dayFromNow === 1) viewAtPoint = 5;
        else viewAtPoint = 1/math.log(dayFromNow, viewAt_base);
        history.point = history.point * viewAtPoint;
      })
      user._history = sumAllElemWithSameField(user._history, 'userId', 'point')
                  .map(elem => {elem.point /= elem.count; return elem}); // calculate average

      // multiply similarity with history point
      simUsers.forEach(u => {
        u.point = 0;
        user._history.forEach(history => {
          if (u.row.toString() === history.userId.toString()) {
            u.point = u.similarity*history.point;
          }
        })
      })

      simUsers = sumAllElemWithSameField(simUsers, 'colId', 'point');

      simUsers =
        simUsers
        .filter(elem => elem.colId.toString() !== currentViewId.toString() && elem.point !== 0)
        .sort((a,b) => b.point - a.point)
      for (let u of simUsers) {
        const district = await District.findOne({district_id: u.col.address.district});
        const province = await Province.findOne({province_id: u.col.address.province});
        
        const field = await Field3.findOne({code: u.col.jobField})
        u.col.fieldName = field;
        u.col.address.district = (district)? `${district.type} ${district.name}`: '';
        u.col.address.province = (province)? `${province.type} ${province.name}`: '';
      }
      res.json({history: user._history, simUsers});
    } catch (error) {
      console.log(error);
      res.status(500).send(error)
    }
  }
}
