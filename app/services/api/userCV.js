// UserCV
const User = require('../models/User/User');
const UserEducation = require('../models/User/UserEducation');
const UserExperience = require('../models/User/UserExperience');
const UserProject = require('../models/User/UserProject');
const UserHistory = require('../models/User/UserHistory');
const UserDescription = require('../models/User/UserDescription');
const UserActivity = require('../models/User/UserActivity');
const UserAward = require('../models/User/UserAward');
const UserCourse = require('../models/User/UserCourse');
const UserLanguage = require('../models/User/UserLanguage');
const UserSkill = require('../models/User/UserSkill');
const UserPublication = require('../models/User/UserPublication');
const UserCertificate = require('../models/User/UserCertificate');

// Organization, Major, Degree, District and Province
const Organization = require('../models/Organization/Organization');
const Major = require('../models/Major');
const Degree = require('../models/Degree');
const District = require('../models/District');
const Province = require('../models/Province');

// Module
const ObjectId = require('mongodb').ObjectId;
const crypto = require('crypto');
const jwt    = require('jsonwebtoken');
const config = require('../../config');
const fs = require('fs');
const deleteKey = require('key-del');
const async = require('async');

// libs
const { updateCVCodeUserCV } = require('../libs/applyRecruitment');
const { ITEM_TYPE } = require('../libs/constants');

module.exports = {
  // education
  createEducation: function(req, res) {
    const obj = req.body;
    if (!obj.school_name) {
      return res.json({success: false, message: "Bạn cần chọn trường"});
    }
    obj.school_name = (obj.school_name)? obj.school_name : null;
    obj.author = req.decoded._id;
    const newEducation = new UserEducation(obj);
    newEducation.save(function(err) {
      if (err) {
        console.log(err);
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Tạo mới thành công"});
      }
    })
  },

  editEducation: async function(req, res) {
    const {id} = req.query;
    const obj = req.body;
    if (!obj.school_name) {
      return res.json({success: false, message: "Bạn cần chọn trường"});
    }
    const newObj = await updateCVCodeUserCV(obj, ITEM_TYPE.EDUCATION, id);
    UserEducation.findByIdAndUpdate(id, newObj, function(err, updated) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Cập nhật thành công"});
      }
    })
  },

  deleteEducation: function(req, res) {
    const {id} = req.body;
    UserEducation.findByIdAndRemove(id, function(err, deleted) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Xóa thành công"});
      }
    })
  },

  // experience
  createExperience: function(req, res) {
    const obj = req.body;
    if (!obj.position) {
      return res.json({success: false, message: "Bạn cần điền vị trí"});
    }
    obj.author = req.decoded._id;
    const newExperience = new UserExperience(obj);
    newExperience.save(function(err) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Tạo mới thành công"});
      }
    })
  },

  editExperience: async function(req, res) {
    const {id} = req.query;
    const obj = req.body;
    if (!obj.position) {
      return res.json({success: false, message: "Bạn cần điền vị trí"});
    }
    const newObj = await updateCVCodeUserCV(obj, ITEM_TYPE.EXPERIENCE, id);
    UserExperience.findByIdAndUpdate(id, newObj, function(err, updated) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Cập nhật thành công"});
      }
    })
  },

  deleteExperience: function(req, res) {
    const {id} = req.body;
    UserExperience.findByIdAndRemove(id, function(err, deleted) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Xóa thành công"});
      }
    })
  },

  // project
  createProject: function(req, res) {
    const obj = req.body;
    if (!obj.project_name) {
      return res.json({success: false, message: "Bạn cần điền tên dự án"});
    }
    obj.author = req.decoded._id;
    const newProject = new UserProject(obj);
    newProject.save(function(err) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Tạo mới thành công"});
      }
    })
  },

  editProject: async function(req, res) {
    const {id} = req.query;
    const obj = req.body;
    if (!obj.project_name) {
      return res.json({success: false, message: "Bạn cần điền tên dự án"});
    }
    const newObj = await updateCVCodeUserCV(obj, ITEM_TYPE.PROJECT, id);
    UserProject.findByIdAndUpdate(id, newObj, function(err, updated) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Cập nhật thành công"});
      }
    })
  },

  deleteProject: function(req, res) {
    const {id} = req.body;
    UserProject.findByIdAndRemove(id, function(err, deleted) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Xóa thành công"});
      }
    })
  },

  // activity
  createActivity: function(req, res) {
    const obj = req.body;
    if (!obj.activity_name) {
      return res.json({success: false, message: "Bạn cần điền tên hoạt động"});
    }
    obj.author = req.decoded._id;
    const newActivity = new UserActivity(obj);
    newActivity.save(function(err) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Tạo mới thành công"});
      }
    })
  },

  editActivity: async function(req, res) {
    const {id} = req.query;
    const obj = req.body;
    if (!obj.activity_name) {
      return res.json({success: false, message: "Bạn cần điền tên hoạt động"});
    }
    const newObj = await updateCVCodeUserCV(obj, ITEM_TYPE.ACTIVITY, id);
    UserActivity.findByIdAndUpdate(id, newObj, function(err, updated) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Cập nhật thành công"});
      }
    })
  },

  deleteActivity: function(req, res) {
    const {id} = req.body;
    UserActivity.findByIdAndRemove(id, function(err, deleted) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Xóa thành công"});
      }
    })
  },

  // award
  createAward: function(req, res) {
    const obj = req.body;
    if (!obj.award_name) {
      return res.json({success: false, message: "Bạn cần điền tên giải thưởng"});
    }
    obj.author = req.decoded._id;
    const newAward = new UserAward(obj);
    newAward.save(function(err) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Tạo mới thành công"});
      }
    })
  },

  editAward: async function(req, res) {
    const {id} = req.query;
    const obj = req.body;
    if (!obj.award_name) {
      return res.json({success: false, message: "Bạn cần điền tên giải thưởng"});
    }
    const newObj = await updateCVCodeUserCV(obj, ITEM_TYPE.AWARD, id);
    UserAward.findByIdAndUpdate(id, newObj, function(err, updated) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Cập nhật thành công"});
      }
    })
  },

  deleteAward: function(req, res) {
    const {id} = req.body;
    UserAward.findByIdAndRemove(id, function(err, deleted) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Xóa thành công"});
      }
    })
  },

  // course
  createCourse: function(req, res) {
    const obj = req.body;
    if (!obj.course_name) {
      return res.json({success: false, message: "Bạn cần điền tên khóa học"});
    }
    obj.author = req.decoded._id;
    const newCourse = new UserCourse(obj);
    newCourse.save(function(err) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Tạo mới thành công"});
      }
    })
  },

  editCourse: async function(req, res) {
    const {id} = req.query;
    const obj = req.body;
    if (!obj.course_name) {
      return res.json({success: false, message: "Bạn cần điền tên khóa học"});
    }
    const newObj = await updateCVCodeUserCV(obj, ITEM_TYPE.COURSE, id);
    UserCourse.findByIdAndUpdate(id, newObj, function(err, updated) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Cập nhật thành công"});
      }
    })
  },

  deleteCourse: function(req, res) {
    const {id} = req.body;
    UserCourse.findByIdAndRemove(id, function(err, deleted) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Xóa thành công"});
      }
    })
  },

  // publication
  createPublication: function(req, res) {
    const obj = req.body;
    if (!obj.publication_name) {
      return res.json({success: false, message: "Bạn cần điền tên báo"});
    }
    obj.author = req.decoded._id;
    const newPublication = new UserPublication(obj);
    newPublication.save(function(err) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Tạo mới thành công"});
      }
    })
  },

  editPublication: async function(req, res) {
    const {id} = req.query;
    const obj = req.body;
    if (!obj.publication_name) {
      return res.json({success: false, message: "Bạn cần điền tên báo"});
    }
    const newObj = await updateCVCodeUserCV(obj, ITEM_TYPE.PUBLICATION, id);
    UserPublication.findByIdAndUpdate(id, newObj, function(err, updated) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Cập nhật thành công"});
      }
    })
  },

  deletePublication: function(req, res) {
    const {id} = req.body;
    UserPublication.findByIdAndRemove(id, function(err, deleted) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Xóa thành công"});
      }
    })
  },

  // skill
  createSkill: async (req, res) => {
    try {
      const obj = req.body;
      if (obj.skill_name.length == 0) {
        return res.json({success: false, message: "Bạn cần điền ít nhất 1 kỹ năng"});
      }
      obj.author = req.decoded._id;
      const newSkill = new UserSkill(obj);
      const oldSkill = await UserSkill.findOne({ author: req.decoded._id });
      if (oldSkill) {
        let skill_name = [];
        async.eachSeries(obj.skill_name, (skill, cb) => {
          if (oldSkill.skill_name.indexOf(skill) == -1) {
            skill_name.push(skill);
          }
          cb();
        },(err) => {
          UserSkill.findOneAndUpdate({ author: req.decoded._id }, { $pushAll: { "skill_name": skill_name } }, (err, updatedSkill) => {
            if (err) {
              console.log(error);
              res.status(500).send(error);
            }
            else {
              res.json({success: true, message: "Tạo mới thành công"});
            }
          })
        })
        // const updatedSkill = await UserSkill.findOneAndUpdate({ author: req.decoded._id }, { $pushAll: { "skill_name": obj.skill_name } });
        // res.json({success: true, message: "Tạo mới thành công"});
      }
      else {
        await newSkill.save();
        res.json({success: true, message: "Tạo mới thành công"});
      }  
    } catch (error) {
      console.log(error);
      res.status(500).send(error);
    }
  },

  editSkill: async function(req, res) {
    const {id} = req.query;
    const obj = req.body;
    if (obj.skill_name.length == 0) {
      return res.json({success: false, message: "Bạn cần điền ít nhất 1 kỹ năng"});
    }
    const newObj = await updateCVCodeUserCV(obj, ITEM_TYPE.SKILL, id);
    UserSkill.findByIdAndUpdate(id, newObj, function(err, updated) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Cập nhật thành công"});
      }
    })
  },

  deleteSkill: function(req, res) {
    const {id} = req.body;
    UserSkill.findByIdAndRemove(id, function(err, deleted) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Xóa thành công"});
      }
    })
  },

  // language
  createLanguage: function(req, res) {
    const obj = req.body;
    if (!obj.language_name) {
      return res.json({success: false, message: "Bạn cần chọn ngôn ngữ"});
    }
    obj.author = req.decoded._id;
    const newLanguage = new UserLanguage(obj);
    newLanguage.save(function(err) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Tạo mới thành công"});
      }
    })
  },

  editLanguage: async function(req, res) {
    const {id} = req.query;
    const obj = req.body;
    if (!obj.language_name) {
      return res.json({success: false, message: "Bạn cần chọn ngôn ngữ"});
    }
    const newObj = await updateCVCodeUserCV(obj, ITEM_TYPE.LANGUAGE, id);
    UserLanguage.findByIdAndUpdate(id, newObj, function(err, updated) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Cập nhật thành công"});
      }
    })
  },

  deleteLanguage: function(req, res) {
    const {id} = req.body;
    UserLanguage.findByIdAndRemove(id, function(err, deleted) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Xóa thành công"});
      }
    })
  },

  editBasicInfo: function(req, res) {
    const obj = req.body;
    User.findByIdAndUpdate(req.decoded._id, obj, function(err, updated) {
      if (err) {
        res.json({success: false, message: "Update unsuccessfully"});
        throw err;
      }
      else {
        res.json({success: true, message: "Update successfully"});
      }
    })
  },

  getSchools: function(req, res) {
    Organization.find({type: 1}).select('name').exec(function(err, schools) {
      if (err) {
        res.json({success: false, message: "Find unsuccessfully"});
        throw err;
      }
      else {
        res.json({success: true, message: "Find successfully", schools});
      }
    })
  },

  getMajors: function(req, res) {
    Major.find().exec(function(err, majors) {
      if (err) {
        console.log(err);
        res.json({success: false, message: "Find unsuccessfully"});
        throw err;
      }
      else {
        res.json({success: true, message: "Find successfully", majors});
      }
    })
  },

  getDegrees: function(req, res) {
    Degree.find().exec(function(err, degrees) {
      if (err) {
        res.json({success: false, message: "Find unsuccessfully"});
        throw err;
      }
      else {
        res.json({success: true, message: "Find successfully", degrees});
      }
    })
  },

  getSchoolById: function(req, res) {
    Organization.findById(req.query.id).exec(function(err, school) {
      if (err) {
        console.log(err);
        res.json({success: false, message: "Find unsuccessfully"});
        throw err;
      }
      else {
        res.json({success: true, message: "Find successfully", school});
      }
    })
  },

  getMajorById: function(req, res) {
    Major.findById(req.query.id).exec(function(err, major) {
      if (err) {
        console.log(err);
        res.json({success: false, message: "Find unsuccessfully"});
        throw err;
      }
      else {
        res.json({success: true, message: "Find successfully", major});
      }
    })
  },

  getDegreeById: function(req, res) {
    Degree.findById(req.query.id).exec(function(err, degree) {
      if (err) {
        res.json({success: false, message: "Find unsuccessfully"});
        throw err;
      }
      else {
        res.json({success: true, message: "Find successfully", degree});
      }
    })
  },

  // certificate
  createCertificate: function(req, res) {
    const obj = req.body;
    if (!obj.certificate_name) {
      return res.json({success: false, message: "Bạn cần điền tên bằng cấp"});
    }
    obj.author = req.decoded._id;
    obj.degree = (obj.degree)? obj.degree : null;
    const newCertificate = new UserCertificate(obj);
    newCertificate.save(function(err) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Tạo mới thành công"});
      }
    })
  },

  editCertificate: async function(req, res) {
    const {id} = req.query;
    const obj = req.body;
    if (!obj.certificate_name) {
      return res.json({success: false, message: "Bạn cần điền tên bằng cấp"});
    }
    const newObj = await updateCVCodeUserCV(obj, ITEM_TYPE.CERTIFICATE, id);
    UserCertificate.findByIdAndUpdate(id, newObj, function(err, updated) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Cập nhật thành công"});
      }
    })
  },

  deleteCertificate: function(req, res) {
    const {id} = req.body;
    UserCertificate.findByIdAndRemove(id, function(err, deleted) {
      if (err) {
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
        throw err;
      }
      else {
        res.json({success: true, message: "Xóa thành công"});
      }
    })
  },

  getDistricts: function(req, res) {
    District.find().exec(function(err, districts) {
      if (err) {
        console.log(err);
        res.json({success: false, message: "Error"});
      }
      else {
        res.json({success: true, message: "Successfully", districts});
      }
    })
  },

  getProvinces: function(req, res) {
    Province.find().exec(function(err, provinces) {
      if (err) {
        console.log(err);
        res.json({success: false, message: "Error"});
      }
      else {
        res.json({success: true, message: "Successfully", provinces});
      }
    })
  },

  editJobSearch: function(req, res) {
    var options = req.body;
    options.createdDate = new Date();
    User.findByIdAndUpdate(req.decoded._id, { jobSearch: options }).exec(function (err, result) {
      if (err) {
        console.log(err);
        res.json({success: false, message: "Lỗi máy chủ. Vui lòng thử lại sau"});
      }
      else {
        res.json({success: true, message: "Cập nhật thành công"});
      }
    })
  }
}
