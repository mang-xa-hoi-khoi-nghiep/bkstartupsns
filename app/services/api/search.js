const PostingList = require('../models/Search/PostingList');
const District = require('../models/District');
const Province = require('../models/Province');
const Field3 = require('../models/Field/Field3');

const _ = require('lodash');
const async = require('async');
const math = require('mathjs');
const nlp = require('../libs/vnNLP').main;
const {countViewPerPeriod, calculateThreshold} = require('../libs/trending');
const { SORT_TYPES } = require('../libs/constants');
const NOT_EXISTED = -1;
const EXISTED = 0;
const FIELDS = {
  FIELD1: 0,
  FIELD2: 1,
  FIELD3: 2,
  FIELD4: 3,
  FIELD5: 4,
  FIELD6: 5
}
const TYPES = {
  USER: 'user',
  RECRUITMENT: 'recruitment',
  ORGANIZATION: 'organization'
}
const NOT_REQUIRED = "Không yêu cầu";
const calculateMaxScore = (object) => {

  const scoreArray = [];
  if (object.termFrequency.hasOwnProperty('field1')) {
    scoreArray.push(object.termFrequency.field1 * 4);
  }
  if (object.termFrequency.hasOwnProperty('field2')) {
    scoreArray.push(object.termFrequency.field2)
  }
  if (object.termFrequency.hasOwnProperty('field3')) {
    scoreArray.push(object.termFrequency.field3)
  }
  if (object.termFrequency.hasOwnProperty('field4')) {
    scoreArray.push(object.termFrequency.field4)
  }
  const maxScore = _.max(scoreArray)
  return maxScore;
}
const increaseTermFrequencyInField = (FIELD_NAME, object) => {
  switch (FIELD_NAME) {
    case FIELDS.FIELD1:

      if (object.termFrequency.hasOwnProperty('field1')) {
        object.termFrequency.field1++;
      }
      else {
        object.termFrequency.field1 = 1;
      }
      break;
    case FIELDS.FIELD2:

      if (object.termFrequency.hasOwnProperty('field2')) {
        object.termFrequency.field2++;
      }
      else {
        object.termFrequency.field2 = 1;
      }

      break;
    case FIELDS.FIELD3:

      if (object.termFrequency.hasOwnProperty('field3')) {
        object.termFrequency.field3++;
      }
      else {
        object.termFrequency.field3 = 1;
      }

      break;
    case FIELDS.FIELD4:

      if (object.termFrequency.hasOwnProperty('field4')) {
        object.termFrequency.field4++;
      }
      else {
        object.termFrequency.field4 = 1;
      }

      break;
    default:
      break;
  }
  object.maxScore = calculateMaxScore(object);
  return object;
}
//console.log(nlp("Tôi là phạm trần trí. Hôm nay trời thật đẹp phải ko?;~!@#$%^&*()[]{}/"));
const buildTermDictionary = (documents, NUM_OF_FIELDS) => {
  const dictionary = [];

  for (var FIELD_NAME = 0; FIELD_NAME < NUM_OF_FIELDS; FIELD_NAME++) {
    for (var i = 0; i < documents.length; i++) {
      var text =
        (FIELD_NAME === FIELDS.FIELD1) ? nlp(documents[i].field1)
        : (FIELD_NAME === FIELDS.FIELD2) ? nlp(documents[i].field2)
        : (FIELD_NAME === FIELDS.FIELD3) ? nlp(documents[i].field3)
        : nlp(documents[i].field4)

      for (var j = 0; j < text.length; j++) {
        var isInDictionary = false;
        var marker = NOT_EXISTED;

        // check if existed in dictionary
        for (var k = 0; k < dictionary.length; k++) {
          if (text[j] === dictionary[k].text) {
            isInDictionary = true;
            marker = k;

            break;
          }
        }
        if (!isInDictionary) {
          const newWord = {
            text: text[j],
            docFrequency: 1,
            inDocuments: []
          }
          const object = increaseTermFrequencyInField(
            FIELD_NAME,
            {
              id: documents[i]._id,
              termFrequency: {

              }
            }
          )
          newWord.inDocuments.push(object);
          dictionary.push(newWord);
        }
        else {
          // check if is in document array
          var isInDocuments = false;
          var inDocumentMarker = NOT_EXISTED;
          for (var h = 0; h < dictionary[marker].inDocuments.length; h++) {
            if (documents[i]._id.toString() === dictionary[marker].inDocuments[h].id.toString()) {
              isInDocuments = true;
              inDocumentMarker = h;
              break;
            }
          }
          if (!isInDocuments) {
            dictionary[marker].docFrequency++;


            const object = increaseTermFrequencyInField(
              FIELD_NAME,
              {
                id: documents[i]._id,
                termFrequency: {}
              }
            )
            dictionary[marker].inDocuments.push(object);
          }
          else {
            dictionary[marker].inDocuments[inDocumentMarker]
              = increaseTermFrequencyInField(FIELD_NAME, dictionary[marker].inDocuments[inDocumentMarker])
            
          }
        }
      }
    }
  }
  return dictionary;
}
const selectCollections = (type) => {
  
  var Collection, TermDictionary, populateOption = '', NumOfFields = 0;
  switch (type) {
    
    case TYPES.USER:
      Collection = require('../models/User/User');
      TermDictionary = require('../models/Search/U_TermDictionary');
      populateOption = '_skills _languages _avatar';
      NumOfFields = 3;
      break;
    case TYPES.RECRUITMENT:
      Collection = require('../models/Recruitment');
      TermDictionary = require('../models/Search/R_TermDictionary');
      populateOption = '';
      NumOfFields = 3;
      break;
    case TYPES.ORGANIZATION:
      Collection = require('../models/Organization/Organization');
      TermDictionary = require('../models/Search/O_TermDictionary');
      populateOption = '_employees _products _events';
      NumOfFields = 1;
      break;
    default:
      break;
  }
  return {Collection, TermDictionary, populateOption, NumOfFields};
}
const mappingDocuments = (documents, type) => {
  var toIndexDocuments = [];
  var newDoc = {};
  switch (type) {
    
    case TYPES.USER:
    toIndexDocuments = documents.map((doc) => {
      const skills = _.flattenDeep(doc._skills.map(skill => skill.skill_name)).join(' ');
      const languages = doc._languages.map(lang => lang.language_name).join(' ');
      newDoc = {
        _id: doc._id,
        field1: doc.fullname,
        field2: skills,
        field3: languages
        // title, name, skill
      }
      return newDoc;
    })
      break;
    case TYPES.RECRUITMENT:
      toIndexDocuments = documents.map((doc) => {
        newDoc = {
          _id: doc._id,
          field1: doc.title,
          field2: doc.requirement.positions.join(' '),
          field3: doc.requirement.skills.join(' '),
          // skills, organization, positions
        }
        return newDoc;
      })
      break;
    case TYPES.ORGANIZATION:
      toIndexDocuments = documents.map((doc) => {
        newDoc = {
          _id: doc._id,
          field1: doc.name,
          // field2: doc.requirement.positions.join(' '),
          // field3: doc.requirement.skills.join(' '),
          // skills, organization, positions
        }
        return newDoc;
      })
      break;
    default:
      break;
  }
  return toIndexDocuments;
}

/* 
  Mục đích: Index văn bản
  Input: Loại văn bản (user, recruitment, organization)
*/
const Indexing = (type) => {
  console.log("Search: Start Indexing type", type);
  var {Collection, populateOption, TermDictionary, NumOfFields} = selectCollections(type);
  // Tìm tất cả các văn bản trong Collection
  Collection
    .find({})
    .populate(populateOption)
    .exec((err, documents) => {
      if (err) throw err;
      // Tập hợp các trường cần index tùy vào type (user, recruitment, organization)
      const toIndexDocuments = mappingDocuments(documents, type);
      // Xây dựng term dictionary
      const dictionary = buildTermDictionary(toIndexDocuments, NumOfFields);
      
      // Lưu TermDictionary vào cơ sở dữ liệu
      TermDictionary
        .remove({})
        .exec((err) => {
          if (err) throw err;
          async.each(
            dictionary,
            (item, next) => {
              const word = new TermDictionary({
                text: item.text,
                docFrequency: item.docFrequency,
                inDocuments: item.inDocuments
              });
              word.save((err) => {
                if (err) throw err;
                next();
              })
            },
            (err) => {
              if (err) throw err;
              console.log("Search: Done Indexing type", type);
            }
          )
        })
    })
}


/* 
  Mục đích: Hợp tất cả các kết quả tìm được
  Input: terms: bảng từ khóa của người dùng nhập sau khi được analysis
  Output: Tất cả các kết quả có terms xuất hiện trong nội dung
*/
const BooleanOR = (terms) => {
  const all = [];
  // thêm tất cả các văn bản vào bảng all
  for (var i = 0; i < terms.length; i++) {
    for (var j = 0; j < terms[i].inDocuments.length; j++) {
      const item = terms[i].inDocuments[j].id;

      all.push(item);
    }
  }
  // rút gọn tất cả các kết quả trùng thành 1 kết quả
  const union = _.uniqWith(all, (a, b) => {
    return a.toString() === b.toString()
  })
  return union;
}
/* 
  Mục đích: Xếp hạng kết quả tìm kiếm
  Input: 
   - terms: Mảng từ khóa
   - results: Kết quả tìm kiếm
   - totalDocuments: Tổng số văn bản
  Output: Mảng kết quả đã được xếp hạng
*/ 
const ranking = (terms, results, totalDocuments) => {
  const relevantList = [];
  for (var i = 0; i < results.length; i++) {
    const existedTerms = [];
    for (var j = 0; j < terms.length; j++) {
      for (var k = 0; k < terms[j].inDocuments.length; k++) {
        if (results[i].toString() === terms[j].inDocuments[k].id.toString()) {
          existedTerms.push(terms[j].text);
          var isExisted = false;
          var marker = NOT_EXISTED;
          for (var h = 0; h < relevantList.length; h++) {
            if (results[i].toString() === relevantList[h].id.toString()) {
              isExisted = true;
              marker = h;
              break;
            }
          }
          // thuật toán tf_idf
          const tf_idf = terms[j].inDocuments[k].maxScore * math.log(1 + totalDocuments / terms[j].docFrequency, 10);
          if (!isExisted) {
            relevantList.push({
              id: results[i],
              relevantScore: tf_idf
            })
          }
          else {
            relevantList[marker].relevantScore += tf_idf;
          }
        }
      }
    }
    // Nhân số điểm tính được với số từ khớp với query string mà văn bản đó có
    relevantList.forEach((elem) => {
      if (elem.id.toString() === results[i].toString()) {
        elem.existedTerms = existedTerms;
        elem.relevantScore *= existedTerms.length;
      }
    })
  }
  return relevantList;
}

// const terms = nlp("Nhân Giang Băng Khí");
// console.log(terms);

const sortHitsBasedOnSortMode = (hits, sortMode) => {
  var sortedHits = [];
  switch (sortMode) {
    case SORT_TYPES.RELEVANT:
      sortedHits = hits.sort((a, b) => {
        if (b.relevantScore === a.relevantScore) {
          return new Date(b.updatedDate).getTime() - new Date(a.updatedDate).getTime();
        }
        return b.relevantScore - a.relevantScore;
      })
      break;
    case SORT_TYPES.TRENDING:
      sortedHits = countViewPerPeriod(hits);
      sortedHits = hits.sort((a, b) => b.viewPerDay - a.viewPerDay);
      break;
    case SORT_TYPES.NEWEST:
      sortedHits = hits.sort((a, b) => new Date(b.updatedDate).getTime() - new Date(a.updatedDate).getTime());
      break;
    default:
      sortedHits = [];
      break;
  }
  return sortedHits;
}
const retrieveDocuments = (arrayOfWords, options, populateOptions, page, PAGE_SIZE, res, type, sortMode = '') => {
  var {Collection, TermDictionary} = selectCollections(type);
  TermDictionary
    .find({ text: { $in: arrayOfWords } })
    .exec((err, terms) => {
      if (err) throw err;
      
      Collection.count({}, (err, totalDocuments) => {
        const union = BooleanOR(terms);
        const relevantList = ranking(terms, union, totalDocuments);
        const ids = relevantList.map((elem) => elem.id);
        options['_id'] = { $in: ids };
        console.log('OPTIONS:', options);
        Collection
          .find(options)
          .lean()
          .populate(populateOptions)
          .exec(async  (err, hits) => {
            if (err) throw err;
            console.log('HITS: ', hits);
            hits.forEach((elem) => {
              relevantList.forEach((item) => {
                if (elem._id.toString() === item.id.toString()) {
                  elem.relevantScore = item.relevantScore;
                }
              })
            })

            // total
            const total = hits.length;
            
            // sort
            var sortedHits = sortHitsBasedOnSortMode(hits, sortMode);     
            
            //paging
            const firstIndex = page * PAGE_SIZE, lastIndex = page * PAGE_SIZE + PAGE_SIZE;
            sortedHits = sortedHits.slice(firstIndex, lastIndex);

            // additional field -------------------
            try {
              if (type === TYPES.RECRUITMENT) {
                for (let r of sortedHits) {
                  const district = await District.findOne({district_id: r.location.district});
                  const province = await Province.findOne({province_id: r.location.province});
                  r.location.district = (district)? `${district.type} ${district.name}`: '';
                  r.location.province = (province)? `${province.type} ${province.name}`: '';
                }
              }
              else if (type === TYPES.USER) {
                for (let u of sortedHits) {
                  const district = await District.findOne({district_id: u.address.district});
                  const province = await Province.findOne({province_id: u.address.province});
                  const field = await Field3.findOne({code: u.jobField})
                  u.fieldName = field;
                  u.address.district = (district)? `${district.type} ${district.name}`: '';
                  u.address.province = (province)? `${province.type} ${province.name}`: '';
                }
              }
              else if (type === TYPES.ORGANIZATION) {
                for (let o of sortedHits) {
                  const district = await District.findOne({district_id: o.location.district});
                  const province = await Province.findOne({province_id: o.location.province});
                  const fields = await Field3.find({code: {$in: o.fields}});
          
                  o.fieldNames = fields;
                  o.location.district = (district)? `${district.type} ${district.name}`: '';
                  o.location.province = (province)? `${province.type} ${province.name}`: '';
                }
              }
              // -----------------
              res.json({/* terms, union,*/ 
                  relevantList, 
                  total, 
                  sortedHits,
                  page,
                  elemCount: sortedHits.length
                }
              );  
            } catch (error) {
              console.log(error);
              res.status(500).send();
            }
            
          })
      });
    })
}
Indexing(TYPES.RECRUITMENT);
Indexing(TYPES.USER);
Indexing(TYPES.ORGANIZATION);
module.exports = {
  
  searchRecruitments: (req, res) => {
    const query = req.query.searchStr;
    const { page, PAGE_SIZE, sortMode } = req.query;
    const body = req.body;
    var arrayOfWords = [];
    if (query) {
      arrayOfWords = nlp(query);
    }
    console.log(arrayOfWords);
    console.log(sortMode);
    const options = {
      'requirement.minSalary': {$lte: body.salary[1]},
      'requirement.maxSalary': {$gte: body.salary[0]},
      'requirement.minExperience': { $gte: body.experience },
    }
    if (!_.isEmpty(body.fields)) options['requirement.fieldName'] = { $elemMatch: {$in: body.fields} };
    if (!_.isEmpty(body.jobType)) options['requirement.jobType'] = { $in: body.jobType };
    if (!_.isEmpty(body.degree) && !_.includes(body.degree, NOT_REQUIRED)) options['requirement.atLeastDegree'] = { $in: body.degree };
    if (!_.isEmpty(body.gender) && !_.includes(body.gender, NOT_REQUIRED)) options['requirement.gender'] = { $in: body.gender };
    if (!_.isEmpty(body.province)) options['location.province'] = { $in: body.province };
    const r_populateOptions = {
      path: '_organization.id',
      populate: {path: '_banner _logo'}
    }
    if (sortMode === SORT_TYPES.NEWEST) {
      retrieveDocuments(arrayOfWords, options, r_populateOptions, Number(page), Number(PAGE_SIZE), res, TYPES.RECRUITMENT, sortMode);
    }
    else {
      const threshold = calculateThreshold();
      if (sortMode === SORT_TYPES.TRENDING) {
        options['view.lastModified'] = { $gte: threshold };
      }
      retrieveDocuments(arrayOfWords, options, r_populateOptions, Number(page), Number(PAGE_SIZE), res, TYPES.RECRUITMENT, sortMode);
    }
    
  },
  searchUsers: (req, res) => {
    const query = req.query.searchStr;
    const { page, PAGE_SIZE, sortMode } = req.query;
    const body = req.body;
    var arrayOfWords = [];
    if (query) {
      arrayOfWords = nlp(query);
    }
    const options = {
      // 'requirement.minSalary': {$lte: body.salary[1]},
      // 'requirement.maxSalary': {$gte: body.salary[0]},
      'experience': { $gte: body.experience },
    }
    if (body.isJobSearchOn) options['jobSearch.isOn'] = body.isJobSearchOn;
    if (!_.isEmpty(body.fields)) options['jobField'] = { $in: body.fields } ;
    if (!_.isEmpty(body.degree) && !_.includes(body.degree, NOT_REQUIRED)) options['educationLevel'] = { $in: body.degree };
    if (!_.isEmpty(body.gender) && !_.includes(body.gender, NOT_REQUIRED)) options['gender'] = { $in: body.gender };
    if (!_.isEmpty(body.province)) options['address.province'] = { $in: body.province };
    const u_populateOptions = '_languages _skills _avatar';
    
    if (sortMode === SORT_TYPES.NEWEST) {
      retrieveDocuments(arrayOfWords, options, u_populateOptions, Number(page), Number(PAGE_SIZE), res, TYPES.USER, sortMode);
    }
    else {
      const threshold = calculateThreshold();
      if (sortMode === SORT_TYPES.TRENDING) {
        options['view.lastModified'] = { $gte: threshold };
      }
      retrieveDocuments(arrayOfWords, options, u_populateOptions, Number(page), Number(PAGE_SIZE), res, TYPES.USER, sortMode);
    }
  },
  searchOrganizations: (req, res) => {
    const query = req.query.searchStr;
    const { page, PAGE_SIZE, sortMode } = req.query;
    const body = req.body;
    var arrayOfWords = [];
    if (query) {
      arrayOfWords = nlp(query);
    }
    const options = {};

    if (!_.isEmpty(body.fields)) options['fields'] = { $elemMatch: {$in: body.fields} };
    if (!_.isEmpty(body.type)) options['type'] = { $in: body.type };
    if (!_.isEmpty(body.province)) options['location.province'] = { $in: body.province };
    const o_populateOptions = '_logo _banner';

    if (sortMode === SORT_TYPES.NEWEST) {
      retrieveDocuments(arrayOfWords, options, o_populateOptions, Number(page), Number(PAGE_SIZE), res, TYPES.ORGANIZATION, sortMode);
    }
    else {
      const threshold = calculateThreshold();
      if (sortMode === SORT_TYPES.TRENDING) {
        options['view.lastModified'] = { $gte: threshold };
      }
      retrieveDocuments(arrayOfWords, options, o_populateOptions, Number(page), Number(PAGE_SIZE), res, TYPES.ORGANIZATION, sortMode);
    }
  },
  advancedSearch: (req, res) => {
    const query = req.query.searchStr;
    const { page, PAGE_SIZE, options } = req.query;
    const arrayOfWords = nlp(query);
    retrieveDocuments(arrayOfWords, options, page, PAGE_SIZE, res);
  },
}