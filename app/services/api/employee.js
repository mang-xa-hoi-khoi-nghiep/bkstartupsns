const Employee = require('../models/Organization/Employee');
const _ = require('lodash');
const async = require('async');

module.exports = {
  update: async (req, res) => {
    try {
      const { id } = req.params
      const updatedEmployee = req.body
      await Employee.findByIdAndUpdate(id, updatedEmployee);
      res.json({success: true});
    } catch (error) {
      console.log(error);
      res.status(500).send({success: false});
    }
    
  },
  delete: async (req, res) => {
    try {
      const { id } = req.params;
      await Employee.findByIdAndRemove(id);
      res.json({success: true});
    } catch (error) {
      console.log(error);
      res.status(500).send({success: false});
    }
  }
}
