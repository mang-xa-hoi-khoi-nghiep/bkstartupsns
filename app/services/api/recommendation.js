const math = require('mathjs');
const ObjectId = require('mongodb').ObjectId;
const Comment = require('../models/Comment');
const SimCampaign = require('../models/SimCampaign');
const Campaign = require('../models/Campaign');
const _ = require('lodash');
const async = require('async');
const User = require('../models/User/User');
const Correlation = require('node-correlation');
const QUALITY_VIEW = {
  TIME_PERIOD: 10000,
  THRESHOLD_DATE: 86400000 * 30 // 1 month
}
// const convertToObjectId = () => {
//   Comment
//   .find({campaign: {$exists: true}})
//   .exec((err, results) => {
//     if (err) throw err
//     results.forEach((obj) => {
//       obj.campaign = ObjectId(obj.campaign);
//       const newObj = new Comment(obj);
//       newObj.save();
//     })
//   });
// }
// convertToObjectId();
const printMatrix = (matrix) => {
  if (_.isArray(matrix)) {
    for (var i = 0; i < matrix.length; i++) {
      var row = '';
      for (var j = 0; j < matrix[i].length; j++) {
        row += (matrix[i][j] + ' ');
      }
      console.log(row);
    }
  }
  else {
    console.log(matrix);
  }

}
const buildItemUserMatrix = (users, campaigns) => {
  var itemUserMatrix = [];
  var itemUserMatrixLabel = [];
  for (var i = 0; i < campaigns.length; i++) {
    itemUserMatrix[i] = [];
    itemUserMatrixLabel[i] = [];
    for (var j = 0; j < users.length; j++) {
      itemUserMatrix[i][j] = 0;
      itemUserMatrixLabel[i][j] = {
        campaign: campaigns[i]._id,
        user: users[j]._id
      }
      for (var k = 0; k < users[j]._comments.length; k++) {
        if (campaigns[i]._id.toString() === users[j]._comments[k].campaign.toString()) {
          itemUserMatrix[i][j] = users[j]._comments[k].rating;
          break;
        }
      }
    }
  }
  return { itemUserMatrix, itemUserMatrixLabel }
}
const buildHistoryItemUserMatrix = (users, campaigns) => {
  const itemUserMatrix = [];
  const POINT = 1;
  for (var i = 0; i < users.length; i++) {
    var temp = [users[i]._history[0]]
    for (var j = 1; j < users[i]._history.length; j++) {
      var isExisted = false;
      for (var k = 0; k < temp.length; k++) {
        if (temp[k].campaign.toString() === users[i]._history[j].campaign.toString()) {
          temp[k].point += POINT;
          isExisted = true;
        }
      }
      if (!isExisted) {
        temp.push(users[i]._history[j]);
      }
    }
    users[i]._history = temp;
    temp = [];
  }

  for (var i = 0; i < campaigns.length; i++) {
    itemUserMatrix[i] = [];
    for (var j = 0; j < users.length; j++) {
      itemUserMatrix[i][j] = 0;
      for (var k = 0; k < users[j]._history.length; k++) {

        if (users[j]._history[k] && campaigns[i]._id.toString() === users[j]._history[k].campaign.toString()) {
          itemUserMatrix[i][j] = users[j]._history[k].point;
          break;
        }
      }
    }
  }
  return itemUserMatrix;
}
const Normalization = (matrix) => {
  const denominators = []
  const normalizedMatrix = [];
  for (var i = 0; i < matrix.length; i++) {
    var total = 0;
    for (var j = 0; j < matrix[i].length; j++) {
      total += Math.pow(matrix[i][j], 2);
    }
    const denominator = Math.sqrt(total);
    denominators.push(denominator);
  }


  for (var i = 0; i < matrix.length; i++) {
    normalizedMatrix[i] = [];
    for (var j = 0; j < matrix[i].length; j++) {
      normalizedMatrix[i][j] = Number((matrix[i][j] / denominators[i]).toFixed(4));
      if (denominators[i] === 0) {
        normalizedMatrix[i][j] = 0;
      }
    }
  }
  return normalizedMatrix;
}

const buildItemItemMatrix = (matrix, matrixLabel) => {
  const ItemItemMatrix = [];
  const ItemItemMatrixLabel = [];
  for (var i = 0; i < matrix.length; i++) {
    ItemItemMatrix[i] = [];
    ItemItemMatrixLabel[i] = [];
    for (var j = 0; j < matrix.length; j++) {
      ItemItemMatrixLabel[i][j] = {
        Row_Campaign: matrixLabel[i][0].campaign,
        Col_Campaign: matrixLabel[j][0].campaign
      }
      if (i === j) {
        ItemItemMatrix[i][j] = 1;
      }
      else {
        ItemItemMatrix[i][j] = Number(math.dot(matrix[i], matrix[j]).toFixed(4));
      }
    }
  }
  return { ItemItemMatrix, ItemItemMatrixLabel };
}

const AttributesMatrix = (campaigns) => {
  const weightCategory = 0.75, weightStage = 0.25;

  const attributesMatrix = [];
  const attributesMatrixLabel = [];
  for (var i = 0; i < campaigns.length; i++) {
    attributesMatrix[i] = [];
    attributesMatrixLabel[i] = []
    for (var j = 0; j < campaigns.length; j++) {
      attributesMatrixLabel[i][j] = {
        Row_Campaign: campaigns[i]._id,
        Col_Campaign: campaigns[j]._id
      }
      attributesMatrix[i][j] = 0;
      if (campaigns[i].categories === campaigns[j].categories) {
        attributesMatrix[i][j] = weightCategory;
      }
      if (campaigns[i].stage === campaigns[j].stage) {
        attributesMatrix[i][j] += weightStage;
      }
    }
  }
  return { attributesMatrix, attributesMatrixLabel };
}
const buildCombinedMatrixLabel = (matrixLabel, combinedMatrix) => {
  const combinedMatrixLabel = [];
  for (var i = 0; i < combinedMatrix.length; i++) {
    combinedMatrixLabel[i] = [];
    for (var j = 0; j < combinedMatrix[i].length; j++) {
      const item = {
        Row_Campaign: matrixLabel[i][j].Row_Campaign,
        Col_Campaign: matrixLabel[i][j].Col_Campaign,
        similarity: combinedMatrix[i][j],
      }
      combinedMatrixLabel[i][j] = item;
    }
  }
  return combinedMatrixLabel;
}
// recommend campaign to user
// recommend user to user
// recommend user to recruitment news // content
const buildSimilarList = () => {
  User
    .find()
    .populate('_comments')
    .populate('_history')
    .exec(((err, users) => {
      if (err) throw err;
      Campaign
        .find()
        .populate('_comments')
        .exec((err, campaigns) => {
          if (err) {
            throw err;
          }
          // rating matrix
          const itemUser = buildItemUserMatrix(users, campaigns);
          const itemUserMatrix = itemUser.itemUserMatrix;
          const itemUserMatrixLabel = itemUser.itemUserMatrixLabel;
          printMatrix(itemUser.itemUserMatrix);
          const itemUserNormalizedMatrix = Normalization(itemUserMatrix);

          const itemItem = buildItemItemMatrix(itemUserNormalizedMatrix, itemUserMatrixLabel);
          const itemItemMatrix = itemItem.ItemItemMatrix;
          const itemItemMatrixLabel = itemItem.ItemItemMatrixLabel;

          // history matrix
          // const itemUserHistory = buildHistoryItemUserMatrix(users, campaigns);
          // const itemUserHistoryNormalizedMatrix = Normalization(itemUserHistory);
          // const itemItemHistoryMatrix = buildItemItemMatrix(itemUserHistoryNormalizedMatrix, itemUserMatrixLabel).ItemItemMatrix

          // attributes matrix
          const attributes = AttributesMatrix(campaigns);
          const attributesMatrix = attributes.attributesMatrix;
          const attributesMatrixLabel = attributes.attributesMatrixLabel;
          printMatrix(attributesMatrix)
          // collaborative filtering matrix
          const weightRating = 1 //, weightHistory = 0.25;
          const weightedRatingMatrix = math.multiply(itemItemMatrix, weightRating);
          //const weightedHistoryMatrix = math.multiply(itemItemHistoryMatrix, weightHistory);
          //const combinedCFMatrix = math.add(weightedRatingMatrix, weightedHistoryMatrix);



          const weightAttributes = 0.5, weightCF = 0.5;
          const weightedCFMatrix = math.multiply(weightedRatingMatrix, weightCF);
          const weightedAttributesMatrix = math.multiply(attributesMatrix, weightAttributes);

          // combined attributes matrix and collaborative filtering matrix
          const combinedMatrix = math.add(weightedCFMatrix, weightedAttributesMatrix);
          const combinedMatrixLabel = buildCombinedMatrixLabel(itemItemMatrixLabel, combinedMatrix);




          SimCampaign
            .remove({})
            .exec(async (err) => {
              if (err) throw err;
              try {
                console.log('Recommendation: Start indexing sim campaigns');
                for (const row of combinedMatrixLabel) {
                  const newRow = new SimCampaign({
                    campaign_id: ObjectId(row[0].Row_Campaign),
                    listOfCampaigns: row
                  })
                  await newRow.save();
                }
                console.log('End indexing sim campaigns');
              } catch (error) {
                console.log(error);
              }
            })
        })
    }))
}
buildSimilarList();
const simCandidates = (personalCampaigns, simCampaigns) => {
  const temp = [];
  const ranking = [];
  // multiply campaign similarity with user personal rating of that campaign
  for (var i = 0; i < personalCampaigns.length; i++) {
    for (var j = 0; j < simCampaigns.length; j++) {
      if (simCampaigns[j].campaign_id.toString() === personalCampaigns[i].campaignId.toString()) {
        for (var k = 0; k < simCampaigns[j].listOfCampaigns.length; k++) {
          simCampaigns[j].listOfCampaigns[k].similarity *= personalCampaigns[i].rating;
        }
      }
    }
  }

  // flaten data to an 2 dimensional array
  for (var i = 0; i < simCampaigns.length; i++) {
    for (var j = 0; j < simCampaigns[i].listOfCampaigns.length; j++) {
      var element = simCampaigns[i].listOfCampaigns[j];
      const item = {
        campaign: element.Col_Campaign,
        similarity: element.similarity
      }
      temp.push(item);
    }
  }

  // sum all similarity after multiply
  ranking.push(temp[0]);
  for (var i = 1; i < temp.length; i++) {
    if (isExisted(ranking, temp[i])) {
      for (var j = 0; j < ranking.length; j++) {
        if (ranking[j].campaign.toString() === temp[i].campaign.toString()) {
          ranking[j].similarity += temp[i].similarity;
        }
      }
    }
    else {
      ranking.push(temp[i]);
    }
  }

  //filter out all campaigns existed in user personal rating campaign list
  try {
    const final_ranking =
      ranking.filter((elem) => {
        var isExisted = false;
        for (var i = 0; i < personalCampaigns.length; i++) {
          var item = personalCampaigns[i];
          if (item.campaignId.toString() === elem.campaign.toString()) {
            isExisted = true;
          }
        }
        if (!isExisted) {
          return elem;
        }
      })
        .sort((a, b) => {
          return b.similarity - a.similarity;
        })


    return final_ranking;
  } catch (error) {
    return [];
  }

}
const isExisted = (arr, elem) => {
  for (var i = 0; i < arr.length; i++) {
    if (elem.campaign.toString() === arr[i].campaign.toString()) {
      return true;
    }
  }
  return false;
}
const combineRanking = (first, second) => {
  const temp = first.concat(second);
  const combinedRanking = [temp[0]];
  for (var i = 1; i < temp.length; i++) {
    var isExisted = false;
    for (var j = 0; j < combinedRanking.length; j++) {
      if (combinedRanking[j].campaign.toString() === temp[i].campaign.toString()) {
        isExisted = true;
        marker = j;
        break;
      }
    }
    if (!isExisted) {
      combinedRanking.push(temp[i]);
    }
    else {
      combinedRanking[marker].similarity = (combinedRanking[marker].similarity + temp[i].similarity) / 2;
    }
  }

  return combinedRanking;
}
//Recommendation();

module.exports = {
  getRecommendCampaigns: (req, res) => {

    var { list } = req.body;
    list = JSON.parse(list);
    const ids = list.map((elem) => {
      return ObjectId(elem.id);
    })

    Campaign
      .find({ _id: { $in: ids } })
      .lean()
      .populate('_author')
      .exec((err, recommendCampaigns) => {
        if (err) {
          throw err;
        }

        list.forEach((item) => {
          recommendCampaigns.forEach((campaign) => {
            if (item.id.toString() === campaign._id.toString()) {
              campaign.similarity = item.similarity;
            }
          })
        })
        recommendCampaigns.sort((a, b) => b.similarity - a.similarity);
        res.json({ success: true, recommendCampaigns });
      })
  },
  recommendCampaignsToUser: (req, res) => {

    const userId = req.decoded._id;
    const pageSize = Number(req.query.pageSize);
    User
      .findOne({ _id: userId })
      .populate('_comments')
      .populate({
        path: '_history',
        match: { type: 'campaign' }
      })
      .exec((err, user) => {
        if (err) throw err;

        var personalRatingCampaigns =
          user._comments
            .map((elem) => {
              return {
                rating: elem.rating,
                campaignId: elem.campaign,
              }
            })

        // remove duplicate in rating
        personalRatingCampaigns = _.uniqWith(personalRatingCampaigns, (a, b) => {
          return a.campaignId.toString() === b.campaignId.toString()
        })

        const ratingIds = personalRatingCampaigns.map((elem) => {
          return elem.campaignId;
        })

        var personalViewCampaigns = user._history.map((elem) => {
          var rating = elem.point;
          console.log(rating);
          if (elem.timePeriod >= QUALITY_VIEW.TIME_PERIOD) {
            rating++;
          }
          if (Date.now() - new Date(elem.viewAt).getTime() <= QUALITY_VIEW.THRESHOLD_DATE) {
            rating++;
          }
          return {
            rating,
            campaignId: elem.campaignId,
          }
        })
        const viewIds = personalViewCampaigns.map(elem => elem.campaignId);

        SimCampaign
          .find({ campaign_id: { $in: ratingIds } })
          .exec((err, results) => {
            if (err) throw err;
            SimCampaign
              .find({ campaign_id: { $in: viewIds } })
              .exec((err, viewResults) => {
                if (err) throw err;
                const viewRanking = simCandidates(personalViewCampaigns, viewResults);

                console.log('View: ', viewRanking);
                const ratingRanking = simCandidates(personalRatingCampaigns, results)


                const combinedRanking = combineRanking(ratingRanking, viewRanking);

                var final_ids = [];
                try {
                  final_ids = ratingRanking.map(elem => elem.campaign);
                } catch (error) { }


                Campaign
                  .find({ _id: { $in: final_ids } })
                  .lean()
                  .populate('_author')
                  .exec((err, sim) => {
                    if (err) throw err;

                    sim.forEach((elem) => {
                      ratingRanking.forEach((item) => {
                        if (elem._id.toString() === item.campaign.toString()) {
                          elem.similarity = item.similarity;
                        }
                      })
                    })
                    const sortedSim = sim.sort((a, b) => {
                      return b.similarity - a.similarity;
                    }).slice(0, pageSize)

                    res.json({ success: true, simCandidates: sortedSim });
                  })
              })
          })
      })
  }
}