const Invitation = require('../models/Invitation');
const C_Invitation = require('../libs/invitation').Invitation;
module.exports = {
  pushInvitation: async (req, res) => {
    try {
      const userId = req.decoded._id;
      console.log(userId);
      await Invitation.updateMany({receiver: userId, isPushed: false}, {$set: {isPushed: true}});
      res.json({success: true});
    } catch (error) {
      console.error(error);
      res.status(500).send(error);
    }
  },
  getInvitation: async (req, res) => {
    try {
      const userId = req.decoded._id;
      const invitations = await Invitation.find({receiver: userId, isPushed: true}).populate('sender', 'firstname lastname');
      res.json({invitations});
    } catch (error) {
      console.log(error);
      res.status(500).send(error);
    }
  },
  create: async (req, res) => {
    try {
      const {sender, receiver, content, url, type} = req.body;
      const invitation = new C_Invitation(sender, receiver, content, url, type);
      invitation.send();
      res.json({success: true});
    } catch (error) {
      console.error(error);
      res.status(500).send();
    }
  },
  update: async (req, res) => {
    try {
      const {id, isAccepted} = req.body;
      await C_Invitation.update(id, isAccepted);
      console.log("Update done");
      res.json({success: true});
    } catch (error) {
      console.log(error);
      res.status(500).send({success: false});
    }
  },
  check: async (req, res) => {
    try {
      const id = req.body.id;
      const updated = await Invitation.findOneAndUpdate({_id: id}, {$set: {isChecked: true}}, {new: true}).populate('sender', 'firstname lastname');
      res.json({updated});
    } catch (error) {
      console.log(error);
      res.status(500).send(error);
    }
  },
  checkAll: async (req, res) => {
    try {
      const receiverId = req.decoded._id;
      await Invitation.updateMany({receiver: receiverId, isChecked: false}, {$set: {isChecked: true}});
      const invitations = await Invitation.find({receiver: receiverId, isPushed: true}).populate('sender', 'firstname lastname');;
      res.json({invitations});
    } catch (error) {
      console.log(error);
      res.status(500).send(error);
    }
  }
}