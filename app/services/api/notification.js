const Notification = require('../models/Notification');

module.exports = {
  pushNotification: async (req, res) => {
    try {
      const userId = req.decoded._id;
      console.log(userId);
      await Notification.updateMany({receiver: userId, isPushed: false}, {$set: {isPushed: true}});
      res.json({success: true});
    } catch (error) {
      console.error(error);
      res.status(500).send(error);
    }
  },
  getNotification: async (req, res) => {
    try {
      const userId = req.decoded._id;
      const notifications = await Notification.find({receiver: userId, isPushed: true}).populate('sender', 'firstname lastname');
      res.json({notifications});
    } catch (error) {
      console.log(error);
      res.status(500).send(error);
    }
  },
  create: async (req, res) => {
    try {
      const body = req.body;
      const notification = new Notification(body);
      await notification.save();  
      res.json({success: true});
    } catch (error) {
      console.error(error);
      res.status(500).send(error);
    }
  },
  check: async (req, res) => {
    try {
      const id = req.body.id;
      const updated = await Notification.findOneAndUpdate({_id: id}, {$set: {isChecked: true}}, {new: true}).populate('sender', 'firstname lastname');
      res.json({updated});
    } catch (error) {
      console.log(error);
      res.status(500).send(error);
    }
  },
  checkAll: async (req, res) => {
    try {
      const receiverId = req.decoded._id;
      await Notification.updateMany({receiver: receiverId, isChecked: false}, {$set: {isChecked: true}});
      const notifications = await Notification.find({receiver: receiverId, isPushed: true}).populate('sender', 'firstname lastname');
      res.json({notifications});
    } catch (error) {
      console.log(error);
      res.status(500).send(error);
    }
  }
}