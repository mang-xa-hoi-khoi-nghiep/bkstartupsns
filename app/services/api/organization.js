const Organization = require('../models/Organization/Organization');
const Employee = require('../models/Organization/Employee');
const Product = require('../models/Organization/Product');
const Event = require('../models/Organization/Event');
const Field3 = require('../models/Field/Field3');
const District = require('../models/District');
const Province = require('../models/Province');
const {Invitation, INVITATION_TYPES, ACCEPTED_TYPES} = require('../libs/invitation');
const M_Invitation = require('../models/Invitation');
const _ = require('lodash');
const async = require('async');
const { TRENDING_TYPES } = require('./user');
const { trending } = require('../libs/trending');
const { SORT_TYPES } = require('../libs/constants');

module.exports = {
  getOne: async (req, res) => {
    try {
      const { id } = req.params;
      const organization = 
      await Organization
      .findOne({_id: id})
      .lean()
      .populate('_products _events _banner _logo _recruitments')
      .populate({
        path: '_employees',
        match: {isAccepted: ACCEPTED_TYPES.ACCEPTED},
        populate: {
          path: '_employee',
        }
      })
      const fields = await Field3.find({code: {$in: organization.fields}});
      organization.fieldNames = fields;
      if (!req.query.mode) {
        const district = await District.findOne({district_id: organization.location.district});
        const province = await Province.findOne({province_id: organization.location.province});

        
        organization.location.district = (district)? `${district.type} ${district.name}`: '';
        organization.location.province = (province)? `${province.type} ${province.name}`: '';
      }
      res.json({organization});
    } catch (error) {
      console.log(error);
      res.status(500).send();
    }
  },
  getBasedOnUser: async (req, res) => {
    try {
      const userId = req.decoded._id;
      const organizations = await Organization.find({_author: userId}).populate('_employees _products _events _banner _logo');
      res.json({organizations});
    } catch (error) {
      console.log(error);
      res.status(500).send();
    }
  },
  filter: async (req, res) => {
    try {
      const { page, PAGE_SIZE } = req.query;
      const query = req.query;
      const body = req.body;
      var organizations;
      const options = {};

      if (!_.isEmpty(body.fields)) options['fields'] = { $elemMatch: {$in: body.fields} };
      if (!_.isEmpty(body.type)) options['type'] = { $in: body.type };
      if (!_.isEmpty(body.province)) options['location.province'] = { $in: body.province };
      
      const populateOptions = '_logo _banner';
      if (query.sortMode === SORT_TYPES.NEWEST) {
        
        organizations = await Organization.find(options).populate(populateOptions).lean().sort('-updatedDate')
        .skip(Number(page*PAGE_SIZE)).limit(Number(PAGE_SIZE));
        const total = await Organization.count(options);
        for (let o of organizations) {
          const district = await District.findOne({district_id: o.location.district});
          const province = await Province.findOne({province_id: o.location.province});
          const fields = await Field3.find({code: {$in: o.fields}});
  
          o.fieldNames = fields;
          o.location.district = (district)? `${district.type} ${district.name}`: '';
          o.location.province = (province)? `${province.type} ${province.name}`: '';
        }
        res.json({organizations, page: Number(page), total, elemCount: organizations.length});  
      }
      else if (query.sortMode === SORT_TYPES.TRENDING) {
        trending(req, res, options, Organization, TRENDING_TYPES.ORGANIZATION, populateOptions);
      }
      
    } catch (error) {
      console.log(error);
      res.status(500).send();
    }
  },
  create: async (req, res) => {
    try {
      if (!req.decoded) {
        res.json({success: false});
        return;
      }
      const body = req.body;
      body._author = req.decoded._id;
      const newOrganization = new Organization(body);
      await newOrganization.save();
      res.json({organizationId: newOrganization._id});
    } catch (error) {
      console.log(error);
      res.status(500).send({success: false});
    }
  },
  update: async (req, res) => {
    try {
      const { id } = req.params
      const updatedOrganization = req.body
      console.log(updatedOrganization);
      await Organization.findByIdAndUpdate(id, {
        $set: updatedOrganization
      });
      res.json({success: true});
    } catch (error) {
      console.log(error);
      res.status(500).send({success: false});
    }
    // console.log("api");
    // const { id } = req.params;
    // const obj = req.body;
    // Organization.findByIdAndUpdate(id, obj, function(err, updated) {
    //   if (err) {
    //     res.json({success: false});
    //     throw err;
    //   }
    //   else {
    //     res.json({success: true});
    //   }
    // })
  },
  delete: async (req, res) => {
    
  },
  viewCount: async (req, res) => {
    try {
      const { id } = req.body;
      const option = {
        $inc: { 'view.total': 1 },
        $currentDate: { 'view.lastModified': true },
        $push: { 'view.at': new Date() }
      }
      const organization = await Organization.findOneAndUpdate({_id: id}, option);
      res.json({organization});
    } catch (error) {
      console.log(error);
      res.status(500).send({success: false, error})
    }
  },
  createEmployee: async (req, res) => {
    try {
      if (!req.decoded) {
        res.json({success: false, message: "Chưa đăng nhập"});
        return;
      }
      const body = req.body;
      body._author = req.decoded._id;
  
      if (!body._employee) {
        const newEmployee = new Employee(body);
        await newEmployee.save();
        res.json({success: true});
        return;  
      }

      const employee = await Employee.findOne(
        {_employee: body._employee, _organization: body._organization}
      )
      const organization = await Organization.findOne({_id: body._organization});
      if (employee && employee.isAccepted === ACCEPTED_TYPES.ACCEPTED) {
        res.json({success: false, message: "Thành viên đã tồn tại trong tổ chức"});
        return;
      }
      if (employee && employee.isAccepted === ACCEPTED_TYPES.UNKNOWN) {
        
        const invitation = await M_Invitation.findOne(
          {receiver: employee._employee, _organization: employee._organization, isAccepted: ACCEPTED_TYPES.UNKNOWN}
        );
        if (invitation) {
          res.json({success: false, message: "Lời mời đã được gửi đi trước đó. Xin chở phản hồi"});
          return;
        }

      }
      if (employee && employee.isAccepted === ACCEPTED_TYPES.REJECTED) {
        await Employee.findOneAndUpdate(
          {_employee: employee._employee, _organization: employee._organization},
          {isAccepted: ACCEPTED_TYPES.UNKNOWN}
        )
        const invitation = new Invitation(
          req.decoded._id, 
          employee._employee, 
          `Đã gửi lời mời tham gia tổ chức ${organization.name}`,
          '',
          INVITATION_TYPES.EMPLOYEE,
          employee._organization
        )
        invitation.send();
        res.json({success: true});
        return;
      }

      const newEmployee = new Employee(body);
      await newEmployee.save();
      const invitation = new Invitation(
        req.decoded._id, 
        body._employee,
        `Đã gửi lời mời tham gia tổ chức ${organization.name}`,
        '',
        INVITATION_TYPES.EMPLOYEE,
        body._organization
      )
      invitation.send();
      res.json({success: true});
    } catch (error) {
      console.log(error);
      res.status(500).send({success: false})
    }
  },
  getEmployee: async (req, res) => {
    try {
      const {id} = req.params;
      const employee = await Employee.findById(id).populate('_employee');
      res.json({employee});
    } catch (error) {
      console.log(error);
      res.status(500).send({success: false})
    }
  },
  updateEmployee: async (req, res) => {
    try {
      const {id} = req.params;
      const body= req.body;
      await Employee.findByIdAndUpdate(id, {$set: {position: body.position, description: body.description}})
      res.json({success: true});
    } catch (error) {
      console.log(error);
      res.status(500).send({success: false})
    }
  },
  deleteEmployee: async (req, res) => {
    try {
      const {id} = req.params;
      await Employee.findByIdAndRemove(id);
      res.json({success: true});
    } catch (error) {
      console.log(error);
      res.status(500).send({success: false})
    }
    
    
  },
  createEvent: async (req, res) => {
    try {
      if (!req.decoded) {
        res.json({success: false, message: "Chưa đăng nhập"});
        return;
      }
      const body = req.body;
      body._author = req.decoded._id;
      body.createdDate = new Date();
      const newEvent = new Event(body);
      await newEvent.save();
      res.json({success: true});
    } catch (error) {
      console.log(error);
      res.status(500).send({success: false});
    }
  },
}
