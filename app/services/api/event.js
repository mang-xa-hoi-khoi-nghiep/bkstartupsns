const Event = require('../models/Organization/Event');
const _ = require('lodash');
const async = require('async');

module.exports = {
  update: async (req, res) => {
    try {
      const { id } = req.params
      const updatedEvent = req.body
      await Event.findByIdAndUpdate(id, updatedEvent);
      res.json({success: true});
    } catch (error) {
      console.log(error);
      res.status(500).send({success: false});
    }
    
  },
  delete: async (req, res) => {
    try {
      const { id } = req.params;
      await Event.findByIdAndRemove(id);
      res.json({success: true});
    } catch (error) {
      console.log(error);
      res.status(500).send({success: false});
    }
  }
}
