var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Tag = new Schema({
  text: {type: String, unique: true}
})
Tag.path('text').validate((value, done) => {
  this.model('Tag').count({ text: value }, (err, count) => {
      if (err) {
          return done(err);
      } 
      // If `count` is greater than zero, "invalidate"
      done(!count);
  });
}, 'Tag already exists');

module.exports = mongoose.model('Tag', Tag);