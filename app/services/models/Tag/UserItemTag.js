var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserItemTag = new Schema({
  tag_id: {type: Schema.Types.ObjectId, ref: 'Tag'},
  user_id: {type: Schema.Types.ObjectId, ref: 'User'},
  type: {type: String}, // cv
  createdAt: {type: Date, default: Date.now}
})

module.exports = mongoose.model('UserItemTag', UserItemTag);