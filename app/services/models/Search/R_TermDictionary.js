var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var R_TermDictionary = new Schema({
  text: {type: String}, // Là giá trị của term
  docFrequency: {type: Number}, // tần số xuất hiện của từ đó trong toàn bộ tập văn bản.
  inDocuments: [
    {
      id: {type: Schema.Types.ObjectId}, // id của văn bản mà từ đó xuất hiện
      termFrequency: { // tần số xuất hiện của từ đó trong văn bản.
        //field(n): trường mà từ đó xuất hiện. Ví dụ: tiêu đề, giới thiệu, nội dung…
        field1: {type: Number, default: 0}, // field 1 has highest weight factor
        field2: {type: Number, default: 0},
        field3: {type: Number, default: 0},
        field4: {type: Number, default: 0},
        field5: {type: Number, default: 0},
        field6: {type: Number, default: 0},
      },
      maxScore: {type: Number, default: 0} // Điểm số lớn nhất trong các field.
    }
  ]  
})

module.exports = mongoose.model('R_TermDictionary', R_TermDictionary);