var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var C_TermDictionary = new Schema({
  
  text: {type: String},
  docFrequency: {type: Number},
  inDocuments: [
    {
      id: {type: Schema.Types.ObjectId},
      termFrequency: {
        field1: {type: Number, default: 0}, // field 1 has highest weight factor
        field2: {type: Number, default: 0},
        field3: {type: Number, default: 0},
        field4: {type: Number, default: 0},
        field5: {type: Number, default: 0},
        field6: {type: Number, default: 0},
      },
      maxScore: {type: Number, default: 0}
    }
  ]
  
})

module.exports = mongoose.model('C_TermDictionary', C_TermDictionary);