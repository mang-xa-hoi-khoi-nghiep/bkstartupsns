var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PostingList = new Schema({
  word: {
    value: {type: Number},
    inArray: {type: Array, default: []}
  }
})

module.exports = mongoose.model('PostingList', PostingList);