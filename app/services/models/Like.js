var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Like = new Schema({
  author: {type: Schema.Types.ObjectId, ref: 'User'},
  
  news : {type: Schema.Types.ObjectId, ref: 'News'},
  like: {type: Number}, // 0: follow, 1: like

})

module.exports = mongoose.model('Like', Like);
