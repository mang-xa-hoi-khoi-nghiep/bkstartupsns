const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Employee = new Schema({
  _author: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    default: null
  },
  _employee: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    default: null
  },
  _organization: {
    type: Schema.Types.ObjectId,
    ref: 'Organization',
    default: null
  },
  image: { type: String, default: "" },
  name: { type: String, default: "" },
  position: { type: String, default: "" },
  description: { type: String, default: "" },
  isAccepted: { type: String, default: 'unknown' }, // accepted, rejected, unknown  

}, {timestamps: true})


module.exports = mongoose.model('Employee', Employee);