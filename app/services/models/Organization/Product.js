var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Product = new Schema({
  title: { type: String, default: '' },
  _author: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    default: null // reference to User table
  },
  _organization: {
    type: Schema.Types.ObjectId,
    ref: 'Organization',
    default: null
  },
  description: { type: String, default: '' },
  content: { type: String, default: ''},
  createdDate: { type: Date },
  updatedDate: { type: Date, default: Date.now },
  startedDate: { type: Date },
  endedDate: { type: Date },
}, {timestamps: true})

module.exports = mongoose.model('Product', Product);