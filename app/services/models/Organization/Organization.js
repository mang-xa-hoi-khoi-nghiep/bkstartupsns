var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Organization = new Schema({
  _author: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    default: null
  },
  _logo: {
    type: Schema.Types.ObjectId,
    ref: 'Resource',
    default: null
  },
  _banner: {
    type: Schema.Types.ObjectId,
    ref: 'Resource',
    default: null
  },
  name: {
    type: String, default: ''
  },
  phone: {
    type: String, default: ''
  },
  email: {
    type: String,
    //unique: true,
    default: ''
  },
  
  
  fields: { type: Array },
  location: {
    streetNumber: { type: String, default: "" },
    ward: { type: String, default: "" },
    district: { type: String, default: "" },
    province: { type: String, default: "" },
    lat: { type: Number, default: 0 },
    lng: { type: Number, default: 0 }
  },
  slogan: {
    type: String, default: ''
  },
  description: {
    type: String, default: ''
  },
  strongPoints: {type: String, default: ''},
  mission: {
    type: String, default: ''
  },
  vision: {
    type: String, default: ''
  },
  status: {
    type: Number,
    require: true,
    default: 1
  }, //1: active, 2: banned, 3: deleted
  type: {
    type: String, default: 'Khác'
  },// Trường, Khác
  view: {
    total: { type: Number, default: 0 },
    lastModified: { type: Date },
    at: { type: Array }
  },
  tags: {
    type: Array,
    default: []
  },
  createdDate: { type: Date },
  updatedDate: { type: Date, default: Date.now }
  
}, {timestamps: true});



Organization.virtual('_employees', {
  ref: 'Employee',
  localField: '_id',
  foreignField: '_organization'
});
Organization.virtual('_products', {
  ref: 'Product',
  localField: '_id',
  foreignField: '_organization'
});
Organization.virtual('_events', {
  ref: 'Event',
  localField: '_id',
  foreignField: '_organization'
});
Organization.virtual('_recruitments', { 
  ref: 'Recruitment',
  localField: '_id',
  foreignField: '_organization.id'
})
Organization.set('toObject', {virtuals: true});
Organization.set('toJSON', {virtuals: true});



module.exports = mongoose.model('Organization', Organization);
