const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const R_Follow = new Schema({
  _author: { type: Schema.Types.ObjectId, ref: 'User' },
  _recruitment: { type: Schema.Types.ObjectId, ref: 'Recruitment' },
  isFollowed: { type: Boolean, default: false }
})

module.exports = mongoose.model('R_Follow', R_Follow);
