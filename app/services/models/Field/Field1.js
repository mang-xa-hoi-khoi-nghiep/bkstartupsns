var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Field1 = new Schema({
  code: {type: String, default: ""},
  viName: {type: String, default: ""},
  engName: {type: String, default: ""}
})

module.exports = mongoose.model('Field1', Field1);
