var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Field3 = new Schema({
  code: {type: String, default: ""},
  parentCode: {type: String, default: ""},
  viName: {type: String, default: ""},
  engName: {type: String, default: ""}
})

module.exports = mongoose.model('Field3', Field3);
