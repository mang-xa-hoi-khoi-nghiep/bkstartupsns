const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const R_Like = new Schema({
  _author: { type: Schema.Types.ObjectId, ref: 'User' },
  _recruitment: { type: Schema.Types.ObjectId, ref: 'Recruitment' },
  isLiked: { type: Boolean, default: false }
})

module.exports = mongoose.model('R_Like', R_Like);
