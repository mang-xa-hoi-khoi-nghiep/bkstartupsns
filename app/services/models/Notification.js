var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Notification = new Schema({
  content: { type: String, default: '' },
  url: {type: String, default: '' },
  createdDate: { type: Date, default: Date.now },
  sender: { type: Schema.Types.ObjectId, ref: 'User' },
  receiver: { type: Schema.Types.ObjectId, ref: 'User' },
  isChecked: { type: Boolean, default: false },
  isPushed: { type: Boolean, default: false },
})
module.exports = mongoose.model('Notification', Notification);
