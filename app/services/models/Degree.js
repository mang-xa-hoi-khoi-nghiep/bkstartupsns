var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Degree = new Schema({
  name: {type: String}
})

module.exports = mongoose.model('Degree', Degree);
