var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserEducation = new Schema({
  author: { type: Schema.Types.ObjectId, ref: 'User' },
  school_name: { type: Schema.Types.ObjectId, ref: 'Organization', default: null },
  start: { type: String, default: "" },
  end: { type: String, default: "" },
  major: { type: String, default: "" },
  description: { type: String, default: "" },
  grade: { type: String, default: "" },
  isPublic: [{
    isPublic: { type: Boolean, default: true },
    cvCode: { type: String, default: "" },
    recruitmentId: { type: Schema.Types.ObjectId, ref: 'Recruitment', default: null }
  }],
  isActive: { type: Boolean, default: false },
  file_url: { type: String },
  file_name: { type: String }
}, { timestamps: true })

module.exports = mongoose.model('UserEducation', UserEducation);
