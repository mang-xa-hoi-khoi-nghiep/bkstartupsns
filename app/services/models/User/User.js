var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var User = new Schema({
  email: {
    type: String,
    required: true,
    unique: true
  },
  image_url: {
    type: String,
    default: ""
  },
  image_name: {
    type: String,
    default: ""
  },
  _avatar: {
    type: Schema.Types.ObjectId,
    ref: 'Resource',
    default: null
  },
  lastname: {
    type: String,
    require: true
  },
  firstname: {
    type: String,
    require: true
  },
  passwordHash: { type: String },
  salt: { type: String },
  phone: { type: String },

  // 1: person, 2: company
  role: {
    type: Number,
    require: true
  },
  gender: { // for recommend
    type: String,
    default: "Không xác định"
  },
  title: {type: String, default: ""}, // for searching
  birthdate: { type: Date, default: Date.now }, // for recommend
  jobField: { type: String, default: "-1" }, // for recommend
  positions: { type: Array, default: [] }, // for recommend
  experience: { type: Number, default: 0 }, // for recommend
  educationLevel: { type: String, default: "Không yêu cầu" }, // for recommend ["Không yêu cầu", "Trung cấp", "Cao đẳng", "Đại học", "Sau đại học"];
  motivation: { type : String },
  wallet: { type: Number, default: 0 },
  otp: {type: Number, default: 0},
  address: {
    streetNo: { type: String, default: "" }, // save string
    ward: { type: String, default: "" }, // save string
    district: { type: String, default: "" }, // save district_id
    province: { type: String, default: "" } // save province_id
  },
  
  jobSearch: {
    jobType: { type: Array }, // ["Toàn thời gian", "Bán thời gian", "Thực tập", "Thời vụ"];
    minSalary: { type: Number, default : 3 },
    maxSalary: { type: Number, default: 15 },
    location: {
      district: { type: String, default: "" }, // save district_id
      province: { type: String, default: "" } // save province_id
    },
    createdDate: {type: Date},
    updatedDate: {type: Date, default: Date.now},
    isOn: {type: Boolean, default: false}
    // other factor is already exist in CV
  },

  location: {
    lat: { type: Number },
    lng: { type: Number }
  },

  // 0: unactive, 1: active, 2: banned, 3: deleted
  status: {
    type: Number,
    require: true,
    default: 1
  },
  website: { type: String, default: "" },

  createdDate: { type: Date },
  updatedDate: { type: Date, default: Date.now },
  loginDate: { type: Date },

  sns: [{
    title: { type: String },
    info: { type: String }
  }],
  applyTo: [{ 
    type: Schema.Types.ObjectId,
    ref: 'Recruitment'
  }],
  versions: [{
    cvCode: { type: String },
    recruitmentId: { type: Schema.Types.ObjectId, ref: 'Recruitment', default: null },
    createdDate: { type: Date }
  }],
  view: {
    total: { type: Number, default: 0 },
    lastModified: { type: Date },
    at: { type: Array }
  },
}, {timestamps: true});

//virtual field to populate
User.virtual('_organizations', {
  ref: 'Organization',
  localField: '_id',
  foreignField: 'principal'
});

User.virtual('_educations', {
  ref: 'UserEducation',
  localField: '_id',
  foreignField: 'author'
});

User.virtual('_experiences', {
  ref: 'UserExperience',
  localField: '_id',
  foreignField: 'author'
});

User.virtual('_projects', {
  ref: 'UserProject',
  localField: '_id',
  foreignField: 'author'
});

User.virtual('_skills', {
  ref: 'UserSkill',
  localField: '_id',
  foreignField: 'author'
});

User.virtual('_awards', {
  ref: 'UserAward',
  localField: '_id',
  foreignField: 'author'
});

User.virtual('_languages', {
  ref: 'UserLanguage',
  localField: '_id',
  foreignField: 'author'
});

User.virtual('_publications', {
  ref: 'UserPublication',
  localField: '_id',
  foreignField: 'author'
});

User.virtual('_certificates', {
  ref: 'UserCertificate',
  localField: '_id',
  foreignField: 'author'
});

User.virtual('_activities', {
  ref: 'UserActivity',
  localField: '_id',
  foreignField: 'author'
});

User.virtual('_courses', {
  ref: 'UserCourse',
  localField: '_id',
  foreignField: 'author'
});

User.virtual('_middlemans', {
  ref: 'UserMiddleman',
  localField: '_id',
  foreignField: 'author'
});



User.virtual('_recruitments', {
  ref: 'Recruitment',
  localField: '_id',
  foreignField: '_author'
});

User.virtual('_organizations', {
  ref: 'Organization',
  localField: '_id',
  foreignField: '_author'
});
User.virtual('_likes', {
  ref: 'Like',
  localField: '_id',
  foreignField: 'author'
});

User.virtual('_comments', {
  ref: 'Comment',
  localField: '_id',
  foreignField: 'author'
});

User.virtual('_giveFunds', {
	ref: 'Fund',
	localField: '_id',
	foreignField: 'user',
});

User.virtual('_history', {
	ref: 'UserHistory',
	localField: '_id',
	foreignField: 'author',
});

// User.virtual('_tags', {
// 	ref: 'UserItemTag',
// 	localField: '_id',
// 	foreignField: 'user_id',
// });

User.virtual('fullname').get(function() {
  return this.lastname + ' ' + this.firstname;
});

User.virtual('fullAddress').get(function() {
  var address = this.address;
  return (address)? 
    address.streetNo + ', ' + address.ward + ', ' + address.district + ', ' + address.province
    : null;
});

User.set('toObject', {virtuals: true});
User.set('toJSON', {virtuals: true});

//validate credentials before saving to database
User.path('email').validate(function(value, res) {
  const User = mongoose.model('User');
  User.find({email: value}, function(err, users) {
    res(users.length === 0);
  });
}, 'email already exists');


module.exports = mongoose.model('User', User);


// JSON GENERATOR

/*
[
  '{{repeat(20)}}',
  {
    firstname: '{{firstName()}}',
    lastname: '{{surname()}}',
    passwordHash: '036e12f90d0b994c015dcc9027b462fa347292bd3ba26d9f214c427b30057a261a4e75bf99ecfecd324ce93e24e7ca04853644b44dd22ff137a979c687e397a8',
    salt: 'cdbd819fbd4ae9e4',
    email: '{{email()}}',
    role: '{{random(1, 2)}}',
    status: 1
    
  } 
]
*/