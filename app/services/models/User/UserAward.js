var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserAward = new Schema({
  author: { type: Schema.Types.ObjectId, ref: 'User' },
  award_name: { type: String },
  associateWith: { type: String },
  issuer: { type: String },
  month: { type: String },
  year: { type: String },
  description: { type: String },
  isPublic: [{
    isPublic: { type: Boolean, default: true },
    cvCode: { type: String, default: "" },
    recruitmentId: { type: Schema.Types.ObjectId, ref: 'Recruitment', default: null }
  }]
}, { timestamps: true })

module.exports = mongoose.model('UserAward', UserAward);
