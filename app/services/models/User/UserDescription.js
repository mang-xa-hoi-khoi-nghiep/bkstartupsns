var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserDescription = new Schema({
  author: { type: Schema.Types.ObjectId, ref: 'User' },
  description: { type: String },
  isPublic: [{
    isPublic: { type: Boolean, default: true },
    cvCode: { type: String, default: "" },
    recruitmentId: { type: Schema.Types.ObjectId, ref: 'Recruitment', default: null }
  }],
  currentJob: {
    label: { type: String },
    start: { type: String }
  },
  currentSchool: {
    label: { type: String },
    start: { type: String }
  }
})

module.exports = mongoose.model('UserDescription', UserDescription);
