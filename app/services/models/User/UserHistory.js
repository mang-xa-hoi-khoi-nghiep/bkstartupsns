var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserHistory = new Schema({
  author: { type: Schema.Types.ObjectId, ref: 'User' },
  
  recruitmentId: { type: Schema.Types.ObjectId, ref: 'Recruitment' },
  userId: { type: Schema.Types.ObjectId, ref: 'User' },
  organizationId: { type: Schema.Types.ObjectId, ref: 'Organization' },
  
  viewAt: { type: Date, default: Date.now },
  timePeriod: { type: Number, default: ''},
  point: { type: Number, default: 1 },
  type: { type: String, default: '' } // user, recruitment
})

module.exports = mongoose.model('UserHistory', UserHistory);
