var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserPublication = new Schema({
  author: { type: Schema.Types.ObjectId, ref: 'User' },
  publication_name: { type: String },
  publisher: { type: String },
  publishedDate: { type: Date },
  coAuthor: [{ type: String }],
  publication_url: { type: String },
  isPublic: [{
    isPublic: { type: Boolean, default: true },
    cvCode: { type: String, default: "" },
    recruitmentId: { type: Schema.Types.ObjectId, ref: 'Recruitment', default: null }
  }]
}, { timestamps: true })

module.exports = mongoose.model('UserPublication', UserPublication);
