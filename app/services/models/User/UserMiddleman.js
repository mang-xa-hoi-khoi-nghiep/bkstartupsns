var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserMiddleman = new Schema({
  author: { type: Schema.Types.ObjectId, ref: 'User' },
  fullName: { type: String, default: "" },
  jobType: { type: String, default: "" },
  email: { type: String, default: "" },
  phoneNo: { type: String, default: ""},
  isPublic: [{
    isPublic: { type: Boolean, default: true },
    cvCode: { type: String, default: "" },
    recruitmentId: { type: Schema.Types.ObjectId, ref: 'Recruitment', default: null }
  }]
}, { timestamps: true })

module.exports = mongoose.model('UserMiddleman', UserMiddleman);
