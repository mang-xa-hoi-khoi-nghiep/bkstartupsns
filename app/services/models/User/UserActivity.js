var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserActivity = new Schema({
  author: { type: Schema.Types.ObjectId, ref: 'User' },
  activity_name: { type: String, default: "" },
  role: { type: String, default: "" },
  start: {
  	month: { type: String, default: "" },
  	year: { type: String, default: "" }
  },
  end: {
  	month: { type: String, default: "" },
  	year: { type: String, default: "" }
  },
  isActive: { type: Boolean, default: false },
  isPublic: [{
    isPublic: { type: Boolean, default: true },
    cvCode: { type: String, default: "" },
    recruitmentId: { type: Schema.Types.ObjectId, ref: 'Recruitment', default: null }
  }]
  
}, { timestamps: true })

module.exports = mongoose.model('UserActivity', UserActivity);
