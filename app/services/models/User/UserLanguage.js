var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserLanguage = new Schema({
  author: { type: Schema.Types.ObjectId, ref: 'User' },
  language_name: { type: String },
  level: { type: String },
  isPublic: [{
    isPublic: { type: Boolean, default: true },
    cvCode: { type: String, default: "" },
    recruitmentId: { type: Schema.Types.ObjectId, ref: 'Recruitment', default: null }
  }]
}, { timestamps: true })

module.exports = mongoose.model('UserLanguage', UserLanguage);
