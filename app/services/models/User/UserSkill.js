var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSkill = new Schema({
  author: { type: Schema.Types.ObjectId, ref: 'User' },
  skill_name: { type: Array },
  isPublic: [{
    isPublic: { type: Boolean, default: true },
    cvCode: { type: String, default: "" },
    recruitmentId: { type: Schema.Types.ObjectId, ref: 'Recruitment', default: null }
  }]
}, { timestamps: true })

module.exports = mongoose.model('UserSkill', UserSkill);
