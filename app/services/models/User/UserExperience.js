var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserExperience = new Schema({
  author: { type: Schema.Types.ObjectId, ref: 'User' },
  company_name: { type: String, default: "" },
  start: {
    month: { type: String, default: "" },
    year: { type: String, default: "" }
  },
  end: {
    month: { type: String, default: "" },
    year: { type: String, default: "" }
  },
  location: { type: String, default: "" },
  position: { type: String, default: "" },
  description: { type: String, default: "" },
  isPublic: [{
    isPublic: { type: Boolean, default: true },
    cvCode: { type: String, default: "" },
    recruitmentId: { type: Schema.Types.ObjectId, ref: 'Recruitment', default: null }
  }],
  isActive: { type: Boolean, default: false },
  file_url: { type: String, default: "" },
  file_name: { type: String, default: "" }
}, { timestamps: true })

module.exports = mongoose.model('UserExperience', UserExperience);
