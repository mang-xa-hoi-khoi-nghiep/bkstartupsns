var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserCertificate = new Schema({
  certificate_name: { type: String },
  author: { type: Schema.Types.ObjectId, ref: 'User' },
  associateWith: { type: String, default: "" },
  receivedDate: { type: Date },
  degree: { type: Schema.Types.ObjectId, ref: 'Degree', default: null },
  result: { type: String, default: "" },
  classification: { type: String, default: "" },
  isPublic: [{
    isPublic: { type: Boolean, default: true },
    cvCode: { type: String, default: "" },
    recruitmentId: { type: Schema.Types.ObjectId, ref: 'Recruitment', default: null }
  }],
  file_url: { type: String },
  file_name: { type: String }
}, { timestamps: true })

module.exports = mongoose.model('UserCertificate', UserCertificate);
