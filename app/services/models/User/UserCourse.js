var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserCourse = new Schema({
  author: { type: Schema.Types.ObjectId, ref: 'User' },
  course_name: { type: String },
  associateWith: { type: String },
  isPublic: [{
    isPublic: { type: Boolean, default: true },
    cvCode: { type: String, default: "" },
    recruitmentId: { type: Schema.Types.ObjectId, ref: 'Recruitment', default: null }
  }]
}, { timestamps: true })

module.exports = mongoose.model('UserCourse', UserCourse);
