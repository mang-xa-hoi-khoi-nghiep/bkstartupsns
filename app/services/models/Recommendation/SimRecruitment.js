var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SimRecruitment = new Schema({
    row: {type: Schema.Types.ObjectId, ref: 'Recruitment'},
    col: {type: Schema.Types.ObjectId, ref: 'Recruitment'},
    similarity: {type: Number, default: 0}
})
module.exports = mongoose.model('SimRecruitment', SimRecruitment);