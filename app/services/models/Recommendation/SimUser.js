var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SimUser = new Schema({
    row: {type: Schema.Types.ObjectId, ref: 'User'},
    col: {type: Schema.Types.ObjectId, ref: 'User'},
    similarity: {type: Number, default: 0}
})
module.exports = mongoose.model('SimUser', SimUser);