var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Mail = new Schema({
  from: { type: String, default: '' },
  to: {type: String, default: '' },
  createdDate: { type: Date, default: Date.now },
  subject: { type: String, default: '' },
  text: { type: String, default: '' },
  html: { type: String, default: '' },
  isSent: { type: Boolean, default: false },
})

module.exports = mongoose.model('Mail', Mail);
