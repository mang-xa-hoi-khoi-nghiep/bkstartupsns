var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// khi ung tuyen vo 1 tin tuyen dung thi user se dc quyen set public/ private cua cv cho từng tin tuyen dung.
// khi ung tuyen sẽ sinh ra code mới cho từng cv. Cho xuất url cv của từng tin tuyển dụng để send email.
var Recruitment = new Schema({
  title: { type: String, default: "" },
  _author: {
    type: Schema.Types.ObjectId,
    ref: 'User' // reference to User table
  },
  _organization: {
    id: {
      type: Schema.Types.ObjectId,
      ref: 'Organization', // reference to Organization table
      default: null
    },
    name: {type: String, default: ""},
    description: {type: String, default: ""},
    lng: { type: Number, default: 0 },
    lat: { type: Number, default: 0 },
    streetNumber: { type: String, default: "" }, // save string
    ward: { type: String, default: "" }, // save string
    district: { type: String, default: "" }, // save district_id
    province: { type: String, default: "" } // save province_id
  },
  
  content: { type: String, default: "" },
  benefit: { type: String, default: "" },
  requirement: 
  {
    fieldName: { type: Array },
    positions: { type: Array }, // user tag, just for recommendation // NOTE: second phrase of recommendation
    quantity: { type: Number, default: 1 },
    skills: { type: Array }, // user tag, just for recommendation // NOTE: second phrase of recommendation
    jobType: { type: Array }, // ["Toàn thời gian", "Bán thời gian", "Thực tập", "Thời vụ"];
    atLeastDegree: { type: String, default: "" }, // ["Không yêu cầu", "Trung cấp", "Cao đẳng", "Đại học", "Sau đại học"];
    minSalary: { type: Number, default: 0 },
    maxSalary: { type: Number, default: 0 },
    minExperience: { type: Number, default: 0 },
    languages: { type: Array },
    minAge: { type: Number, default: 0 },
    maxAge: { type: Number, default: 0 },
    gender: { type: String, default: "" } // ["Không yêu cầu", "Nam", "Nữ"]
  },
  location: {
    lng: { type: Number },
    lat: { type: Number },
    streetNumber: { type: String, default: "" }, // save string
    ward: { type: String, default: "" }, // save string
    district: { type: String, default: "" }, // save district_id
    province: { type: String, default: "" } // save province_id
  },
  description: { type: String, default: "" },
  createdDate: { type: Date },
  updatedDate: { type: Date, default: Date.now },
  startDate: { type: Date, default: null },
  endDate: { type: Date, default: null },
  isReceivedEmail: { type: Boolean, default: false },
  view: {
    total: { type: Number, default: 0 },
    lastModified: { type: Date },
    at: { type: Array }
  },
  interestedCandidates: [
    {
      candidateId: {
        type: Schema.Types.ObjectId,
        ref: 'User', // reference to User table
        default: null
      },
      createdDate: { type: Date },
      updatedDate: { type: Date, default: Date.now },
      isAccepted: { type: String, default: 'unknown' }, // accepted, rejected, unknown  // candidate side
    }
  ],
  applications: { type: Array },
  // attachFiles: [{
  //   userId: {
  //     type: Schema.Types.ObjectId,
  //     ref: 'User', // reference to User table
  //     default: null
  //   },
  //   fileUrl: { type: Array }
  // }],
  _candidates: [{
    userId: {
      type: Schema.Types.ObjectId,
      ref: 'User', // reference to User table
      default: null
    },
    CVUrl: { type: String, default: "" },
    fileUrl: { type: Array },
    isAccepted: { type: Number, default: -1 }, // -1: nothing, 1: accepted, 0: rejected // recruitment side
    rating: { // -1: nothing, 0 - 5: rated 
      appearance: { type: Number, default: -1 },
      qualification: { type: Number, default: -1 },
      communication: { type: Number, default: -1 },
      experience: { type: Number, default: -1 },   
      note: { type: String, default: "" },
      isRated: { type: Boolean, default: false }
    }, 
    updatedDate: { type: Date }
  }],
  
  //requirement: { type: String },
  status: { type: Number, default: 1 }, // 0: pending, 1: active, 2: blocked, 3: deleted 4: unfinished 5: updatePending
}, {timestamps: true});
Recruitment.virtual('_likes', {
  ref: 'R_Like',
  localField: '_id',
  foreignField: '_recruitment'
});
Recruitment.virtual('_follows', {
  ref: 'R_Follow',
  localField: '_id',
  foreignField: '_recruitment'
});
Recruitment.set('toObject', {virtuals: true});
Recruitment.set('toJSON', {virtuals: true});

module.exports = mongoose.model('Recruitment', Recruitment);


/* JS GENERATOR
[
  '{{repeat(8)}}',
  {
    "_author" : '59d749a11b4e742304b07684', 
    "status" : 1, 
    "updatedDate" : '{{date(new Date(2017, 9, 2), new Date())}}', 
    "createdDate" : '{{date(new Date(2017, 8, 1), new Date(2017, 9, 1))}}', 
    "description" : '{{lorem(1, "paragraphs")}}',
    "requirement" : {
      "quantity" : '{{integer(1, 10)}}', 
      "jobType" : [
        '{{repeat(1, 3)}}',
        '{{random("Toàn thời gian", "Bán thời gian", "Thực tập", "Thời vụ")}}'
      ],
      "atLeastDegree" : '{{random("Không yêu cầu", "Trung cấp", "Cao đẳng", "Đại học", "Sau đại học")}}', 
      "minSalary" : '{{integer(1, 10)}}', 
      "maxSalary" : '{{integer(11, 20)}}', 
      "minAge" : '{{integer(18, 24)}}', 
      "maxAge" : '{{integer(25, 44)}}', 
      "minExperience" : '{{integer(0, 10)}}',  
      "gender" : '{{random("Nam", "Nữ", "Không yêu cầu")}}',
      "languages" : [
          '{{repeat(1, 3)}}',
          '{{random("Tiếng Anh", "Tiếng Việt", "Tiếng Nhật", "Tiếng Đức", "Tiếng Tây Ban Nha", "Tiếng Trung")}}'
      ], 
      "skills" : [
          '{{repeat(1, 3)}}',
          '{{random("C#", "php", "Teamwork", "Leader", "Communication", "Presentation", "Autocad", "Photoshop", "Matlab", "Content Strategy", "Creative Thinking", "Critical Thinking", "Customer Service", "Decision Making", "Delegation", "Deductive Reasoning", "Digital Marketing", "Digital Media")}}'
      ], 
      "positions" : [
         '{{repeat(1, 3)}}',
         '{{random("adviser", "corporate development planning", "analyst", "business management", "business methods", "filing systems" ,"industrial and commercial methods", "point of sale system manager", "price management analyst", "quality auditor", "industrial standards", "records filing systems analysts supervisor")}}'
      ], 
      "fieldName" : [
            '{{repeat(1, 3)}}',
        '{{random("52140101", "52210210", "52210221", "52140114", "52140201", "52210221", "52140219", "52140221", "52140227", "52140217", "52140215", "52210104", "52210103", "52210105", "52210107")}}'
      ]
    }, 
    "benefit" : "", 
    "content" : '{{lorem(2, "paragraphs")}}', 
    "title" : '{{lorem(1, "sentences")}}'
  }
]

*/