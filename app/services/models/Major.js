var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Major = new Schema({
  name: {type: String}
})

module.exports = mongoose.model('Major', Major);
