var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Content = new Schema({
  _id: {type:String},
  problems: {type: String},
  problems_update: {type: String},
  
  product: {type: String},
  product_update: {type: String},
  
  goal: {type: String},
  goal_update: {type: String},
  
  benefit: {type: String},
  benefit_update: {type: String},
  
  budget: {type: String},
  budget_update: {type: String},
  
  cost: {type: String},
  cost_update: {type: String},
  
  risk: {type: String},
  risk_update: {type: String},
  
  revenue: {type: String},
  revenue_update: {type: String},
  
  // imageId: [{
  //   type: Schema.Types.ObjectId,
  //   ref: 'Image'
  // }],
  // video: [{
  //   type: Schema.Types.ObjectId,
  //   ref: 'Video'
  // }],
})
module.exports = mongoose.model('Content', Content);
