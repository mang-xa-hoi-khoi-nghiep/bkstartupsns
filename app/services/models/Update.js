var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Update = new Schema({
  _id: {type: Schema.Types.ObjectId},
  imageId: [{
    type: Schema.Types.ObjectId,
    ref: 'Image'
  }],
  video: [{type: Schema.Types.ObjectId, ref: 'Video'}],
  content: {type: String},
  createdDate: {type: Date}
})
