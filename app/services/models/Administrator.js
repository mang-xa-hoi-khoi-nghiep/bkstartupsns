var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Administrator = new Schema({
  _id: {type: Schema.Types.ObjectId}, //auto generated
  email: {type: String},
  lastname: {type: String},
  firstname: {type: String},
  password: {type: String},
  phone: {type: String},
  role: {type: String},
  address: {type: String},
})
