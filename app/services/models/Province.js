var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Province = new Schema({
  name: { type: String },
  type: { type: String },
  province_id: { type: String },
  total_users: { type: Number, default: 0 },
  total_recruitments: { type: Number, default: 0 },
  total_companies: { type: Number, default: 0 }
})

module.exports = mongoose.model('Province', Province);
