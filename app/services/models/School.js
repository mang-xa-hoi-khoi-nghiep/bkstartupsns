var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var School = new Schema({
  name: { type: String, default: "" }
})

module.exports = mongoose.model('School', School);
