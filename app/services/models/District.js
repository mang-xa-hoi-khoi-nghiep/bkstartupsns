var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var District = new Schema({
  name: { type: String },
  type: { type: String },
  district_id: { type: String },
  province_id: { type: String }
})

module.exports = mongoose.model('District', District);
