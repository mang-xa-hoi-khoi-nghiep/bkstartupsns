var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Comment = new Schema({
  content: {type: String},
  createdDate: {type: Date, default: Date.now},
  report: {type: Number, default: 0},
  author: {type: Schema.Types.ObjectId, ref: 'User'},
  campaign: {type: Schema.Types.ObjectId, ref: 'Campaign'},
  rating: {type: Number}
})

module.exports = mongoose.model('Comment', Comment);

// JSON GENERATOR

/* 
[
  '{{repeat(100)}}',
  {
    content: '{{lorem()}}',
    createdDate: '{{date(new Date(2017, 7, 1), new Date(), "YYYY-MM-dd")}}',
    author: '{{random("5974b49e734d1d6202a92122", "5975c5525db2b22028e6030a", "598b0aa56491b518f8a78ea8", "5996a9298546032f382103a6", "599c4189b710344a5b38abbf", "599c4189a6b875231ba31577", "599c4189e1482e7fc67051f9", "599c41899599223433848fed", "599c41890719a2ff9696ebe9", "599c418985527e1134565c49", "599c41891152fd72eb896048", "599c41892f9b0e2d6f3a7dad", "599c4189625f3d108ccfe252", "599c418994101b8206d80f3d", "599c418978bac30fdce9edce", "599c418918e23fa13c12a3b5", "599c4189639f453a2464263f", "599c41899da32e85948ec2e7", "599c4189d3af49e7a5d7daac", "599c418934d3809716b5fa5a", "599c418926adce4694d328e6", "599c41898bb5a6eb5dc634c8", "599c4189b59ccb9561b74c72", "599c4189b214a34a65650fac")}}',
    campaign: '{{random("59998a394883a63aa4743cd5", "59998a394883a63aa4743cd6", "59998a394883a63aa4743cd7", "59998a394883a63aa4743cd8", "59998a394883a63aa4743cd9", "59998a394883a63aa4743cda", "59998a394883a63aa4743cdb", "59998a394883a63aa4743cdc", "59998a394883a63aa4743cdd", "59998a394883a63aa4743cde", "59998a394883a63aa4743cdf", "59998a394883a63aa4743ce0", "59998a394883a63aa4743ce1", "59998a394883a63aa4743ce2", "59998a394883a63aa4743ce3", "59998a394883a63aa4743ce4", "59998a394883a63aa4743ce5", "59998a394883a63aa4743ce6", "59998a394883a63aa4743ce7", "59998a394883a63aa4743ce8")}}',
    rating: '{{random(1, 2, 3, 4, 5)}}'
    
  } 
]

*/