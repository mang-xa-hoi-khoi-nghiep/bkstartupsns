var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Invitation = new Schema({
  content: { type: String, default: '' },
  url: {type: String, default: '' },
  sender: { type: Schema.Types.ObjectId, ref: 'User', default: null },
  receiver: { type: Schema.Types.ObjectId, ref: 'User', default: null },
  isChecked: { type: Boolean, default: false },
  isPushed: { type: Boolean, default: false },
  isAccepted: { type: String, default: 'unknown' }, // accepted, rejected, unknown, cancelled
  type: { type: String, default: '' }, // employee,
  _organization: { type: Schema.Types.ObjectId, ref: 'Organization', default: null }, // type: employee,
  _recruitment: { type: Schema.Types.ObjectId, ref: 'Recruitment', default: null } // type: recruitment
}, {timestamps: true});
module.exports = mongoose.model('Invitation', Invitation);
