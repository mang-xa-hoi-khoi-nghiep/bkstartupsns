const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Resource = new Schema({
  url: {type: String, default: ""},
  originalName: { type: String, default: "" },
  title: { type: String, default: "" },
  _recruitment: {type: Schema.Types.ObjectId, ref: 'Recruitment', default: null},
  _organization: {type: Schema.Types.ObjectId, ref: 'Organization', default: null},
  _user: {type: Schema.Types.ObjectId, ref: 'User', default: null}, 
}, {timestamps: true});

module.exports = mongoose.model('Resource', Resource);
