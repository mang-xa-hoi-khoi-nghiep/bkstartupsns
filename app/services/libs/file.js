const xlsx = require('node-xlsx');
const async = require('async');
const Field1 = require('../models/Field/Field1');
const Field2 = require('../models/Field/Field2');
const Field3 = require('../models/Field/Field3');
const School = require('../models/School');
const LENGTH = {
  FIELD1: 4,
  FIELD2: 6,
  FIELD3: 8
}
module.exports = {
  importExcel: () => {
    
    const workSheetsFromFile = xlsx.parse(`${__dirname}/fields.xlsx`);
    const data = workSheetsFromFile[0].data; 
    var field1 = [], field2 = [], field3 = [];
    for (var i = 0; i < data.length; i++) {
      var rowCode = '';
      try {
        rowCode = data[i][0].toString();  
      } catch (error) {
        rowCode = '';
      }
      if (rowCode.length === LENGTH.FIELD1) {
        field1.push({
          code: rowCode,
          viName: data[i][1],
          engName: data[i][2]
        })
      }
      else if (rowCode.length === LENGTH.FIELD2) {
        field2.push({
          code: rowCode,
          parentCode: rowCode.substring(0, LENGTH.FIELD1),
          viName: data[i][1],
          engName: data[i][2]
        })
      }
      else {
        field3.push({
          code: rowCode,
          parentCode: rowCode.substring(0, LENGTH.FIELD2),
          viName: data[i][1],
          engName: data[i][2]
        })
      }
    }
    field3 = field3.filter(elem => elem.code !== '#'); 
    console.log('START SAVING FIELD1');
    async.each(
      field1,
      (elem, next) => {
        const newField = new Field1(elem);
        newField.save((err) => {
          if (err) throw err;
        })
        next();
      },
      (err) => {
        if (err) throw err;
        console.log('FIELD1 SAVED');
      }
    )
    console.log('START SAVING FIELD 2');
    async.each(
      field2,
      (elem, next) => {
        const newField = new Field2(elem);
        newField.save(err => {
          if (err) throw err;
        })
        next();
      },
      (err) => {
        if (err) throw err;
        console.log('FIELD2 SAVED');
      }
    )
    console.log('START SAVING FIELD 3');
    async.each(
      field3,
      (elem, next) => {
        const newField = new Field3(elem);
        newField.save(err => {
          if (err) throw err;
        })
        next();
      },
      (err) => {
        if (err) throw err;
        console.log('FIELD3 SAVED');
      }
    )
  },

  importSchoolFromExcel: async () => {
    
      const workSheetsFromFile = xlsx.parse(`${__dirname}/schools.xlsx`);
      const data = workSheetsFromFile[0].data; 
      var schools = [];
      for (var i = 0; i < data.length; i++) {
        try {
          schools.push({
            name: data[i][0]
          });
        } catch (error) {
          console.log(error);
        }
      }
      console.log(schools);
      console.log('Start saving school');
      for (const item of schools) {
        const newSchool = new School(item);
        await newSchool.save();
      }
      console.log('End saving school');
  }
}