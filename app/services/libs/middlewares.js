const User = require('../models/User/User');
const jwt    = require('jsonwebtoken');
 
const watchUserUpdate = async (req, res, next) => {
    try {
      await User.findByIdAndUpdate(req.decoded._id, { updatedDate: new Date() });
      next();
    } catch (error) {
      console.log(error);
      next();
    }
}
const auth = (req, res, next) => {
  try {
    const token = req.body.token || req.query.token || req.headers['x-access-token'];
    if (token) {
  
      // verifies secret and checks exp
      jwt.verify(token, process.env.JWT_SECRET, function(err, decoded) {
        if (err) {
          return res.json({ success: false, message: 'Failed to authenticate token.' });
  
        } else {
          // if everything is good, save to request for use in other routes
          req.decoded = decoded;
          next();
        }
      });
  
    } else {
  
      // if there is no token
      // return an error
      return res.status(200).send({
          success: false,
          message: 'No token provided.'
      });
  
    }
  } catch (error) {
    return res.status(404).send({success: false})
  }
}
module.exports = {
    watchUserUpdate,
    auth,
}