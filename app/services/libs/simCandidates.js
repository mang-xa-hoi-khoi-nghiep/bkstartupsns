/*
  This file has 2 functions:
  1. Recommend similar recruitments based on a specific recruitment
  2. Recommend similar users based on a specific user
*/


const async = require('async');
const _ = require('lodash');
const math = require('mathjs');
const nlp = require('./vnNLP').tagAnalysis;
const ObjectId = require('mongodb').ObjectId;
const { fromNow } = require('../libs/shareFunction');
const { getFields, AT_LEAST_DEGREE, FALSIFIES } = require('./recruitmentRecommender');
const RECRUITMENT_TYPES = {
  R_FIELD: { label: 'r_field', weighted: 0.5 },
  R_LOCATION: { label: 'r_location', weighted: 0.2 },
  R_SALARY: { label: 'r_salary', weighted: 0.05 },
  R_EXPERIENCE: { label: 'r_experience', weighted: 0.05 },
  R_JOB_TYPE: { label: 'r_job_type', weighted: 0.05 },
  R_POSITIONS: { label: 'r_positions', weighted: 0.025 },
  R_DEGREE: { label: 'r_degree', weighted: 0.025 },
  R_LANGUAGE: { label: 'r_language', weighted: 0.025 },
  R_SKILL: { label: 'r_skill', weighted: 0.025 },
  R_AGE: { label: 'r_age', weighted: 0.025 },
  R_GENDER: { label: 'r_gender', weighted: 0.025 },
}
const USER_TYPES = { // descend in weight factor
  U_FIELD: { label: 'u_field', weighted: 0.5 },
  U_LOCATION: { label: 'u_location', weighted: 0.2 },
  U_EXPERIENCE: { label: 'u_experience', weighted: 0.1 },
  U_POSITIONS: { label: 'u_positions', weighted: 0.05 },
  U_SKILL: { label: 'u_skill', weighted: 0.05 },
  U_DEGREE: { label: 'u_degree', weighted: 0.025 },
  U_LANGUAGE: { label: 'u_language', weighted: 0.025 },
  U_AGE: { label: 'u_age', weighted: 0.025 },
  U_GENDER: { label: 'u_gender', weighted: 0.025 },
}
const TYPES = {
  RECRUITMENT: 'recruitment',
  USER: 'user'
}
const NOT_REQUIRED = "Không yêu cầu";
const NOT_DEFINED = "Không xác định";
class Score {
  constructor(item1, item2, type, typeArr) {
    this.item1 = item1;
    this.item2 = item2;
    this.type = type;
    this.typeArr = typeArr;
  }
  mapDegreeToValue(degreeArr, degree) {
    const value = (degreeArr.find((elem) => elem[0] === degree))[1];
    return value;
  }
  // RECRUITMENT
  r_field() {
    try {

      if (!_.isEmpty(_.intersectionBy(this.item1.field.field3, this.item2.field.field3, 'code'))) {
        return 1;
      }
      else if (!_.isEmpty(_.intersectionBy(this.item1.field.field2, this.item2.field.field2, 'code'))) {
        return 0.75;
      }
      else if (!_.isEmpty(_.intersectionBy(this.item1.field.field1, this.item2.field.field1, 'code'))) {
        return 0.25;
      }
      else return 0;
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  r_location() {
    try {
      const item1Province = this.item1.location.province;
      const item2Province = this.item2.location.province;
      const item1District = this.item1.location.district;
      const item2District = this.item2.location.district;
      if (!item1Province || !item2Province) return 0;
          
      if (item1Province !== item2Province) {
        return 0;
      }
      if (item1Province === item2Province) {
        
        return 0.75;
      }
      if (item1Province === item2Province && item1District === item2District) {
        return 1;
      }
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  r_degree() {
    try {
      const item1Degree = this.mapDegreeToValue(AT_LEAST_DEGREE, this.item1.requirement.atLeastDegree);
      const item2Degree = this.mapDegreeToValue(AT_LEAST_DEGREE, this.item2.requirement.atLeastDegree);
      if (item1Degree === 0 || item2Degree === 0) {
        return 1;
      }
      if (item1Degree > item2Degree)
        return item2Degree / item1Degree;
      return item1Degree / item2Degree;
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  r_experience() {
    try {
      const item1Exp = this.item1.requirement.minExperience;
      const item2Exp = this.item2.requirement.minExperience;
      const score = (item1Exp > item2Exp) ? item2Exp / item1Exp : item1Exp / item2Exp;
      if (_.includes(FALSIFIES, score)) return 1;

      return score;
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  r_age() {
    try {
      const item1MinAge = this.item1.requirement.minAge;
      const item1MaxAge = this.item1.requirement.maxAge;

      const item2MinAge = this.item2.requirement.minAge;
      const item2MaxAge = this.item2.requirement.maxAge;
      if (this.item1.requirement.minAge <= this.item2.requirement.maxAge
        && this.item1.requirement.maxAge >= this.item2.requirement.minAge) {
        return 1;
      }
      return 0;
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  r_gender() {
    try {
      const item1Gender = this.item1.requirement.gender;
      const item2Gender = this.item2.requirement.gender;
      if (!item1Gender || !item2Gender) return 0;

      if (item1Gender === NOT_REQUIRED
        || item2Gender === NOT_REQUIRED
        || item1Gender === item2Gender) {
        return 1;
      }
      return 0;
    } catch (error) {
      console.log(error);
      return 0;
    }

  }
  r_language() {
    try {

      const intersection =
        _.intersection(
          this.item1.requirement.languages,
          this.item2.requirement.languages
        )
      const maxScore = this.item1.requirement.languages.length;

      //console.log('LANGUAGE: ', intersection.length/ maxScore);
      if (isNaN(intersection.length / maxScore)) {
        return 0;
      }
      return intersection.length / maxScore;

    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  r_skill() { // user tag
    try {
      const item1Skills = this.item1.requirement.skills.map(elem => nlp(elem));
      const item2Skills = this.item2.requirement.skills.map(elem => nlp(elem));
      const intersection =
        _.intersection(
          item1Skills,
          item2Skills
        )
      const maxScore = this.item1.requirement.skills.length;
      if (isNaN(intersection.length / maxScore)) {
        return 0;
      }
      return intersection.length / maxScore;
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  r_positions() { // user tag
    try {

      const item1Positions = this.item1.requirement.positions.map(elem => nlp(elem));

      const item2Positions = this.item2.requirement.positions.map(elem => nlp(elem));
      if (!_.isEmpty(_.intersection(item1Positions, item2Positions))) {
        return 1;
      }
      return 0;
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  r_salary() {
    try {
      if (this.item1.requirement.minSalary <= this.item2.requirement.maxSalary
        && this.item1.requirement.maxSalary >= this.item2.requirement.minSalary) {
        return 1;
      }
      return 0;
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  r_jobType() {
    try {
      if (!_.isEmpty(_.intersection(this.item1.requirement.jobType, this.item2.requirement.jobType))) {
        return 1;
      }
      return 0;
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  // USER
  u_field() {
    try {
      if (!_.isEmpty(_.intersectionBy(this.item1.field.field3, this.item2.field.field3, 'code'))) {
        return 1;
      }
      else if (!_.isEmpty(_.intersectionBy(this.item1.field.field2, this.item2.field.field2, 'code'))) {
        return 0.75;
      }
      else if (!_.isEmpty(_.intersectionBy(this.item1.field.field1, this.item2.field.field1, 'code'))) {
        return 0.25;
      }
      else return 0;
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  u_location() {
    try {
      const item1Province = this.item1.address.province;
      const item2Province = this.item2.address.province;
      const item1District = this.item1.address.district;
      const item2District = this.item2.address.district;
      if (!item1Province || !item2Province) return 0;


      if (item1Province !== item2Province) {
        return 0;
      }
      if (item1Province === item2Province) {
        return 0.75;
      }
      if (item1Province === item2Province && item1District === item2District) {
        return 1;
      }
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  u_positions() {
    try {
      const item1Positions = this.item1.positions.map(elem => nlp(elem));
      const item2Positions = this.item2.positions.map(elem => nlp(elem));
      if (!_.isEmpty(_.intersection(item1Positions, item2Positions))) {
        return 1;
      }
      return 0;
    } catch (error) {
      console.log(error);
      return 0;
    }
    return 0;
  }
  u_experience() {
    try {
      const item1Exp = this.item1.experience;
      const item2Exp = this.item2.experience;
      const score = (item1Exp > item2Exp) ? item2Exp / item1Exp : item1Exp / item2Exp;
      if (_.includes(FALSIFIES, score)) return 1;

      return score;
    } catch (error) {
      console.log(error);
      return 0;
    }
    return 0;
  }
  u_skill() {
    try {
      const item1Skills = _.flattenDeep(this.item1._skills.map(skill => skill.skill_name)).map(elem => nlp(elem));
      const item2Skills = _.flattenDeep(this.item2._skills.map(skill => skill.skill_name)).map(elem => nlp(elem));
      const intersection =
        _.intersection(
          item1Skills,
          item2Skills
        )
      const maxScore = item1Skills.length;
      if (isNaN(intersection.length / maxScore)) {
        return 0;
      }
      return intersection.length / maxScore;
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  u_degree() {
    try {
      const item1Degree = this.mapDegreeToValue(AT_LEAST_DEGREE, this.item1.educationLevel);
      const item2Degree = this.mapDegreeToValue(AT_LEAST_DEGREE, this.item2.educationLevel);
      if (item1Degree === 0 || item2Degree === 0) {
        return 1;
      }
      if (item1Degree > item2Degree)
        return item2Degree / item1Degree;
      return item1Degree / item2Degree;
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  u_language() {
    try {
      const item1Languages = this.item1._languages.map(lang => lang.language_name);
      const item2Languages = this.item2._languages.map(lang => lang.language_name);
      const intersection =
        _.intersection(
          item1Languages,
          item2Languages
        )
      const maxScore = item1Languages.length;
      if (isNaN(intersection.length / maxScore)) {
        return 0;
      }
      //console.log('LANGUAGE: ', intersection.length/ maxScore);
      return intersection.length / maxScore;

    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  u_age() {
    try {
      const item1Age = fromNow(new Date(this.item1.birthdate)) / 365;
      const item2Age = fromNow(new Date(this.item2.birthdate)) / 365;
      if (item1Age === 0 || item2Age === 0) {
        return 0;
      }
      if (item1Age > item2Age)
        return item2Age / item1Age;
      return item1Age / item2Age;
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  u_gender() {
    try {
      const item1Gender = this.item1.gender;
      const item2Gender = this.item2.gender;
      if (!item1Gender || !item2Gender) return 0;

      if (item1Gender === NOT_DEFINED
        || item2Gender === NOT_DEFINED
        || item1Gender === item2Gender) {
        return 1;
      }
      return 0;
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  setScore() {
    switch (this.type) {
      // RECRUITMENT
      case this.typeArr.R_FIELD:
        return this.r_field();
        break;
      case this.typeArr.R_LOCATION:
        return this.r_location();
        break;
      case this.typeArr.R_SALARY:
        return this.r_salary();
        break;
      case this.typeArr.R_JOB_TYPE:
        return this.r_jobType();
        break;
      case this.typeArr.R_POSITIONS:
        return this.r_positions();
        break;
      case this.typeArr.R_EXPERIENCE:
        return this.r_experience();
        break;
      case this.typeArr.R_DEGREE:
        return this.r_degree();
        break;
      case this.typeArr.R_LANGUAGE:
        return this.r_language();
        break;
      case this.typeArr.R_SKILL:
        return this.r_skill();
        break;
      case this.typeArr.R_AGE:
        return this.r_age();
        break;
      case this.typeArr.R_GENDER:
        return this.r_gender();
        break;
      // USER
      case this.typeArr.U_FIELD:
        return this.u_field();
        break;
      case this.typeArr.U_LOCATION:
        return this.u_location();
        break;
      case this.typeArr.U_POSITIONS:
        return this.u_positions();
        break;
      case this.typeArr.U_EXPERIENCE:
        return this.u_experience();
        break;
      case this.typeArr.U_SKILL:
        return this.u_skill();
        break;
      case this.typeArr.U_DEGREE:
        return this.u_degree();
        break;
      case this.typeArr.U_LANGUAGE:
        return this.u_language();
        break;
      case this.typeArr.U_AGE:
        return this.u_age();
        break;
      case this.typeArr.U_GENDER:
        return this.u_gender();
        break;
      default:
        break;
    }
  }
}

class Matrix {
  constructor(items, typeArr) {
    this.items = items;
    this.typeArr = typeArr;
    this.listOfAttribute = {};
    this.finalAttribute = [];
    this.label = [];
  }
  buildAttributeLabel(type) {
    for (var i = 0; i < this.items.length; i++) {
      this.label[i] = [];
      for (var j = 0; j < this.items.length; j++) {
        this.label[i][j] = {
          row: this.items[i]._id,
          col: this.items[j]._id
        }
      }
    }
  }
  buildAttribute(type) {
    const m_attribute = [];
    for (var i = 0; i < this.items.length; i++) {
      m_attribute[i] = [];
      for (var j = 0; j < this.items.length; j++) {
        const ScoreInstance = new Score(this.items[i], this.items[j], type, this.typeArr);
        const score = ScoreInstance.setScore() * type.weighted;
        m_attribute[i][j] = score;

      }
    }
    return m_attribute;
  }
  buildListOfAttribute() {
    _.map(this.typeArr, (type) => {
      this.listOfAttribute[type.label] = this.buildAttribute(type);
    })
  }
  buildFinalAttribute() {
    this.finalAttribute = _.reduce(this.listOfAttribute, (result, value, key) => {
      return math.add(result, value);
    });

  }
  main() {
    this.buildAttributeLabel();
    this.buildListOfAttribute();
    // console.log('LIST ATTRIBUTE: ', this.listOfAttribute);
    this.buildFinalAttribute();
    // console.log('LIST FINAL ATTRIBUTE: ', this.finalAttribute);
    return { finalAttribute: this.finalAttribute, label: this.label };
  }
}

class simCandidates {
  constructor(matrix, matrixLabel) {
    this.matrix = matrix;
    this.matrixLabel = matrixLabel;
  }
  buildList() {
    const simCandidates = [];
    for (var i = 0; i < this.matrix.length; i++) {
      simCandidates[i] = [];
      for (var j = 0; j < this.matrix[i].length; j++) {
        const item = {
          row: this.matrixLabel[i][j].row,
          col: this.matrixLabel[i][j].col,
          similarity: this.matrix[i][j],
        }
        simCandidates[i][j] = item;
      }
    }
    return simCandidates;
  }
}

class RecommenderIndexing {
  constructor(type) {
    this.type = type;

  }
  selectCollectionsBasedOnType() {
    var Collection, populateOption, Model, ITEM_TYPES;
    switch (this.type) {
      case TYPES.RECRUITMENT:
        Collection = require('../models/Recruitment');
        populateOption = '';
        Model = require('../models/Recommendation/SimRecruitment');
        ITEM_TYPES = RECRUITMENT_TYPES;
        break;
      case TYPES.USER:
        Collection = require('../models/User/User');
        populateOption = '_languages _skills';
        Model = require('../models/Recommendation/SimUser');
        ITEM_TYPES = USER_TYPES;
        break;
      default:
        break;
    }
    return { Collection, populateOption, Model, ITEM_TYPES };
  }
  async exec() {
    try {
      console.log(`Recommender: Start similiar ${this.type} indexing`);
      const { Collection, populateOption, Model, ITEM_TYPES } = this.selectCollectionsBasedOnType();
      const items = await Collection.find().lean().populate(populateOption);
      async.each(
        items,
        (item, next) => {
          getFields(
            (this.type === TYPES.RECRUITMENT) ? item.requirement.fieldName : [item.jobField],
            (field) => {
              item.field = field;
              next();
            }
          )
        },
        async (err) => {
          if (err) console.log(err);
          const MatrixInstance = new Matrix(items, ITEM_TYPES);
          const { finalAttribute, label } = MatrixInstance.main();
          const simCandidatesInstance = new simCandidates(finalAttribute, label);
          const simList = _.flattenDeep(simCandidatesInstance.buildList())
            .filter(elem => elem.row.toString() !== elem.col.toString()).map(elem => {
              if (elem.similarity) return elem;
              elem.similarity = 0;
              return elem;
            });

          // console.log('MATRIX: ', finalAttribute);
          // console.log('SIM LIST: ', simList);

          try {
            await Model.remove({});
            async.each(simList, (elem, next) => {
              const newRow = new Model(elem);
              newRow.save((err) => {
                if (err) throw err;
                next();
              });
            }, (err) => { if (err) console.log(err); console.log(`Recommender: End similiar ${this.type} indexing`); })
          } catch (error) {
            console.log(error);
          }

        }
      )
    } catch (error) {
      console.log(error);
    }
  }
}
module.exports = {
  RecommenderIndexing,
  TYPES
}
