const User = require('../models/User/User');
const M_Invitation = require('../models/Invitation');
const Notification = require('./notification').Notification;
const Employee = require('../models/Organization/Employee');
const Organization = require('../models/Organization/Organization');
const Recruitment = require('../models/Recruitment');
const INVITATION_TYPES = {
  EMPLOYEE: 'employee',
  RECRUITMENT: 'recruitment'
}
const ACCEPTED_TYPES = {
  UNKNOWN: 'unknown',
  ACCEPTED: 'accepted',
  REJECTED: 'rejected',
  CANCELLED: 'cancelled'
}
const CANDIDATE_ACCEPTED_TYPES = {
  UNKNOWN: -1,
  ACCEPTED: 1,
  REJECTED: 0,
}
class Invitation {
  constructor(sender, receiver, content, url, type, _organization = null, _recruitment = null) {
    this.sender = sender;
    this.receiver = receiver;
    this.content = content;
    this.url = url;
    this.type = type;
    this._organization = _organization;
    this._recruitment = _recruitment;
  }
  async send() {
    try {
      const invitation = new M_Invitation({
        sender: this.sender,
        receiver: this.receiver,
        content: this.content,
        url: this.url,
        type: this.type,
        _organization: this._organization,
        _recruitment: this._recruitment
      })
      await invitation.save();

    } catch (error) {
      console.log(error);
    }
  }
  static async update(id, isAccepted) {
    try {
      const result = await M_Invitation.findOne({_id: id});
      if (result.isAccepted === ACCEPTED_TYPES.CANCELLED) return;
      const invitation =  await M_Invitation.findOneAndUpdate({_id: id, isPushed: true}, {isAccepted}, {new: true});
      
      var invitationContent = '', url = '';
      if (invitation.type === INVITATION_TYPES.EMPLOYEE) {
        await Employee.findOneAndUpdate(
          {_employee: invitation.receiver, _organization: invitation._organization}, {isAccepted})
        const organization = await Organization.findOne({_id: invitation._organization});
        
        invitationContent = (invitation.isAccepted === ACCEPTED_TYPES.ACCEPTED)
        ? `Đã chấp nhận vào tổ chức ${organization.name}`
        : `Đã từ chối vào tổ chức ${organization.name}`;
        url = `/organizations/${organization._id}`
      }
      else if (invitation.type === INVITATION_TYPES.RECRUITMENT) {
        const recruitment = await Recruitment.findOneAndUpdate({
          _id: invitation._recruitment,
          'interestedCandidates.candidateId': invitation.receiver,
        }, {
          $set: {
            'interestedCandidates.$.isAccepted': isAccepted
          }
        })
        invitationContent = (invitation.isAccepted === ACCEPTED_TYPES.ACCEPTED)
        ? `Đã chấp nhận lời mời ứng tuyển vào ${recruitment.title}`
        : `Đã từ chối lời mời ứng tuyển vào ${recruitment.title}`;
        url = `/recruitments/${recruitment._id}`
      }
      const notification = new Notification(
        invitation.receiver, 
        invitation.sender, 
        invitationContent,
        url
      );
      notification.send();
      console.log("Invitation update");
    } catch (error) {
      console.log(error);
    }
  }
  static async cancel(senderId, receiverId, itemId, type) {
    try {
      const item = (type === INVITATION_TYPES.EMPLOYEE)? '_organization': '_recruitment';
      const findOptions = {};
      findOptions.isAccepted = ACCEPTED_TYPES.UNKNOWN;
      findOptions.sender = senderId;
      findOptions.receiver = receiverId;
      findOptions[item] = itemId;
      console.log(findOptions);
      const invitation = await M_Invitation.findOne(findOptions);
      if (!invitation) {
        console.log("Không có lời mời  này");
        return;
      }
      if (type === INVITATION_TYPES.RECRUITMENT) {
        // await Recruitment.update({
        //   _id: invitation._recruitment,
        // }, {
        //   $pull: {interestedCandidates : { candidateId: invitation.receiver }}   
        // });
        await Recruitment.findOneAndUpdate({
          _id: invitation._recruitment,
          'interestedCandidates.candidateId': invitation.receiver
          }, {
            $set: {
              'interestedCandidates.$.isAccepted': ACCEPTED_TYPES.CANCELLED
            }
        })
      }
      else if (type === INVITATION_TYPES.EMPLOYEE) {
        await Employee.findOneAndRemove({
          _organization: invitation._organization,
          _employee: invitation.receiver,
          isAccepted: ACCEPTED_TYPES.CANCELLED
        })
      }

      await M_Invitation.findOneAndUpdate({_id: invitation._id}, {isAccepted: ACCEPTED_TYPES.CANCELLED});


    } catch (error) {
      console.log(error);
    }
  }
}
module.exports = {
  Invitation,
  INVITATION_TYPES,
  ACCEPTED_TYPES,
  CANDIDATE_ACCEPTED_TYPES
}