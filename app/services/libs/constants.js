
// vnNLP
const stopWords = ["bị", "bởi", "cả", "các", "cái", "cần", "càng", "chỉ", "chiếc", "cho", "chứ", "chưa", "chuyện",
"có", "có thể", "cứ", "của", "cùng", "cũng", "đã", "đang", "đây", "để", "đến nỗi", "đều", "điều",
"do", "đó", "được", "dưới", "gì", "khi", "không", "là", "lại", "lên", "lúc", "mà", "mỗi", "một cách",
"này", "nên", "nếu", "ngay", "nhiều", "như", "nhưng", "những", "nơi", "nữa", "phải", "qua", "ra",
"rằng", "rằng", "rất", "rất", "rồi", "sau", "sẽ", "so", "sự", "tại", "theo", "thì", "trên", "trước",
"từ", "từng", "và", "vẫn", "vào", "vậy", "vì", "việc", "với", "vừa"]
const specialCharacters = [".", ",", "'","\"",  ':', '`', "!", "@", "#", "$", "%", "^", "&", "*", "?", "\\",
";", "{", "}", "[", "]", "(", ")", "~", "/", "-" , "\n", "\r", "\t"];
const falsifies = [null, undefined, '']

const SORT_TYPES = {
  TRENDING: 'trending',
	NEWEST: 'newest',
	RELEVANT: 'relevant'
}

const ITEM_TYPE = {
	EDUCATION: 1,
	EXPERIENCE: 2,
	PROJECT: 3,
	ACTIVITY: 4,
	AWARD: 5,
	COURSE: 6,
	LANGUAGE: 7,
	PUBLICATION: 8,
	SKILL: 9,
	BASICINFO: 10,
	CERTIFICATE: 11,
	MIDDLEMAN: 12
}
module.exports = {
  stopWords, specialCharacters, falsifies, SORT_TYPES, ITEM_TYPE
}
