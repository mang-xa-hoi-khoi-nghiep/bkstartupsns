const nodemailer = require('nodemailer');
const M_Mail = require('../models/Mail');
const config_email = require('../../config');
const config = {
    service: 'gmail',
    auth: {
        user: config_email.email.username,
        pass: config_email.email.password
    }
};

const mailData = (options) => {
    var obj = {
        from: options.from,
        to: options.to,
        subject: options.subject,
        text: options.text,
        html: options.html
    };
    return obj;
}

sendMail = async (options) => {
    try {
        const result = await M_Mail.create(options);
        var transporter = nodemailer.createTransport(config);
        transporter.sendMail(options, async (err, info) => {
            if (err) {
                console.log(err);
            }
            else {
                const updated = await M_Mail.findOneAndUpdate({_id: result._id}, {isSent: true});
                console.log('Email sent: ' + info.response);
                console.log(updated);
            }
        })
    } catch (error) {
        console.log(error);
    }
}

module.exports = {
  sendMail
}