const ItemModel = require('../models/Recruitment');
const Field3 = require('../models/Field/Field3');
const District = require('../models/District');
const Province = require('../models/Province');
const {fromNow} = require('./shareFunction');
const async = require('async');
const PERIOD = {
  DAY: 0,
  WEEK: 6,
  MONTH: 30
}
const calculateThreshold = () => {
  return new Date((new Date().getTime() - 30*86400000)); // 30 days
}
const countViewPerPeriod = (items, period = PERIOD.DAY) => {
  items.forEach(item => {
    item.viewPerPeriod = 0;
    item.view.at.forEach(viewAt => {
      const dayFromNow = fromNow(viewAt);
      if (dayFromNow <= period) { // view in present day
          item.viewPerPeriod++;
      }
    })
  })
  return items;
}
const trending =  async (req, res, options, ItemModel, type, populateOptions = '') => {
  const { TRENDING_TYPES } = require('../api/user');
  // determine trending recruitment:
  // - filter out all the recruitments that have not been viewed in 30 days
  // - sort recruitments by view per day  
  try {
    const query = req.query;
    const page = Number(query.page);
    const PAGE_SIZE = Number(query.PAGE_SIZE);
    const threshold = calculateThreshold();
    options['view.lastModified'] = { $gte: threshold }
    var items = await ItemModel.find(options).lean().populate(populateOptions)
    items = countViewPerPeriod(items);
    
    items = items.sort((a, b) => b.viewPerPeriod - a.viewPerPeriod);
    const total = await ItemModel.count(options);
    const firstIndex = page * PAGE_SIZE, lastIndex = page * PAGE_SIZE + PAGE_SIZE;
    const chunk = items.slice(firstIndex, lastIndex);
    console.log(type);
    if (type === TRENDING_TYPES.USER) {
      for (let u of chunk) {
        const district = await District.findOne({district_id: u.address.district});
        const province = await Province.findOne({province_id: u.address.province});
        
        const field = await Field3.findOne({code: u.jobField})
        u.fieldName = field;
        u.address.district = (district)? `${district.type} ${district.name}`: '';
        u.address.province = (province)? `${province.type} ${province.name}`: '';
      }
      res.json({items: chunk, total, page: query.page, elemCount: chunk.length}) 
    }
    else if (type === TRENDING_TYPES.ORGANIZATION) {
      // async.each(chunk, (org, next) => {
      //   Field3.find({code: {$in: org.fields}}).exec((err, fields) => {
      //     if (err) throw err;
      //     org.fieldNames = fields;
      //     next();
      //   })
      // }, (err) => {
      //   if (err) throw err;   
      //   res.json({items: chunk, total, page: query.page, elemCount: chunk.length})  
      // })
      for (let o of chunk) {
        const district = await District.findOne({district_id: o.location.district});
        const province = await Province.findOne({province_id: o.location.province});
        const fields = await Field3.find({code: {$in: o.fields}});

        o.fieldNames = fields;
        o.location.district = (district)? `${district.type} ${district.name}`: '';
        o.location.province = (province)? `${province.type} ${province.name}`: '';
      }
      res.json({items: chunk, total, page: query.page, elemCount: chunk.length})  
    }
    else if (type === TRENDING_TYPES.RECRUITMENT) {
      for (let r of chunk) {
        const district = await District.findOne({district_id: r.location.district});
        const province = await Province.findOne({province_id: r.location.province});
        r.location.district = (district)? `${district.type} ${district.name}`: '';
        r.location.province = (province)? `${province.type} ${province.name}`: '';
      }
      res.json({items: chunk, total, page: query.page, elemCount: chunk.length});  
    }
  } catch (error) {
    console.log(error);
    res.status(500).send({error});
  }
}
module.exports = {
  calculateThreshold,
  countViewPerPeriod,
  trending
}