const User = require('../models/User/User');
const M_Notification = require('../models/Notification');
class Notification {
  constructor(sender, receiver, content, url) {
    this.sender = sender;
    this.receiver = receiver;
    this.content = content;
    this.url = url;
  }
  async send() {
    try {
      const notification = new M_Notification({
        sender: this.sender,
        receiver: this.receiver,
        content: this.content,
        url: this.url
      })
      await notification.save();

    } catch (error) {
      console.log(error);
    }
  }
}
module.exports = {
  Notification
}