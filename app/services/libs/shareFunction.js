const _ = require('lodash');
const async = require('async');
const { ITEM_TYPE } = require('./constants');

const selectCollections = (type) => {
  var Collection = '';
  switch (type) {
    case ITEM_TYPE.EDUCATION:
      Collection = require('../models/User/UserEducation');
      break;
    case ITEM_TYPE.EXPERIENCE:
      Collection = require('../models/User/UserExperience');
      break;
    case ITEM_TYPE.PROJECT:
      Collection = require('../models/User/UserProject');
      break;
    case ITEM_TYPE.ACTIVITY:
      Collection = require('../models/User/UserActivity');
      break;
    case ITEM_TYPE.AWARD:
      Collection = require('../models/User/UserAward');
      break;
    case ITEM_TYPE.COURSE:
      Collection = require('../models/User/UserCourse');
      break;
    case ITEM_TYPE.LANGUAGE:
      Collection = require('../models/User/UserLanguage');
      break;
    case ITEM_TYPE.PUBLICATION:
      Collection = require('../models/User/UserPublication');
      break;
    case ITEM_TYPE.SKILL:
      Collection = require('../models/User/UserSkill');
      break;
    case ITEM_TYPE.CERTIFICATE:
      Collection = require('../models/UserCertificate');
      break;
    case ITEM_TYPE.MIDDLEMAN:
      Collection = require('../models/UserMiddleman');
      break;
    default:
      break;
  }
  return Collection;
}

const fromNow = (viewAt) => {
  try {
    const now = Date.now();
    const then  = new Date(viewAt).getTime();
    const dayFromNow = Math.round((now - then)/86400000);
    return dayFromNow;  
  } catch (error) {
    console.log(error);
    return 999999;
  }
}
const sumAllElemWithSameField = (initialArr, fieldToCompare, fieldNeedToSum) => {
  try {
    const arr = _.reduce(initialArr, (result, obj) => {
      if (_.isEmpty(result) || !_.some(result, [fieldToCompare, obj[fieldToCompare]])) {
        obj.count = 1;
        result.push(obj);
        return result;
      }
      else {
        const tmp = _.map(result, (elem) => {
          if (elem[fieldToCompare].toString() == obj[fieldToCompare].toString()) {
            elem.count++;
            elem[fieldNeedToSum] += obj[fieldNeedToSum];
          }
          return elem;
        })
        return tmp;
      }
    }, []);
    return arr;  
  } catch (error) {
    console.log(error);
    return [];
  }
}

const updateCVCodeUserCV = async (initialArr, type, id) => {
  try {
    var Collection = selectCollections(type);
    const dataCV = await Collection.findById(id);
    const isPublic = dataCV.isPublic;
    async.each(isPublic, function(item, callback) {
      if (item.cvCode === initialArr.isPublic[0].cvCode) {
        item.isPublic = initialArr.isPublic[0].isPublic;
        callback("Founded");
      }
      else {
        callback();
      }
    }, function(err) {
      console.log("Done");
    });
    
    initialArr.isPublic = isPublic;
    return initialArr;
  } catch (error) {
    console.log(error);
    return {};
  }
}

module.exports = {
  fromNow,
  sumAllElemWithSameField,
  updateCVCodeUserCV
}