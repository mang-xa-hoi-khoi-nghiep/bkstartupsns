// const vntk = require("vntk");
// const ws = vntk.ws();
const _ = require('lodash');
const stopWords = require('./constants').stopWords;
const specialCharacters = require('./constants').specialCharacters;
const falsifies = require('./constants').falsifies;
const tokenization1 = (str) => {
  const tokens = str.split(' ');
  return tokens;
}
// const tokenization2 = (str) => {
//   const segment = ws.segment(str);
//   console.log(segment);
//   const tmp = segment.split(' ');
//   const tokens = tmp.map(elem => {
//     elem = elem.replace(/_/g, ' ')
//     return elem;
//   })
//   return tokens
// }
const removeSpecialCharacters = (arr) => {
  const terms = arr.map((str) => {

    return str.replace(/(\.|,|:|`|!|@|#|\$|%|\^|&|\*|\(|\)|\?|\'|\"|;|\{|\}|\[|\]|~|\\|\/|-|:|\t|\n|\r)/g, '');
    //return !_.includes(specialCharacters, str);
  })
  return terms
}
const lowerCase = (arr) => {
  const terms = arr.map((str) => {
    return str.toLowerCase();
  })
  return terms
}
const removeSeperatedSentencesDOT = (arr) => {
  const terms = arr.map(str => {
    if (_.includes(str, '.')) {
      str = str.replace(/(\.| )/g, '');
    }
    return str;
  })
  return terms;
}

const removeStopWords = (arr) => {
  const terms = arr.filter((str) => {
    return !_.includes(stopWords, str)
  })
  return terms
}

const removeAccent = (arr) => {
  const terms = arr.map((str) => {
    str = str.replace(/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/g, 'a');
    str = str.replace(/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/g, 'e');
    str = str.replace(/(ì|í|ị|ỉ|ĩ)/g, 'i');
    str = str.replace(/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/g, 'o');
    str = str.replace(/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/g, 'u');
    str = str.replace(/(ỳ|ý|ỵ|ỷ|ỹ)/g, 'y');
    str = str.replace(/(đ)/g, 'd');

    str = str.replace(/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ|A)/g, 'a');
    str = str.replace(/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ|E)/g, 'e');
    str = str.replace(/(Ì|Í|Ị|Ỉ|Ĩ|I)/g, 'i');
    str = str.replace(/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ|O)/g, 'o');
    str = str.replace(/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ|U)/g, 'u');
    str = str.replace(/(Ỳ|Ý|Ỵ|Ỷ|Ỹ|Y)/g, 'y');
    str = str.replace(/(Đ|D)/g, 'd');
    return str;
  })
  return terms
}
const removeFalsifies = (arr) => {
  const terms = arr.filter((str) => {
    return !_.includes(falsifies, str)
  })
  return terms;
}
const removeSpace = (arr) => {

}
/* 
 Mục đích: Xử lý query string của người dùng
 Input: Chuỗi query string
 Output: Mảng terms đã xử lý
*/
const main = (str) => {
  // Tách câu thành từ đơn
  var terms = tokenization1(str);
  // Loại bỏ kí tự đặc biệt
  terms = removeSpecialCharacters(terms);
  // Loại bỏ dấu chấm ngăn câu.
  terms = removeSeperatedSentencesDOT(terms);
  // Chuyển thành chữ thường
  terms = lowerCase(terms);
  // Bỏ dấu
  terms = removeAccent(terms);
  // Bỏ những falsifies (undefined, null, '')
  terms = removeFalsifies(terms);
  return terms;
}
//terms = removeStopWords(terms);
const tagAnalysis = (str) => {
  str = str.toLowerCase();
  str = str.replace(/( )/g, '');
  return str;
}
module.exports = {
  main,
  tagAnalysis
}
