/* 
  The file has 2 functions
  1. Recommend appropriate users to a specific recruitment
  2. Recommend appropriate recruitments to a specific user
*/

const async = require('async');
const _ = require('lodash');
const nlp = require('./vnNLP').tagAnalysis;
const Recruitment = require('../models/Recruitment');
const User = require('../models/User/User');
const Field1 = require('../models/Field/Field1');
const Field2 = require('../models/Field/Field2');
const Field3 = require('../models/Field/Field3');

const AT_LEAST_DEGREE = [["Không yêu cầu", 0], ["Trung cấp", 1], ["Cao đẳng", 2], ["Đại học", 3], ["Sau đại học", 4]];

const FALSIFIES = [undefined, NaN, null];
const RECRUITMENT_TYPES = { // descend in weight factor
  FIELD: { label: 'field', weighted: 0.4 },
  LOCATION: { label: 'location', weighted: 0.2 },
  POSITION: { label: 'position', weighted: 0.1 },
  EXPERIENCE: { label: 'experience', weighted: 0.1 },
  SKILL: { label: 'skill', weighted: 0.05 },
  DEGREE: { label: 'degree', weighted: 0.05 },
  LANGUAGE: { label: 'language', weighted: 0.05 },
  AGE: { label: 'age', weighted: 0.025 },
  GENDER: { label: 'gender', weighted: 0.025 },
}
const USER_TYPES = {
  FIELD: { label: 'field', weighted: 0.3 },
  LOCATION: { label: 'location', weighted: 0.2 },
  SALARY: { label: 'salary', weighted: 0.1 },
  EXPERIENCE: { label: 'experience', weighted: 0.1 },
  DEGREE: { label: 'degree', weighted: 0.1 },
  JOB_TYPE: { label: 'job type', weighted: 0.05 },
  POSITION: { label: 'position', weighted: 0.05 },
  LANGUAGE: { label: 'language', weighted: 0.025 },
  SKILL: { label: 'skill', weighted: 0.025 },
  AGE: { label: 'age', weighted: 0.025 },
  GENDER: { label: 'gender', weighted: 0.025 },
}
class Score {
  constructor(one, elemOfMany, type, typeArr) {
    this.recruitment = (!typeArr.SALARY) ? one : elemOfMany;
    this.user = (typeArr.SALARY) ? one : elemOfMany;
    this.type = type;
    this.typeArr = typeArr;
  }
  field() {
    const recruitmentFields = this.recruitment.field;
    try {
      if (!_.isEmpty(_.intersectionBy(recruitmentFields.field3, this.user.field.field3, 'code'))) {
        return 1;
      }
      else if (!_.isEmpty(_.intersectionBy(recruitmentFields.field2, this.user.field.field2, 'code'))) {
        return 0.75;
      }
      else if (!_.isEmpty(_.intersectionBy(recruitmentFields.field1, this.user.field.field1, 'code'))) {
        return 0.25;
      }
      else return 0;
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  location() {
    try {
      const recruitmentProvince = this.recruitment.location.province;
      const userProvince = this.user.address.province;
      const recruitmentDistrict = this.recruitment.location.district;
      const userDistrict = this.user.address.district;

      if (recruitmentProvince !== userProvince) {
        return 0;
      }
      if (recruitmentProvince === userProvince) {
        return 0.75;
      }
      if (recruitmentProvince === userProvince && recruitmentDistrict === userDistrict) {
        return 1;
      }
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  degree() {
    try {
      const mapDegreeToValue = (degreeArr, degree) => {
        const value = (degreeArr.find((elem) => elem[0] === degree))[1];
        return value;
      }
      const userDegree = mapDegreeToValue(AT_LEAST_DEGREE, this.user.educationLevel);
      const recruitmentDegree = mapDegreeToValue(AT_LEAST_DEGREE, this.recruitment.requirement.atLeastDegree);
      if (recruitmentDegree === 0) {
        return 1;
      }
      if (userDegree === 0) {
        return 0;
      }
      if (userDegree < recruitmentDegree) {
        return 0;
      }


      return recruitmentDegree / userDegree;
    } catch (error) {
      console.log(error);
      return 0;
    }

  }
  experience() {
    try {
      if (this.user.experience < this.recruitment.requirement.minExperience) {
        return 0;
      }
      const score = this.recruitment.requirement.minExperience / this.user.experience;
      if (_.includes(FALSIFIES, score)) return 1;

      return score;
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  age() {
    try {
      const then = new Date(this.user.birthdate).getTime();
      const now = Date.now();
      const age = (now - then) / (365 * 86400000);

      const minAge = this.recruitment.requirement.minAge;
      const maxAge = this.recruitment.requirement.maxAge;
      if (minAge <= age && age <= maxAge) {
        return 1;
      }
      return 0;
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  gender() {
    try {
      const recruitmentGender = this.recruitment.requirement.gender;
      if (recruitmentGender === "Không yêu cầu" || recruitmentGender === this.user.gender) {
        return 1;
      }
      return 0;
    } catch (error) {
      console.log(error);
      return 0;
    }

  }
  language() {
    try {

      const userLanguages = this.user._languages.map(elem => elem.language_name);

      const intersection =
        _.intersection(
          this.recruitment.requirement.languages,
          userLanguages
        )
      const maxScore = this.recruitment.requirement.languages.length;
      //console.log('LANGUAGE: ', intersection.length/ maxScore);
      if (isNaN(intersection.length / maxScore)) {
        return 0;
      }
      return intersection.length / maxScore;

    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  skill() { // user tag
    try {
      const recruitmentSkills = this.recruitment.requirement.skills.map(elem => nlp(elem));
      const userSkills = [];

      this.user._skills.forEach(elem => {
        elem.skill_name.forEach(skill => {
          userSkills.push(nlp(skill));
        })
      });
      const intersection =
        _.intersection(
          recruitmentSkills,
          userSkills
        )
      const maxScore = this.recruitment.requirement.skills.length;
      if (isNaN(intersection.length / maxScore)) {
        return 0;
      }
      return intersection.length / maxScore;
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  position() { // user tag
    try {
      const userPosition = this.user.positions.map(elem => nlp(elem));
      const recruitmentPosition = this.recruitment.requirement.positions.map(elem => nlp(elem));
      if (!_.isEmpty(_.intersection(recruitmentPosition, userPosition))) {
        return 1;
      }
      return 0;
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  salary() {
    try {
      if (this.recruitment.requirement.minSalary <= this.user.jobSearch.maxSalary
        && this.recruitment.requirement.maxSalary >= this.user.jobSearch.minSalary) {
        return 1;
      }
      return 0;
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  jobType() {
    try {
      if (!_.isEmpty(_.intersection(this.user.jobSearch.jobType, this.recruitment.requirement.jobType))) {
        return 1;
      }
      return 0;
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
  setScore() {
    switch (this.type) {
      case this.typeArr.FIELD:
        return this.field();
        break;
      case this.typeArr.LOCATION:
        return this.location();
        break;
      case this.typeArr.DEGREE:
        return this.degree();
        break;
      case this.typeArr.GENDER:
        return this.gender();
        break;
      case this.typeArr.EXPERIENCE:
        return this.experience();
        break;
      case this.typeArr.POSITION:
        return this.position();
        break;
      case this.typeArr.AGE:
        return this.age();
        break;
      case this.typeArr.SKILL:
        return this.skill();
        break;
      case this.typeArr.LANGUAGE:
        return this.language();
        break;
      case this.typeArr.SALARY:
        return this.salary();
        break;
      case this.typeArr.JOB_TYPE:
        return this.jobType();
        break;
      default:
        break;
    }
  }
}

class simCandidates {
  constructor(one, many, typeArr) {
    this.one = one;
    this.many = many;
    this.listOfProp = {};
    this.finalList = [];
    this.typeArr = typeArr;
  }
  // Xây dựng danh sách điểm cho từng attribute
  buildAttributeList(type) {
    const simList = [];
    for (var i = 0; i < this.many.length; i++) {
      const scoreInstance = new Score(this.one, this.many[i], type, this.typeArr);
      const similarity = scoreInstance.setScore() * type.weighted;
      simList.push({
        id: this.many[i]._id,
        similarity
      })
    }
    return simList;
  }
  // Xây dựng danh sách điểm cho tất cả các attribute
  buildListOfProp() {
    _.map(this.typeArr, (type) => {
      const list = this.buildAttributeList(type);
      this.listOfProp[type.label] = list;
    })
  }
  // Lấy tổng điểm tính được thành 1 field duy nhất
  buildFinalList() {
    _.map(this.listOfProp, (property) => {
      property.forEach(elem => {
        if (_.some(this.finalList, { id: elem.id })) {
          this.finalList.forEach((item) => {
            if (item.id.toString() === elem.id.toString()) {
              item.similarity += elem.similarity;
            }
          })
        }
        else {
          this.finalList.push(elem);
        }
      })
    })
  }
  // Sắp xếp danh sách
  sortList() {
    this.finalList = this.finalList.sort((a, b) => b.similarity - a.similarity);
  }
  main() {
    this.buildListOfProp();
    console.log("List Of Prop: ", this.listOfProp);
    const prop = _.cloneDeep(this.listOfProp);
    this.buildFinalList();
    this.sortList();
    //console.log("Final List: ", this.finalList);
    return { final: this.finalList, prop };

  }
}

const getFields = (initial, execFunction) => {
  async.waterfall(
    [
      (callback) => {
        Field3
          .find({ code: { $in: initial } })
          .exec((err, field3) => {
            if (err) throw err;
            callback(null, field3);
          })
      },
      (field3, callback) => {
        const parentCodes = field3.map(elem => elem.parentCode);
        Field2
          .find({ code: { $in: parentCodes } })
          .exec((err, field2) => {
            if (err) throw err;
            callback(null, field2, field3)
          })
      },
      (field2, field3, callback) => {
        const parentCodes = field2.map(elem => elem.parentCode);
        Field1
          .find({ code: { $in: parentCodes } })
          .exec((err, field1) => {
            if (err) throw err;
            callback(null, field1, field2, field3)
          })
      }
    ],
    (err, field1, field2, field3) => {
      if (err) throw err;
      const field = {
        field1, field2, field3
      }
      execFunction(field);
    }
  )
}


module.exports = {
  simCandidates,
  getFields,
  RECRUITMENT_TYPES,
  USER_TYPES,
  AT_LEAST_DEGREE,
  FALSIFIES
}