const express= require('express');
const middlewares = require('../libs/middlewares');
const controller = require('../api/invitation');


const invitationRoutes = express.Router();
invitationRoutes.route('/checkAll').all(middlewares.auth).post(controller.checkAll);
invitationRoutes.route('/push').all(middlewares.auth).post(controller.pushInvitation);
invitationRoutes.route('/check').all(middlewares.auth).post(controller.check);
invitationRoutes.route('/').post(controller.create);
invitationRoutes.route('/').all(middlewares.auth).put(controller.update);
invitationRoutes.route('/').all(middlewares.auth).get(controller.getInvitation);
module.exports = invitationRoutes;