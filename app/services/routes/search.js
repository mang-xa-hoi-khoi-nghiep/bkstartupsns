const express = require('express');
const controller = require('../api/search');


var searchRoutes = express.Router();



searchRoutes.route('/recruitment').post(controller.searchRecruitments);
searchRoutes.route('/user').post(controller.searchUsers);
searchRoutes.route('/organization').post(controller.searchOrganizations);
searchRoutes.route('/advanced-search').get(controller.advancedSearch);
module.exports = searchRoutes;