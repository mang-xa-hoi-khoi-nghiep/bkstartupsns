const express = require('express');
const controller = require('../api/event');
const middlewares = require('../libs/middlewares');


var multer  = require('multer');
var crypto = require('crypto');
var mime = require('mime');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/organization/avatar')
  },
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      cb(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
    });
  }
});
var upload = multer({ storage: storage });

var eventRoutes = express.Router();

eventRoutes.route('/:id').all(middlewares.auth).put(controller.update);
eventRoutes.route('/:id').all(middlewares.auth).delete(controller.delete);


module.exports = eventRoutes;
