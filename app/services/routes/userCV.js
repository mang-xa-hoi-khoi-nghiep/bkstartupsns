const express = require('express');
const controller = require('../api/userCV');

const { watchUserUpdate } = require('../libs/middlewares');
const middlewares = require('../libs/middlewares');

var multer  = require('multer');
var crypto = require('crypto');
var mime = require('mime');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/userCV/certification')
  },
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      cb(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
    });
  }
});

var upload = multer({ storage: storage });

var userCVRoutes = express.Router();

// get schools, majors and degrees
userCVRoutes.route('/get-schools').get(controller.getSchools);
userCVRoutes.route('/get-majors').get(controller.getMajors);
userCVRoutes.route('/get-degrees').get(controller.getDegrees);

// get one school, major and degree by id
userCVRoutes.route('/get-school-by-id').get(controller.getSchoolById);
userCVRoutes.route('/get-major-by-id').get(controller.getMajorById);
userCVRoutes.route('/get-degree-by-id').get(controller.getDegreeById);

// get districts, provinces
userCVRoutes.route('/get-districts').get(controller.getDistricts);
userCVRoutes.route('/get-provinces').get(controller.getProvinces);



userCVRoutes.use(upload.single('img'));

// education
userCVRoutes.route('/create-education').all(middlewares.auth).all(watchUserUpdate).post(controller.createEducation);
userCVRoutes.route('/edit-education').all(middlewares.auth).all(watchUserUpdate).post(controller.editEducation);
userCVRoutes.route('/delete-education').all(middlewares.auth).all(watchUserUpdate).post(controller.deleteEducation);

// experience
userCVRoutes.route('/create-experience').all(middlewares.auth).all(watchUserUpdate).post(controller.createExperience);
userCVRoutes.route('/edit-experience').all(middlewares.auth).all(watchUserUpdate).post(controller.editExperience);
userCVRoutes.route('/delete-experience').all(middlewares.auth).all(watchUserUpdate).post(controller.deleteExperience);

// project
userCVRoutes.route('/create-project').all(middlewares.auth).all(watchUserUpdate).post(controller.createProject);
userCVRoutes.route('/edit-project').all(middlewares.auth).all(watchUserUpdate).post(controller.editProject);
userCVRoutes.route('/delete-project').all(middlewares.auth).all(watchUserUpdate).post(controller.deleteProject);

// activity
userCVRoutes.route('/create-activity').all(middlewares.auth).all(watchUserUpdate).post(controller.createActivity);
userCVRoutes.route('/edit-activity').all(middlewares.auth).all(watchUserUpdate).post(controller.editActivity);
userCVRoutes.route('/delete-activity').all(middlewares.auth).all(watchUserUpdate).post(controller.deleteActivity);

// award
userCVRoutes.route('/create-award').all(middlewares.auth).all(watchUserUpdate).post(controller.createAward);
userCVRoutes.route('/edit-award').all(middlewares.auth).all(watchUserUpdate).post(controller.editAward);
userCVRoutes.route('/delete-award').all(middlewares.auth).all(watchUserUpdate).post(controller.deleteAward);

// course
userCVRoutes.route('/create-course').all(middlewares.auth).all(watchUserUpdate).post(controller.createCourse);
userCVRoutes.route('/edit-course').all(middlewares.auth).all(watchUserUpdate).post(controller.editCourse);
userCVRoutes.route('/delete-course').all(middlewares.auth).all(watchUserUpdate).post(controller.deleteCourse);

// publication
userCVRoutes.route('/create-publication').all(middlewares.auth).all(watchUserUpdate).post(controller.createPublication);
userCVRoutes.route('/edit-publication').all(middlewares.auth).all(watchUserUpdate).post(controller.editPublication);
userCVRoutes.route('/delete-publication').all(middlewares.auth).all(watchUserUpdate).post(controller.deletePublication);

// skill
userCVRoutes.route('/create-skill').all(middlewares.auth).all(watchUserUpdate).post(controller.createSkill);
userCVRoutes.route('/edit-skill').all(middlewares.auth).all(watchUserUpdate).post(controller.editSkill);
userCVRoutes.route('/delete-skill').all(middlewares.auth).all(watchUserUpdate).post(controller.deleteSkill);

// language
userCVRoutes.route('/create-language').all(middlewares.auth).all(watchUserUpdate).post(controller.createLanguage);
userCVRoutes.route('/edit-language').all(middlewares.auth).all(watchUserUpdate).post(controller.editLanguage);
userCVRoutes.route('/delete-language').all(middlewares.auth).all(watchUserUpdate).post(controller.deleteLanguage);

// certificate
userCVRoutes.route('/create-certificate').all(middlewares.auth).all(watchUserUpdate).post(controller.createCertificate);
userCVRoutes.route('/edit-certificate').all(middlewares.auth).all(watchUserUpdate).post(controller.editCertificate);
userCVRoutes.route('/delete-certificate').all(middlewares.auth).all(watchUserUpdate).post(controller.deleteCertificate);

// basic info
userCVRoutes.route('/edit-basic-info').all(middlewares.auth).all(watchUserUpdate).post(controller.editBasicInfo);

// jobSearch
userCVRoutes.route('/edit-jobSearch').all(middlewares.auth).all(watchUserUpdate).post(controller.editJobSearch);

module.exports = userCVRoutes;
