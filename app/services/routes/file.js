const express= require('express');
const middlewares = require('../libs/middlewares');
const controller = require('../api/file');
const multer  = require('multer');
const crypto = require('crypto');
const mime = require('mime');
const fs = require('fs');

// field param must be unique among all the middlewares
const fileUpload = (FILE_DESTINATION , TYPE , field) => {
  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      const newDestination = (req.body && req.body.itemId)? FILE_DESTINATION + '/' + req.body.itemId: FILE_DESTINATION;
      try {
        stat = fs.statSync(newDestination);
      } catch (err) {
          fs.mkdirSync(newDestination);
      }
      cb(null, newDestination);
    },
    filename: function (req, file, cb) {
      crypto.pseudoRandomBytes(16, function (err, raw) {
        cb(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
      });
    }
  });
  const upload = multer({ storage });
  return upload[TYPE](field);
}


const fileRoutes = express.Router();


fileRoutes.route('/image/organization/basic-info').all(fileUpload('./uploads/organization', 'fields', [
  {
    name: 'org_logo', maxCount: 1
  },
  {
    name: 'org_banner', maxCount: 1
  }
])).post(controller.org_basicInfo);

fileRoutes.route('/image/user/avatar').all(fileUpload('./uploads/user', 'single', 'avatar')).post(controller.u_avatar)

fileRoutes.route('/attach-file/recruitment').all(middlewares.auth).all(fileUpload('./uploads/recruitment', 'array', 'files'))
  .post(controller.rec_attachFiles);

fileRoutes.route('/attach-file/:recruitmentId/:userId').all(middlewares.auth).get(controller.getAttachFiles);

module.exports = fileRoutes;
