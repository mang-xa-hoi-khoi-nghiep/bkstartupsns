var express= require('express');
const controller = require('../api/location');
const middlewares = require('../libs/middlewares');
const locationRoutes = express.Router();

locationRoutes.route('/districts/district/:districtId').get(controller.getDistrict);
locationRoutes.route('/provinces/province/:provinceId').get(controller.getProvince);
locationRoutes.route('/districts/:provinceId').get(controller.getDistrictsByProvince);
locationRoutes.route('/existed-provinces/:type').get(controller.getExistedProvinces);
locationRoutes.route('/districts').get(controller.getDistricts);
locationRoutes.route('/provinces').get(controller.getProvinces);

module.exports = locationRoutes;