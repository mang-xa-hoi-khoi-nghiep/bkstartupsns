var express= require('express');
const middlewares = require('../libs/middlewares');
const controller = require('../api/recruitment');
var multer  = require('multer');
var crypto = require('crypto');
var mime = require('mime');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/recruitment/files')
  },
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      cb(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
    });
  }
});

var upload = multer({ storage: storage });
var recruitmentRoutes = express.Router();

recruitmentRoutes.use(upload.array('files'));
recruitmentRoutes.route('/rate').all(middlewares.auth).post(controller.rate);
recruitmentRoutes.route('/apply').all(middlewares.auth).post(controller.apply);
recruitmentRoutes.route('/view').post(controller.viewCount);
recruitmentRoutes.route('/filter').post(controller.filter);
recruitmentRoutes.route('/recruit').all(middlewares.auth).post(controller.recruit);
recruitmentRoutes.route('/sim-recruitment').all(middlewares.auth).get(controller.recommendBasedOnUser);
recruitmentRoutes.route('/sim-recruitment/:id').get(controller.recommendBasedOnRecruitment);
recruitmentRoutes.route('/recommend-user/:id').all(middlewares.auth).get(controller.recommendUser);
recruitmentRoutes.route('/recruitment-userCMS/:id').all(middlewares.auth).get(controller.getOneForUserCMS);
recruitmentRoutes.route('/like/:id').all(middlewares.auth).post(controller.like);
recruitmentRoutes.route('/follow/:id').all(middlewares.auth).post(controller.follow);

recruitmentRoutes.route('/create').all(middlewares.auth).post(controller.create);
recruitmentRoutes.route('/get-by-user-id').all(middlewares.auth).get(controller.getByUserId);
recruitmentRoutes.route('/get-applied-recruitments').all(middlewares.auth).get(controller.getAppliedByUserId);

recruitmentRoutes.route('/interested-candidates/invitation').all(middlewares.auth).delete(controller.cancelInvitation);
recruitmentRoutes.route('/interested-candidates').all(middlewares.auth).post(controller.createInterestedCandidates);
recruitmentRoutes.route('/interested-candidates').all(middlewares.auth).delete(controller.removeInterestedCanidate);

recruitmentRoutes.route('/:id').get(controller.getOne);
recruitmentRoutes.route('/:id').all(middlewares.auth).put(controller.update);
recruitmentRoutes.route('/:id').all(middlewares.auth).delete(controller.delete);

recruitmentRoutes.route('/').get(controller.getAll);
module.exports = recruitmentRoutes;
