var express= require('express');
const controller = require('../api/history');
const middlewares = require('../libs/middlewares');
const historyRoutes = express.Router();


historyRoutes.route('/user-view-history').all(middlewares.auth).post(controller.trackUserViewHistory);
module.exports = historyRoutes;
