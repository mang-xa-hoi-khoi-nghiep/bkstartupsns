var express= require('express');
const controller = require('../api/field');
const config = require('../../config');

var fieldRoutes = express.Router();

fieldRoutes.route('/field1').get(controller.getField1);
fieldRoutes.route('/field2').get(controller.getField2);
fieldRoutes.route('/field3').get(controller.getField3);

fieldRoutes.route('/field1/:code').get(controller.getOne1);
fieldRoutes.route('/field2/:code').get(controller.getOne2);
fieldRoutes.route('/field3/:code').get(controller.getOne3);

fieldRoutes.route('/field3Options').get(controller.getField3Options);


module.exports = fieldRoutes;
