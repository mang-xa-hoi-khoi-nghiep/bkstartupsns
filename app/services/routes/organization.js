const express = require('express');
const controller = require('../api/organization');
const middlewares = require('../libs/middlewares');


var multer  = require('multer');
var crypto = require('crypto');
var mime = require('mime');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/organization/avatar')
  },
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      cb(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
    });
  }
});
var upload = multer({ storage: storage });

var organizationRoutes = express.Router();
organizationRoutes.use(upload.single('img'));

organizationRoutes.route('/me').all(middlewares.auth).get(controller.getBasedOnUser);
organizationRoutes.route('/filter').post(controller.filter);
organizationRoutes.route('/view').post(controller.viewCount);

organizationRoutes.route('/employee/:id').all(middlewares.auth).put(controller.updateEmployee);
organizationRoutes.route('/employee/:id').all(middlewares.auth).delete(controller.deleteEmployee);
organizationRoutes.route('/employee/:id').all(middlewares.auth).get(controller.getEmployee);
organizationRoutes.route('/employee').all(middlewares.auth).post(controller.createEmployee);

organizationRoutes.route('/event').all(middlewares.auth).post(controller.createEvent);

organizationRoutes.route('/:id').get(controller.getOne);
organizationRoutes.route('/:id').all(middlewares.auth).put(controller.update);
organizationRoutes.route('/:id').all(middlewares.auth).delete(controller.delete);
organizationRoutes.route('/').all(middlewares.auth).post(controller.create);


module.exports = organizationRoutes;
