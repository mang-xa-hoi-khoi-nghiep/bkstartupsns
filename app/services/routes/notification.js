const express= require('express');
const middlewares = require('../libs/middlewares');
const controller = require('../api/notification');


const notificationRoutes = express.Router();
notificationRoutes.route('/checkAll').all(middlewares.auth).post(controller.checkAll);
notificationRoutes.route('/push').all(middlewares.auth).post(controller.pushNotification);
notificationRoutes.route('/check').all(middlewares.auth).post(controller.check);
notificationRoutes.route('/').post(controller.create);
notificationRoutes.route('/').all(middlewares.auth).get(controller.getNotification);
module.exports = notificationRoutes;