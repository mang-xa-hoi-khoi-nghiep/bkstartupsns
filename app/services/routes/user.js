const express = require('express');
const controller = require('../api/user');
const middlewares = require('../libs/middlewares');


var multer  = require('multer');
var crypto = require('crypto');
var mime = require('mime');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/user/avatar')
  },
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      cb(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
    });
  }
});

var upload = multer({ storage: storage });

var userRoutes = express.Router();

userRoutes.route('/register').post(controller.register);
userRoutes.route('/authenticate').post(controller.authenticate);

userRoutes.route('/get-active-users').get(controller.getActiveUsers);
userRoutes.route('/recommend-recruitment/:id').get(controller.recommendRecruitment);
userRoutes.route('/view').post(controller.viewCount);
userRoutes.route('/sim-user/:id').get(controller.recommendBasedOnUser);
userRoutes.route('/filter').post(controller.filter);
userRoutes.route('/get-one/:id').get(controller.getOne);


userRoutes.route("/get-all").get(controller.getAllUser);
userRoutes.route("/test").get(controller.test);
userRoutes.use(upload.single('img'));
userRoutes.route('/sim-user').all(middlewares.auth).get(controller.recommendBasedOnPersonalReference);

// get list of users
userRoutes.route('/get-all').all(middlewares.auth).get(controller.getAll);
userRoutes.route('/search').all(middlewares.auth).get(controller.searchUsers);
// check if token exist or not
userRoutes.route('/validate').all(middlewares.auth).get(controller.validate);

// get user info
userRoutes.route('/user-info').all(middlewares.auth).post(controller.userInfo);

// update user
userRoutes.route('/update-user-info').all(middlewares.auth).post(controller.updateUserInfo);
userRoutes.route('/change-pass').all(middlewares.auth).post(controller.changePass);
userRoutes.route('/delete-user').all(middlewares.auth).post(controller.deleteUser);


module.exports = userRoutes;
