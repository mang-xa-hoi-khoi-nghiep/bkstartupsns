const express = require('express');
const controller = require('../api/school');

var schoolRoutes = express.Router();

schoolRoutes.route('/:id').get(controller.getOne);
schoolRoutes.route('/').get(controller.getAll);

module.exports = schoolRoutes;