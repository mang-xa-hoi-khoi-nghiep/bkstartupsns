import React, { Component } from 'react'
import OrganizationFilter from './OrganizationFilter';
import ListOrganization from './ListOrganization';
import _ from 'lodash';
import request from 'superagent';

export const FILTER_TYPES = {
  FIELDS: 'fields',
  TYPE: 'type',
  PROVINCE: 'province',
}
export const CHECKBOX_TYPES = {
  TYPE: 'type',
  PROVINCE: 'province',  
}
export const SORT_TYPES = {
  TRENDING: 'trending',
  NEWEST: 'newest',
  RELEVANT: 'relevant'
}
export const PAGE_SIZE = 5;
export const MODES = {
  FILTER: 0,
  SEARCH: 1
}
class OrganizationHolder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // mode
      mode: MODES.FILTER,
      sortMode: SORT_TYPES.NEWEST,
      // list organization
      organizations: [],
      total: 0,
      page: 0,
      elemCount: 0,
      // organization filter
      searchStr: "",
      displayStr: "",
      fields: [],
      type: [],
      province: [],
      filterProvinces: []
    }
    this.watchFilterChange = this.watchFilterChange.bind(this);
    this.selectFuctionBasedOnMode = this.selectFuctionBasedOnMode.bind(this);
    this.retrieveSearchResults = this.retrieveSearchResults.bind(this);
    this.getSearchInput = this.getSearchInput.bind(this);
    this.getSortOption = this.getSortOption.bind(this);
  }
  watchFilterChange(value, filterType) {
    const newCheckboxState = (arr, value) => {
      const tmp = _.uniqBy([...arr, value].reverse(), 'value').filter(elem => elem.checked);
      return tmp;
    }
    switch (filterType) {
      case FILTER_TYPES.FIELDS:
        this.setState({fields: value}, () => this.selectFuctionBasedOnMode())
        break;
      case FILTER_TYPES.TYPE:
        const { type } = this.state;
        var newArr = newCheckboxState(type, value);
        this.setState({type: newArr}, () => this.selectFuctionBasedOnMode())
        break;
      case FILTER_TYPES.PROVINCE:
        const { province } = this.state;
        var newArr = newCheckboxState(province, value);
        this.setState({province: newArr}, () => this.selectFuctionBasedOnMode())
        break;
      default:
        break;
    }
  }
  setFilterOptions() {
    const {fields, type, province} = this.state;
    const data = {
      fields,
      type: type.map(elem => elem.value),
      province: province.map(elem => elem.value),
    }
    return data;
  }
  postFilterOptions(page = 0, pageSize = PAGE_SIZE, sortMode = SORT_TYPES.TRENDING) {
    const data = this.setFilterOptions();
    // console.log(data);
    this.setState({organizations: []}, () => {
      request
      .post('/api/organization/filter')
      .query({page, PAGE_SIZE: pageSize, sortMode})
      .send(data)
      .end((err, res) => {
        if (err) {
          console.log(err);
        }
        else {
          const organizations = 
          (sortMode === SORT_TYPES.TRENDING)
          ? res.body.items
          : (sortMode === SORT_TYPES.NEWEST)
          ? res.body.organizations
          : [];
          const { total, page, elemCount } = res.body;
          this.setState({organizations, total, page: Number(page), elemCount});
        }
      })
    }) 
  }
  retrieveSearchResults(page = 0, pageSize = PAGE_SIZE, sortMode= SORT_TYPES.RELEVANT) {
    const data = this.setFilterOptions();
    const { searchStr } = this.state;
    if (!searchStr) return;
    this.setState({organizations: [], mode: MODES.SEARCH, displayStr: searchStr, sortMode}, () => {
      request
      .post('/api/search/organization')
      .query({searchStr, page, PAGE_SIZE: pageSize, sortMode })
      .send(data)
      .end((err, res) => {
        if(err) {
          console.log(err);
        }
        else {
          // const { sortedHits, total, page, elemCount } = res.body;
          // async.each(
          //   sortedHits, 
          //   (elem, next) =>{
          //     request
          //     .get(`/api/field/field3/${elem.jobField}`)
          //     .end((err, res) => {
          //       if (err) {
          //         console.log(err);
          //       }
          //       else {
          //         elem.fieldName = res.body.result;
          //       }
          //       next();
          //     })
          //   },
          //   (err) => {
          //     if (err) console.log(err);
          //     this.setState({cvs: sortedHits, total, page: Number(page), elemCount})
          //   }
          // )
          const { sortedHits, total, page, elemCount } = res.body;
          this.setState({organizations: sortedHits, total, page: Number(page), elemCount})
        }
      })
    })
  }
  selectFuctionBasedOnMode(page = 0, pageSize = PAGE_SIZE) {
    if (this.state.mode === MODES.FILTER) {
      this.postFilterOptions(page, PAGE_SIZE, this.state.sortMode);
    }
    else {
      console.log(this.state.sortMode);
      this.retrieveSearchResults(page, PAGE_SIZE, this.state.sortMode);
    }
  }
  getSearchInput(value) {
    this.setState({searchStr: value});
  }
  getSortOption(value) {
    this.setState({sortMode: value}, () => this.selectFuctionBasedOnMode());
  }
  getFilterProvinces() {
    request
    .get('/api/location/existed-provinces/organization')
    .end((err, res) => {
      if (err) console.log(err);
      else {
        this.setState({filterProvinces: res.body.provinces});
      }
    })
  }
  componentWillMount () {
    this.selectFuctionBasedOnMode();
    this.getFilterProvinces();
  }
  
  render () {
    const {filterProvinces, organizations, total, page, elemCount, displayStr, mode, sortMode} = this.state;
    
    return (
      <div className="container">
        <div className="push"></div>
        <div className="row">
          <ListOrganization
            organizations={organizations}
            total={total}
            page={page}
            elemCount={elemCount}
            displayStr={displayStr}
            mode={mode}
            sortMode={sortMode}
            selectFuctionBasedOnMode={this.selectFuctionBasedOnMode}
            getSortOption={this.getSortOption}
          />

          <OrganizationFilter  
            watchFilterChange={this.watchFilterChange} 
            getSearchInput={this.getSearchInput}
            retrieveSearchResults={this.retrieveSearchResults}
            filterProvinces={filterProvinces}        
          />
        </div>
      </div>
    )
  }
}

export default OrganizationHolder;