import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import Loading from 'react-loading-bar';
import 'react-loading-bar/dist/index.css';
import Header from './fixedComponents/Header';
import Footer from './fixedComponents/Footer';
import request from 'superagent';
import _ from 'lodash';
import async from 'async';
import { delay } from '../commons/share';
import { env } from "../commons/env";
import { ACCEPTED_TYPES, INVITATION_TYPES } from '../commons/constants';
const CronJob = require('cron').CronJob;
const $ = window.jQuery;
class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading_bar_show: false,
      user: {},

      recuitments: [],
      total_recuitments: 0,

      searchResults: [],
      totalSearchResults: 0,
      searchLoading: false,
      users: [],
      total_users: 0,

      // notification
      notifications: [],
      //invitation
      invitations: []
    }
    this.logout = this.logout.bind(this);
    this.validateUser = this.validateUser.bind(this);
    this.checkLoginState = this.checkLoginState.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.handleNotiClick = this.handleNotiClick.bind(this);
    this.checkAllNotifications = this.checkAllNotifications.bind(this);
  }
  validateUser() {
    if (localStorage.getItem('token')) {
      const token = localStorage.getItem('token');
      $.ajax({
        method: "GET",
        url: env.serverUrl + "/api/user/validate",
        headers: {
          'x-access-token': token
        },
        //async: false,
      }).done(response => {
        if (response.success) {
          // get notification and invitation
          async.parallel({
            notifications: (callback) => {
              request
              .post(env.serverUrl + '/api/notification/push')
              .set('x-access-token', token)
              .send({ data: 1 })
              .end((err, res) => {
                if (err) console.log(err);
                else {
                  request
                    .get(env.serverUrl + '/api/notification')
                    .set('x-access-token', token)
                    .end((err, res) => {
                      callback(null, res.body.notifications)
                    })
                }
              })
            },
            invitations: (callback) => {
              request
              .post(env.serverUrl + '/api/invitation/push')
              .set('x-access-token', token)
              .send({ data: 1 })
              .end((err, res) => {
                if (err) console.log(err);
                else {
                  request
                    .get(env.serverUrl + '/api/invitation')
                    .set('x-access-token', token)
                    .end((err, res) => {
                      callback(null, res.body.invitations)
                    })
                }
              })
            }
          }, (err, results) => {
            if (err) console.log(err);
            else {
              const { notifications, invitations } = results;
              this.setState({user: response.user, notifications, invitations})
            }
          })
        }
        else {
          if (localStorage.getItem('token')) {
            localStorage.removeItem('token');
          }
          console.log("Bạn chưa đăng ký hoặc tài khoản này đã bị xóa.");
        }
      })
    }
    else {
      console.log('Chưa đăng nhập');
    }
  }
  checkLoginState(message) {
    if (!localStorage.getItem('token')) {
      
      browserHistory.push({
        pathname: '/account',
      });
      return true;
    }
    else {
      return false;
    }
  }
  logout() {
    localStorage.removeItem('token');
    this.setState({ user: {} }, () => location.reload());
  }
  

  
  

  getUsers = (page, PAGE_SIZE) => {
    $.ajax({
      method: "GET",
      url: env.serverUrl + "/api/user/get-active-users?page=" + page + "&page_size=" + PAGE_SIZE + "",
    }).done((response) => {
      if (response.success) {
        const { users, total_elements } = response;
        this.setState({ users, total_users: total_elements });
      }
      else {
        console.log(response.message);
      }
    })
  }
  receiveNotification() {
    const data = { data: 1 }
    const token = localStorage.getItem('token');
    if (!token) return;

    return new CronJob('1 * * * * *', () => {
      request
        .post(env.serverUrl + '/api/notification/push')
        .set('x-access-token', token)
        .send(data)
        .end((err, res) => {
          if (err) console.log(err);
          else {
            request
              .get('/api/notification')
              .set('x-access-token', token)
              .end((err, res) => {
                this.setState({ notifications: res.body.notifications })
              })
          }
        })
    }, null, true, 'Asia/Ho_Chi_Minh');
  }
  receiveInvitation() {
    const data = { data: 1 }
    const token = localStorage.getItem('token');
    if (!token) return;

    return new CronJob('1 * * * * *', () => {
      request
        .post(env.serverUrl + '/api/invitation/push')
        .set('x-access-token', token)
        .send(data)
        .end((err, res) => {
          if (err) console.log(err);
          else {
            request
              .get(env.serverUrl + '/api/invitation')
              .set('x-access-token', token)
              .end((err, res) => {
                this.setState({ invitations: res.body.invitations })
              })
          }
        })
    }, null, true, 'Asia/Ho_Chi_Minh');
  }

  componentWillMount() {
    //localStorage.clear();
    // $(document).ajaxStart(() => {
    //   this.setState({loading_bar_show: true});
    // })
    // $(document).ajaxStop(() => {
    //   this.setState({loading_bar_show: false});
    // })
    this.validateUser();
    this.receiveNotification();
    this.receiveInvitation();
    
    this.getUsers(0, 10);
  }

  getSearchResult(str, page, PAGE_SIZE) {
    //if (str) {
    this.setState({ searchLoading: true }, () => {
      request
        .get(env.serverUrl + '/api/search')
        .query({
          searchStr: str,
          page: page,
          PAGE_SIZE: PAGE_SIZE
        })
        .end((err, res) => {
          if (err) {
            console.log(err);
          }
          else {
            const { sortedHits, totalSearchResults } = res.body;
            this.setState({
              searchResults: sortedHits,
              totalSearchResults,
              searchLoading: false
            })
          }
        })
    })
    // }
  }
  handleSearch(e, str, page = 0, PAGE_SIZE = 20) {
    e.preventDefault();
    this.getSearchResult(str, page, PAGE_SIZE);
  }

  handleSearchChange(e, context, page = 0, PAGE_SIZE = 10) {
    e.persist();
    const str = context.refs.searchInput.value;
    delay(() => {
      this.getSearchResult(str, page, PAGE_SIZE);
    }, 250);
  }
  handleNotiClick(id, url) {
    const token = localStorage.getItem('token');
    if (token) {
      request
        .post(env.serverUrl + '/api/notification/check')
        .set('x-access-token', token)
        .send({ id })
        .end((err, res) => {
          if (err) console.log(err);
          else {
            const { updated } = res.body;
            const { notifications } = this.state;
            const newArr = notifications.map(elem => {
              if (elem._id.toString() === updated._id.toString()) {
                return updated;
              }
              return elem;
            })
            this.setState({ notifications: newArr });
            browserHistory.push(url)
          }
        })
    }
  }

  checkAllNotifications() {
    const token = localStorage.getItem('token');
    if (token) {
      request
        .post(env.serverUrl + '/api/notification/checkAll')
        .set('x-access-token', token)
        .send({ data: 1 })
        .end((err, res) => {
          if (err) console.log(err);
          else {
            this.setState({ notifications: res.body.notifications });
          }
        })
    }
  }

  handleInvitationClick = (id) => {
    const token = localStorage.getItem('token');
    if (token) {
      request
        .post(env.serverUrl + '/api/invitation/check')
        .set('x-access-token', token)
        .send({ id })
        .end((err, res) => {
          if (err) console.log(err);
          else {
            const { updated } = res.body;
            const { invitations } = this.state;
            const newArr = invitations.map(elem => {
              if (elem._id.toString() === updated._id.toString()) {
                return updated;
              }
              return elem;
            })
            this.setState({ invitations: newArr });
          }
        })
    }
  }
  handleInvitationAccepted = (id, isAccepted, type, _recruitment) => {
    const token = localStorage.getItem('token');
    if (!token) return;
    if (type === INVITATION_TYPES.RECRUITMENT && isAccepted === ACCEPTED_TYPES.ACCEPTED) {
      browserHistory.push({
        pathname: `/recruitments/apply/${_recruitment}`,
        state: { invitationId: id }
      })
      return;
    }
    request
    .put(env.serverUrl + '/api/invitation')
    .set('x-access-token', token)
    .send({ id, isAccepted })
    .end((err, res) => {
      if (err) console.log(err);
      else {
        request
        .get('/api/invitation')
        .set('x-access-token', token)
        .end((err, res) => {
          this.setState({invitations: res.body.invitations});
        })
      }
    })
  }

  checkAllInvitations = () => {
    const token = localStorage.getItem('token');
    if (token) {
      request
        .post(env.serverUrl + '/api/invitation/checkAll')
        .set('x-access-token', token)
        .send({ data: 1 })
        .end((err, res) => {
          if (err) console.log(err);
          else {
            this.setState({ invitations: res.body.invitations });
          }
        })
    }
  }
  render() {
    const { user } = this.state;
    
    const { recuitments, total_recuitments, users, total_users, searchLoading, notifications, invitations } = this.state;
    
    const children = React.Children.map(this.props.children,
      (child) => React.cloneElement(child, {
        user,
        recuitments,
        total_recuitments,
        users,
        
        total_users,
        validateUser: this.validateUser,
        checkLoginState: this.checkLoginState,

      }))
    return (
      <div className='App'>
        <Header user={this.state.user}
          logout={this.logout}
          searchLoading={searchLoading}
          notifications={notifications}
          invitations={invitations}
          handleNotiClick={this.handleNotiClick}
          checkAllNotifications={this.checkAllNotifications}
          handleInvitationClick={this.handleInvitationClick}
          checkAllInvitations={this.checkAllInvitations}
          handleInvitationAccepted={this.handleInvitationAccepted}

        />
        <Loading
          show={this.state.loading_bar_show}
          color="red"
        />
        <div className="app-body">
          <main className="main overflow-y-hidden">
            {children}
            <Footer />
          </main>
        </div>
      </div>
    );
  }
}

export default App;
