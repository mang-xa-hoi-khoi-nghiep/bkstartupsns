import React, { Component } from 'react'
import { Link } from 'react-router';
import ListItem from '../share/ListItem';
import request from 'superagent';
import _ from 'lodash';
import moment from 'moment';
import Spinner from 'react-spinkit';
import { SALARY_VALUES, EXPERIENCE_VALUES, SORT_TYPES } from '../recruitmentHolder/RecruitmentHolder';
import LazyLoad from 'react-lazyload';

const FULL_SIZE = 10;

class ListRecruitment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recruitments: [],
    }
  }

  postFilterOptions(page = 0, pageSize = FULL_SIZE, sortMode = SORT_TYPES.NEWEST) {
    const data = {
      salary: [SALARY_VALUES.MIN, SALARY_VALUES.MAX],
      experience: EXPERIENCE_VALUES.MIN
    }
    this.setState({ recruitments: [] }, () => {
      request
        .post('/api/recruitment/filter')
        .query({ page, PAGE_SIZE: pageSize, sortMode })
        .send(data)
        .end((err, res) => {
          if (err) {
            console.log(err);
          }
          else {
            const { recruitments } = res.body;
            this.setState({ recruitments });
          }
        })
    })
  }
  renderList(propRecruitments) {
    try {
      if (_.isEmpty(propRecruitments)) {
        return (
          <div className="row">
            <div className="col-sm-12 spinner-container">
              <Spinner name="three-bounce" color="#14b1bb" className="spinner-center" />
            </div>
          </div>
        )
      }
      const recruitments = propRecruitments.map(
        (elem, index) => {
          var organizationName = '', organizationSlogan = '', organizationLogo = '', arr = [];
          if (!elem._organization) { }
          else if (!elem._organization.id) {
            organizationName = elem._organization.name;
          }
          else if (elem._organization.id) {
            organizationName = elem._organization.id.name;
            organizationSlogan = elem._organization.id.slogan;
            try {
              arr = elem._organization.id._logo.url.split('/');
              organizationLogo = '/' + [arr[arr.length - 2], arr[arr.length - 1]].join('/');
            } catch (error) { }
          }
          return (
            <LazyLoad once={true} height={200} key={elem._id} offset={[-100, 0]}>
              <ListItem
                linkTo={`/recruitments/${elem._id}`}
                imageUrl={organizationLogo}
                name={elem.title}
                sub1={organizationName}
                sub2={organizationSlogan}
                col1_footer={`Cập nhật ${moment(new Date(elem.updatedDate), "YYYYMMDD").fromNow()}`}
                sub4={(elem.view) ? elem.view.total : 0}
                col2_topleft={`${elem.location.streetNumber} ${elem.location.ward} ${elem.location.district} ${elem.location.province}`}
                col2_topright=""
                col2_btmleft={elem.requirement.jobType}
                col2_btmright=""
                col2_footer={elem.requirement.positions}
                col3_top={`Lương: ${elem.requirement.minSalary} - ${elem.requirement.maxSalary} triệu`}
                tags={elem.requirement.skills}
              />
            </LazyLoad>)
        }
      ).filter(elem => elem !== null);
      return (
        <div className="row">
          <div className="col-sm-8">
            <div className="list-item">
              {recruitments}
            </div>
          </div>
        </div>
      )
    } catch (error) {
      console.log(error);
      return <div>404 Not Found.</div>
    }
  }


  componentWillMount() {
    this.postFilterOptions();
  }

  render() {
    const { recruitments, show } = this.state;
    return (
      <div style={{ marginBottom: 10 }}>
        {/* <div className="row">
          <div className="col-sm-8">
            <h2 className="pull-left">Tin tuyển dụng</h2>
            <div className="pull-right" style={{paddingTop: "10px", fontStyle: "italic"}}>
              <Link to="/recruitments">Xem tất cả <i className="angle double right icon"></i></Link>
            </div>
          </div>
        </div> */}
        <h2><Link to="/recruitments" style={{ color: "black" }}>Tin tuyển dụng</Link></h2>
        {this.renderList(recruitments)}

      </div>
    )
  }
}

export default ListRecruitment