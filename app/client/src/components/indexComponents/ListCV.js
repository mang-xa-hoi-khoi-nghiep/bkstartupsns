import React, { Component } from 'react'
import { Link } from 'react-router';
import ListItem from '../share/ListItem';
import request from 'superagent';
import _ from 'lodash';
import moment from 'moment';
import Spinner from 'react-spinkit';
import { AGE_VALUES, EXPERIENCE_VALUES, SORT_TYPES } from '../cvHolder/CVHolder';
import LazyLoad from 'react-lazyload';
const FULL_SIZE = 10;
class ListCV extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cvs: [],
    }
  }

  renderCVs(propCVs) {
    try {
      if (_.isEmpty(propCVs)) {
        return (
          <div className="row">
            <div className="col-sm-12 spinner-container">
              <Spinner name="three-bounce" color="#14b1bb" className="spinner-center" />
            </div>
          </div>
        )
      }
      const cvs = propCVs.map(
        (elem, index) => {
          const languages = elem._languages.map(lang => lang.language_name);
          const skills = _.flattenDeep(elem._skills.map(skill => skill.skill_name));
          var arr = [], avatar = '';
          try {
            arr = elem._avatar.url.split('/');
            avatar = '/' + [arr[arr.length - 2], arr[arr.length - 1]].join('/');
          } catch (error) { }
          return (
            <LazyLoad once={true} height={200} key={elem._id} offset={[-100, 0]}>
              <ListItem
                linkTo={`/cvs/${elem._id}`}
                imageUrl={avatar}
                name={`${elem.lastname} ${elem.firstname}`}
                sub1={(elem.fieldName) ? elem.fieldName.viName : ""}
                sub2={(elem.fieldName) ? elem.fieldName.engName : ""} // jobField
                sub4={(elem.view) ? elem.view.total : 0}
                sub5={(elem.jobSearch.isOn) ? 'Đang tìm việc' : ''}
                col1_footer={`Cập nhật ${moment(new Date(elem.updatedDate), "YYYYMMDD").fromNow()}`}
                col2_topleft={`${elem.address.streetNo} ${elem.address.ward} ${elem.address.district} ${elem.address.province}`}
                col2_topright=""
                col2_btmleft={elem.positions} // array
                col2_btmright=""
                col2_footer={languages} // array
                col3_top={`Có ${elem.experience} năm kinh nghiệm`}
                tags={skills} //array
              />
            </LazyLoad>)
        }
      ).filter(elem => elem !== null);
      return (
        <div className="row">
          <div className="col-sm-8">
            <div className="list-item">
              {cvs}
            </div>
          </div>
        </div>
      )
    } catch (error) {
      console.log(error);
      return <div>404 Not Found.</div>
    }
  }

  postFilterOptions(page = 0, pageSize = FULL_SIZE, sortMode = SORT_TYPES.NEWEST) {
    const data = {
      age: [AGE_VALUES.MIN, AGE_VALUES.MAX],
      experience: EXPERIENCE_VALUES.MIN,
    }
    this.setState({ cvs: [] }, () => {
      request
        .post('/api/user/filter')
        .query({ page, PAGE_SIZE: pageSize, sortMode })
        .send(data)
        .end((err, res) => {
          if (err) {
            console.log(err);
          }
          else {
            const { cvs } = res.body;
            this.setState({ cvs });
          }
        })
    })
  }

  componentWillMount() {
    this.postFilterOptions();
  }
  render() {
    const { cvs, show } = this.state;
    return (
      <div>
        {/* <div className="row">
          <div className="col-sm-8">
            <h2 className="pull-left">Hồ sơ</h2>
            <div className="pull-right" style={{paddingTop: "10px", fontStyle: "italic"}}>
              <Link to="/cvs">Xem tất cả <i className="angle double right icon"></i></Link>
            </div>
          </div>
        </div> */}
        <h2><Link to="/cvs" style={{ color: "black" }}>Hồ sơ</Link></h2>
        {this.renderCVs(cvs)}

      </div>
    )
  }
}

export default ListCV