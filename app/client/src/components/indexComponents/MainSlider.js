import React from 'react';
import {Link} from 'react-router';
import slider1 from '../../assets/img/slider1.jpg';
import slider2 from '../../assets/img/slider2.jpg';
import slider3 from '../../assets/img/slider3.jpg';
import slider4 from '../../assets/img/slider4.jpg';

export default class MainSlider extends React.Component {
    render() {
        return (
            <div id="myCarousel" className="carousel slide" data-ride="carousel">

                <ol className="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" className="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                </ol>


                <div className="carousel-inner">
                    <div className="item active">
                        <img src={slider1} alt="Los Angeles" width="100%" />
                        <div className="carousel-caption">
                            <h2>BKStartup</h2>
                            <h3>Nơi cung cấp các dịch vụ gọi vốn và nhân sự cho dự án khởi nghiệp</h3>
                        </div>
                    </div>

                    <div className="item">
                        <img src={slider2} alt="Chicago" width="100%" />
                        <div className="carousel-caption">
                            <h2>Bạn thiếu nhân sự ?</h2>
                            <h3>Chúng tôi có những ứng viên tuyệt vời</h3>
                            <p>
                                <Link to="/user/recruitment/create" className="btn btn-lg btn-info">Gọi nhân sự</Link>
                            </p>
                        </div>
                    </div>

                    <div className="item">
                        <img src={slider3} alt="New York" width="100%" />
                        <div className="carousel-caption">
                            <h2>Bạn muốn đăng hồ sơ ?</h2>
                            <h3>Chúng tôi sẽ hỗ trợ bạn</h3>
                            {/* <p>
                                <Link to="/new-project" className="btn btn-lg btn-info">Đăng dự án</Link>
                            </p> */}
                        </div>
                    </div>

                    <div className="item">
                        <img src={slider4} alt="New York" width="100%" />
                        <div className="carousel-caption">
                            <h2>Bạn muốn tìm việc ?</h2>
                            <h3>Đây là nơi tốt nhất để bắt đầu</h3>
                            <p>
                                <Link to="/recruitments" className="btn btn-lg btn-info">Tìm việc</Link>
                            </p>
                        </div>
                    </div>
                </div>


                <a className="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span className="glyphicon glyphicon-chevron-left"></span>
                    <span className="sr-only">Previous</span>
                </a>
                <a className="right carousel-control" href="#myCarousel" data-slide="next">
                    <span className="glyphicon glyphicon-chevron-right"></span>
                    <span className="sr-only">Next</span>
                </a>
            </div>
        )
    }
}