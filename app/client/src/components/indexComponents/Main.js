import React from 'react';
import MainSlider from './MainSlider';
import ListOrganizations from './ListOrganizations'
import ListRecruitment from './ListRecruitment';
import ListCV from './ListCV';
class Main extends React.Component {
  
  componentWillMount () {
    window.scrollTo(0, 0);
  }
  
  render() {
    return (
      <div>
        <MainSlider />
        <div className="container">
          <div className="push"></div>
          <ListRecruitment />
          <hr/>
          <ListCV />
          <hr />
          {/* <ListOrganizations /> */}
        </div>
      </div>
    )
  }
}

export default Main;
