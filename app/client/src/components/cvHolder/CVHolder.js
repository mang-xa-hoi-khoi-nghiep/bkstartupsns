import React, { Component } from 'react'
import ListCV from './ListCV';
import CVFilter from './CVFilter';
import _ from 'lodash';
import request from 'superagent';
import async from 'async';
export const FILTER_TYPES = {
  FIELDS: 'fields',
  AGE: 'age',
  EXPERIENCE: 'experience',
  DEGREE: 'degree',
  GENDER: 'gender',
  PROVINCE: 'province',
  DISTRICT: 'district',
  IS_JOB_SEARCH_ON: 'isJobSearchOn'
}
export const CHECKBOX_TYPES = {
  GENDER: 'gender',
  DEGREE: 'degree',
  PROVINCE: 'province'
}
export const SORT_TYPES = {
  TRENDING: 'trending',
  NEWEST: 'newest',
  RELEVANT: 'relevant'
}

export const AGE_VALUES = { MIN: 0, MAX: 60 }
export const EXPERIENCE_VALUES = { MIN: 0, MAX: 10 }
export const PAGE_SIZE = 10;
export const MODES = {
  FILTER: 0,
  SEARCH: 1
}
class CVHolder extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // mode
      mode: MODES.FILTER,
      sortMode: SORT_TYPES.NEWEST,
      // list cv
      cvs: [],
      total: 0,
      page: 0,
      elemCount: 0,
      // cv filter
      searchStr: "",
      displayStr: "",
      fields: [],
      age: [AGE_VALUES.MIN, AGE_VALUES.MAX],
      experience: EXPERIENCE_VALUES.MIN,
      degree: [],
      gender: [],
      province: [],
      filterProvinces: [],
      isJobSearchOn: false
    }
    this.watchFilterChange = this.watchFilterChange.bind(this);
    this.selectFuctionBasedOnMode = this.selectFuctionBasedOnMode.bind(this);
    this.retrieveSearchResults = this.retrieveSearchResults.bind(this);
    this.getSearchInput = this.getSearchInput.bind(this);
    this.getSortOption = this.getSortOption.bind(this);

  }
  watchFilterChange(value, filterType) {
    const newCheckboxState = (arr, value) => {
      const tmp = _.uniqBy([...arr, value].reverse(), 'value').filter(elem => elem.checked);
      return tmp;
    }
    switch (filterType) {
      case FILTER_TYPES.FIELDS:
        this.setState({ fields: value }, () => this.selectFuctionBasedOnMode())
        break;
      case FILTER_TYPES.AGE:
        this.setState({ age: value }, () => this.selectFuctionBasedOnMode())
        break;
      case FILTER_TYPES.EXPERIENCE:
        this.setState({ experience: value }, () => this.selectFuctionBasedOnMode())
        break;
      case FILTER_TYPES.DEGREE:
        const { degree } = this.state;
        var newArr = newCheckboxState(degree, value);
        this.setState({ degree: newArr }, () => this.selectFuctionBasedOnMode())
        break;
      case FILTER_TYPES.GENDER:
        const { gender } = this.state;
        var newArr = newCheckboxState(gender, value);
        this.setState({ gender: newArr }, () => this.selectFuctionBasedOnMode())
        break;
      case FILTER_TYPES.PROVINCE:
        const { province } = this.state;
        var newArr = newCheckboxState(province, value);
        this.setState({ province: newArr }, () => this.selectFuctionBasedOnMode())
        break;
      case FILTER_TYPES.IS_JOB_SEARCH_ON:
        const { isJobSearchOn } = this.state;
        this.setState({ isJobSearchOn: !isJobSearchOn }, () => this.selectFuctionBasedOnMode())
        break;
      default:
        break;
    }
  }
  setFilterOptions() {
    const { fields, age, experience, degree, gender, province, isJobSearchOn } = this.state;
    const data = {
      fields, age, experience, isJobSearchOn,
      degree: degree.map(elem => elem.value),
      gender: gender.map(elem => elem.value),
      province: province.map(elem => elem.value),
      
    }
    return data;
  }
  postFilterOptions(page = 0, pageSize = PAGE_SIZE, sortMode = SORT_TYPES.TRENDING) {
    const data = this.setFilterOptions();
    console.log(data);
    this.setState({ cvs: [] }, () => {
      request
        .post('/api/user/filter')
        .query({ page, PAGE_SIZE: pageSize, sortMode })
        .send(data)
        .end((err, res) => {
          if (err) {
            console.log(err);
          }
          else {
            const cvs =
              (sortMode === SORT_TYPES.TRENDING)
                ? res.body.items
                : (sortMode === SORT_TYPES.NEWEST)
                  ? res.body.cvs
                  : [];

            const { total, page, elemCount } = res.body;
            this.setState({ cvs, total, page: Number(page), elemCount });
          }
        })
    })
  }
  retrieveSearchResults(page = 0, pageSize = PAGE_SIZE, sortMode = SORT_TYPES.RELEVANT) {
    const data = this.setFilterOptions();
    const { searchStr } = this.state;
    if (!searchStr) return;
    this.setState({ cvs: [], mode: MODES.SEARCH, displayStr: searchStr, sortMode }, () => {
      request
        .post('/api/search/user')
        .query({ searchStr, page, PAGE_SIZE: pageSize, sortMode })
        .send(data)
        .end((err, res) => {
          if (err) {
            console.log(err);
          }
          else {
            const { sortedHits, total, page, elemCount } = res.body;
            this.setState({ cvs: sortedHits, total, page: Number(page), elemCount })
          }
        })
    })
  }
  getSearchInput(value) {
    this.setState({ searchStr: value });
  }
  getSortOption(value) {
    this.setState({ sortMode: value }, () => this.selectFuctionBasedOnMode());
  }
  getFilterProvinces() {
    request
      .get('/api/location/existed-provinces/user')
      .end((err, res) => {
        if (err) console.log(err);
        else {
          this.setState({ filterProvinces: res.body.provinces });
        }
      })
  }
  selectFuctionBasedOnMode(page = 0, pageSize = PAGE_SIZE) {
    if (this.state.mode === MODES.FILTER) {
      this.postFilterOptions(page, PAGE_SIZE, this.state.sortMode);
    }
    else {
      this.retrieveSearchResults(page, PAGE_SIZE, this.state.sortMode);
    }
  }
  componentWillMount() {
    window.scrollTo(0, 0);
    this.selectFuctionBasedOnMode();
    this.getFilterProvinces();
  }


  render() {
    const { filterProvinces, age, experience, cvs, total, page, elemCount, displayStr, mode, sortMode, isJobSearchOn } = this.state;

    return (
      <div className="container">
        <div className="push"></div>
        <div className="row">
          <ListCV
            cvs={cvs}
            total={total}
            page={page}
            elemCount={elemCount}
            displayStr={displayStr}
            mode={mode}
            sortMode={sortMode}
            selectFuctionBasedOnMode={this.selectFuctionBasedOnMode}
            getSortOption={this.getSortOption}

          />

          <CVFilter
            watchFilterChange={this.watchFilterChange}
            getSearchInput={this.getSearchInput}
            retrieveSearchResults={this.retrieveSearchResults}
            age={age}
            experience={experience}
            isJobSearchOn={isJobSearchOn}
            filterProvinces={filterProvinces} />
        </div>
      </div>
    )
  }
}

export default CVHolder