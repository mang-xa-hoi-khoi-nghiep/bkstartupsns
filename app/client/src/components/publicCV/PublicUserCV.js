import React from 'react';
import { Link } from 'react-router';
import { Modal, Button } from 'react-bootstrap';
import moment from 'moment';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';

import swal from 'sweetalert';
import UserCV from '../share/UserCV';
import SimUserCVs from './SimUserCVs';
import request from 'superagent';
import _ from 'lodash';
import async from 'async';
import Spinner from 'react-spinkit';
import loading from '../../assets/icon/Rolling.gif';
const RecruitModal = ({ showModal, close, title, choosenRecruitment, content, recruitmentOptions, handleSubmited, handleRecruitmentChange, handleContentChange }) => {
  return (
    <div className="modal-container text-left usercms-modal">
      <Modal show={showModal}
        onHide={close}
        container={this}
        aria-labelledby="contained-modal-title">
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title">{title}</Modal.Title>
        </Modal.Header>
        <form className="form-horizontal usercms-detail" onSubmit={(e) => handleSubmited(e)}>
          <Modal.Body>
            <div className="row">
              <div className="col-sm-12">
                <div className="form-group">
                  <div className="col-sm-12" style={{ marginBottom: 5 }}>
                    <label htmlFor="listRecruitment" className="control-label">Mời ứng tuyển cho tin tuyển dụng </label>
                  </div>
                  <div className="col-sm-12">
                    <Select
                      id="listRecruitment"
                      value={choosenRecruitment}
                      options={recruitmentOptions}
                      onChange={(e) => handleRecruitmentChange(e)}
                    />
                  </div>
                </div>
              </div>
              <div className="col-sm-12">
                <div className="form-group">
                  <div className="col-sm-12" style={{ marginBottom: 5 }}>
                    <label htmlFor="invitation-content" className="control-label">Nội dung </label>
                  </div>
                  <div className="col-sm-12">
                    <textarea
                      id="invitation-content"
                      className="form-control"
                      rows="4"
                      value={content}
                      onChange={(e) => handleContentChange(e)}
                    ></textarea>
                  </div>
                </div>
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button type="submit" className="btn recruitment-apply-btn pull-left">Xác nhận</Button>
            <Button type="button" className="btn btn-default" id="closeBtn" onClick={(e) => close()}>Trở về</Button>
          </Modal.Footer>
        </form>
      </Modal>
    </div>
  )
}
/* profile view */
class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.startViewTime = 0;
    this.endTime = 0;
    this.state = {
      user: {},
      simUsers: [],


      choosenRecruitment: {},
      invitationContent: "",
      recruitmentOptions: [],
      recruitModalShow: false
    }
    this.closeRecruitUserModal = this.closeRecruitUserModal.bind(this);
    this.handleRecruitUserSubmit = this.handleRecruitUserSubmit.bind(this);
    this.handleRecruitmentChange = this.handleRecruitmentChange.bind(this);
  }

  getUser(id) {
    request
      .get(`/api/user/get-one/${id}`)
      .end((err, res) => {
        if (err) console.log(err);
        else {
          this.setState({ user: res.body.user })
        }
      })
  }
  getSimUsers(callback) {
    request
      .get(`/api/user/sim-user/${localStorage.getItem('userId')}`)
      .end((err, res) => {
        if (err) {
          console.log(err);
        }
        else {
          const { simUsers } = res.body;
          const flatten = simUsers.map(elem => { return { ...elem.col, similarity: elem.similarity } });

          callback(null, flatten);
        }
      })
  }
  getPersonalRecommendUsers(callback) {
    if (localStorage.getItem('token')) {
      const token = localStorage.getItem('token');
      request
        .get('/api/user/sim-user')
        .query({ currentViewId: localStorage.getItem('userId') })
        .set('x-access-token', token)
        .end((err, res) => {
          if (err) {
            console.log(err);
          }
          else {
            const { simUsers } = res.body;
            const flatten = simUsers.map(elem => { return { ...elem.col, point: elem.point } });
            callback(null, flatten);
          }
        })
    }
    else {
      callback(null, []);
    }
  }
  recommender() {
    async.parallel({
      personal: this.getPersonalRecommendUsers,
      general: this.getSimUsers,
    },
      (err, results) => {
        if (err) console.log(err);
        const { personal, general } = results;
        let merged = [...general];
        personal.forEach(elem => {
          const index = _.random(2, general.length);
          merged.splice(index, 0, elem);
        })
        let uniqArr = _.uniqBy(merged, '_id');
        async.each(
          uniqArr,
          (elem, next) => {
            request
              .get(`/api/field/field3/${elem.jobField}`)
              .end((err, res) => {
                if (err) {
                  console.log(err);
                }
                else {
                  elem.fieldName = res.body.result;
                }
                next();
              })
          },
          (err) => {
            if (err) console.log(err);
            this.setState({ simUsers: uniqArr });
          }
        )
      }
    )
  }
  postToServerUserView() {
    this.endViewTime = Date.now();
    this.timePeriod = this.endViewTime - this.startViewTime;
    const data = {
      userId: this.props.params.id,
      timePeriod: this.timePeriod,
      type: 'user'
    }
    const token = localStorage.getItem('token');
    if (token) {
      request
        .post('/api/history/user-view-history')
        .set('x-access-token', token)
        .send(data)
        .end((err, res) => {
          if (err) console.log(err);
        })
    }
  }
  countView(id) {
    request
      .post('/api/user/view')
      .send({ id })
      .end((err, res) => {
        if (err) {
          console.log(err);
        }
      })
  }
  getMyRecruitments() {
    const token = localStorage.getItem('token');
    if (token) {
      request
        .get('/api/recruitment/get-by-user-id')
        .set('x-access-token', token)
        .end((err, res) => {
          if (err) console.log(err);
          else {
            const { recruitments } = res.body;
            const recruitmentOptions = recruitments.map(elem => {
              return { value: elem._id, label: elem.title }
            });
            this.setState({
              recruitmentOptions,
              choosenRecruitment: recruitmentOptions[0],
              invitationContent: `Chúng tôi mời bạn tham gia ứng tuyển cho ${recruitmentOptions[0].label}`
            });
          }
        })
    }
  }
  openRecruitUserModal() {
    this.setState({ recruitModalShow: true });
  }
  closeRecruitUserModal() {
    this.setState({ recruitModalShow: false });
  }
  handleRecruitmentChange(e) {
    this.setState({
      choosenRecruitment: e,
      invitationContent: `Chúng tôi mời bạn tham gia ứng tuyển cho ${e.label}`
    });
  }
  handleInvitationContentChange = (e) => {
    this.setState({ invitationContent: e.target.value });
  }
  handleRecruitUserSubmit(e) {
    e.preventDefault();
    const { choosenRecruitment, invitationContent } = this.state;
    const data = {
      recruitmentId: choosenRecruitment.value,
      userId: this.props.params.id,
      invitationContent
    }
    const token = localStorage.getItem('token');
    if (token) {
      request
        .post('/api/recruitment/interested-candidates')
        .set('x-access-token', token)
        .send(data)
        .end((err, res) => {
          if (err) console.log(err);
          else {
            if (res.body.success) {
              alert('Mời ứng tuyển thành công');
              this.setState({ recruitModalShow: false })
            }
            else {
              alert(res.body.message);
            }
          }
        })
    }
  }
  componentWillMount() {
    window.scrollTo(0, 0);
    moment.locale('vi');
    this.startViewTime = Date.now();
    localStorage.setItem('userId', this.props.params.id);
    this.recommender();
    this.getUser(this.props.params.id);
    this.countView(this.props.params.id);
    this.getMyRecruitments();
  }

  componentWillUnmount() {
    this.postToServerUserView();
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.params.id !== nextProps.params.id) {
      this.postToServerUserView();
      window.scrollTo(0, 0);
      window.location.reload();
    }
  }

  render() {
    const { user, simUsers, recruitModalShow, recruitmentOptions, choosenRecruitment, invitationContent } = this.state;
    const basicInfo = {
      _id: user._id,
      lastname: user.lastname,
      firstname: user.firstname,
      address: user.address,
      website: user.website,
      gender: user.gender
    }
    var arr=[], avatar = '';
    try {
      arr = user._avatar.url.split('/');  
      avatar =  '/' + [arr[arr.length-2], arr[arr.length-1]].join('/');
    } catch (error) {}
    return (
      <div className="container-fluid">
        <div className="push"></div>
        {(_.isEmpty(user)) ?
          <div className="row">
            <div className="loading-icon">
                <img src={ loading } />
            </div>
          </div>
          :
          <div className="row">
            <div className="col-sm-7">
              <UserCV user={user} basicInfo={basicInfo} avatar={avatar} />
              <div className="text-center">
                <button className="ui large button recruitment-apply-btn" onClick={() => this.openRecruitUserModal()}>Mời ứng tuyển</button>
                <RecruitModal
                  showModal={recruitModalShow}
                  close={this.closeRecruitUserModal}
                  title='Tuyển dụng'
                  choosenRecruitment={choosenRecruitment}
                  content={invitationContent}
                  recruitmentOptions={recruitmentOptions}
                  handleSubmited={this.handleRecruitUserSubmit}
                  handleRecruitmentChange={this.handleRecruitmentChange}
                  handleContentChange={this.handleInvitationContentChange}
                />
              </div>
            </div>
            <SimUserCVs title="Hồ sơ liên quan" cvs={simUsers} />
          </div>
        }
      </div>
    )
  }
}

export default Profile
