require('./configuration/config');
const express     =   require('express');
const fs          =   require('fs');
const bodyParser  =   require('body-parser');
const morgan      =   require('morgan');
const cookieParser =  require('cookie-parser');
const session     =   require('express-session');
const {mongoose}    =   require('./db/mongoose');
const file = require('./services/libs/file');
const os = require('os');
const path = require('path');
const app = express();
const server  = require('http').createServer(app);
const port = process.env.PORT;


// CORS
app.use(function(req, res, next) {
   res.header("Access-Control-Allow-Origin", "*"); // keep this if your api accepts cross-origin requests
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, X-Access-Token");
   next();
});
//serve static assets

app.use(express.static('uploads/user'));
app.use(express.static('uploads/organization'));
app.use(express.static('uploads/recruitment'));
app.use(express.static('client/src/assets/icon'));




// app.use(cookieParser());
// var sess = {
//   secret: "This is a secret",
//   resave: false,
//   saveUninitialized: false,
//   cookie: {}
// };

// app middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan('dev')); // use morgan to log requests to the console






//app.use(session(sess));
// routes
const userRoutes = require('./services/routes/user');
const userCVRoutes = require('./services/routes/userCV');
const organizationRoutes = require('./services/routes/organization');
const recruitmentRoutes = require('./services/routes/recruitment');
//const recommendationRoutes = require('./services/routes/recommendation');
const historyRoutes = require('./services/routes/history');
const searchRoutes = require('./services/routes/search');
const fieldRoutes = require('./services/routes/field');
const notificationRoutes = require('./services/routes/notification');
const invitationRoutes = require('./services/routes/invitation');
const locationRoutes = require('./services/routes/location');

const employeeRoutes = require('./services/routes/employee');
const eventRoutes = require('./services/routes/event');
const fileRoutes = require('./services/routes/file');
const schoolRoutes = require('./services/routes/school');

app.use('/api/recruitment', recruitmentRoutes);
app.use('/api/user', userRoutes);
app.use('/api/userCV', userCVRoutes);
app.use('/api/organization', organizationRoutes);
//app.use('/api/recommendation', recommendationRoutes);
app.use('/api/history', historyRoutes);
app.use('/api/search', searchRoutes);
app.use('/api/field', fieldRoutes);
app.use('/api/notification', notificationRoutes);
app.use('/api/invitation', invitationRoutes);
app.use('/api/file', fileRoutes);
app.use('/api/location', locationRoutes);

app.use('/api/employee', employeeRoutes);
app.use('/api/event', eventRoutes);
app.use('/api/school', schoolRoutes);
global.user_history = [];


// file
// file.importExcel();
// file.importSchoolFromExcel();
// recommender

//
//search
const RecommenderIndexing = require('./services/libs/simCandidates').RecommenderIndexing;
const TYPES = require('./services/libs/simCandidates').TYPES;
const Test = new RecommenderIndexing(TYPES.RECRUITMENT);
Test.exec();
const Test1 = new RecommenderIndexing(TYPES.USER);
Test1.exec();

// Express only serves static assets in production
if (process.env.NODE_ENV === 'production') {
  app.use(express.static(path.join(__dirname, 'client/build')));
  app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
  });
  app.set('trust proxy', 1) // trust first proxy
  //sess.cookie.secure = true // serve secure cookies
}
server.listen(port, () => {
  console.log(`Find the server at:`, server.address()) // eslint-disable-line no-console
  console.log(process.env.MESSAGE || "Running in production mode");
  console.log("ENDPOINT :", process.env.API_ENDPOINT);
});
module.exports = {app}
