var env = process.env.NODE_ENV || 'production';

if (env === 'production' ||env === 'development' || env === 'test' || env === 'local_dev' || env === 'local_test') {
    
  var config = require('./config.json');
  var envConfig = config[env];

  Object.keys(envConfig).forEach((key) => {
    process.env[key] = envConfig[key];
  });
}


