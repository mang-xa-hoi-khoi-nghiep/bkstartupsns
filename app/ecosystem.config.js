module.exports = {
  apps: [{
    name: "BKStartupServer",
    script: "./server.js",
    watch: false,
    env: {
      "NODE_ENV": "development",
    },
    env_production: {
      "NODE_ENV": "production",
      "PORT": 5000,
      "MONGODB_URI": "mongodb://youthvnTest:test%40youthvn@159.89.193.195:27017/youthvnTest",
      "JWT_SECRET": "xcvlhqk213usklncksxcjkad",
      "MESSAGE": "Running in online database production mode",
      "API_ENDPOINT": "http://159.89.193.195:3000"
    }
  }]
}